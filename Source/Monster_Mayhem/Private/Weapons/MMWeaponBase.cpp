// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapons/MMWeaponBase.h"
#include "DrawDebugHelpers.h"
#include "InputActionValue.h"
#include "MMCharacter.h"
#include "Components/ArrowComponent.h"
#include "NiagaraComponent.h"
#include "Components/AudioComponent.h"
#include "Components/DecalComponent.h"
#include "Engine/StreamableManager.h"
#include "Helper/FMMAsyncLoader.h"
#include "Projectiles/MMProjectileBase.h"
#include "Weapon/MMEjectableObject.h"

// Sets default values
AMMWeaponBase::AMMWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	WeaponMesh -> SetGenerateOverlapEvents(false);
	WeaponMesh -> SetCollisionProfileName("NoCollision");
	WeaponMesh -> SetupAttachment(RootComponent);

	FireSpot = CreateDefaultSubobject<UArrowComponent>(TEXT("Fire Spot"));
	FireSpot -> SetupAttachment(RootComponent);
	
	ClipEjectionSpot = CreateDefaultSubobject<UArrowComponent>(TEXT("Clip Ejection Spot"));
	ClipEjectionSpot -> ArrowSize = 0.05f;
	ClipEjectionSpot -> SetupAttachment(RootComponent);
	
	ShellEjectionSpot = CreateDefaultSubobject<UArrowComponent>(TEXT("Shell Ejection Spot"));
	ShellEjectionSpot -> ArrowSize = 0.05f;
	ShellEjectionSpot -> SetupAttachment(RootComponent);
	
	MuzzleFlash = CreateDefaultSubobject<UNiagaraComponent>(TEXT("Muzzle Flash"));
	MuzzleFlash -> SetAutoActivate(false);
	MuzzleFlash -> SetupAttachment(RootComponent);

	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("Audio"));
	AudioComponent -> SetAutoActivate(false);
	AudioComponent -> SetupAttachment(RootComponent);

	bIsEquipped = false;
	bCanFire = true;

	ReloadTime = 0.f;
	bIsReload = false;

	FireDirectionThreshold = 200.f;
	CurrentDispersion = 0.f;
}

// Called when the game starts or when spawned
void AMMWeaponBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AMMWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AMMWeaponBase::InitializeComponents(const FMMWeaponComponentsParams& ComponentsParams)
{
	if (IsValid(WeaponMesh), IsValid(FireSpot), IsValid(ShellEjectionSpot), IsValid(ClipEjectionSpot), IsValid(MuzzleFlash))
	{
		LoadWeaponMesh(ComponentsParams.GetSkeletalMesh());
		FireSpot -> SetRelativeTransform(ComponentsParams.GetFireSpotTransform());
		ShellEjectionSpot -> SetRelativeTransform(ComponentsParams.GetShellEjectionSpotTransform());
		ClipEjectionSpot -> SetRelativeTransform(ComponentsParams.GetClipEjectionTransform());
		MuzzleFlash -> SetRelativeTransform(ComponentsParams.GetMuzzleFlashTransform()); 
	}
}

void AMMWeaponBase::LoadWeaponMesh(const TSoftObjectPtr<USkeletalMesh>& Mesh)
{
	const auto OnLoadedDelegate = FStreamableDelegate::CreateUObject(this, &ThisClass::SetWeaponMesh, Mesh);
	FMMAsyncLoader::LoadAsset(Mesh, OnLoadedDelegate);
}

void AMMWeaponBase::SetWeaponMesh(TSoftObjectPtr<USkeletalMesh> Mesh)
{
	if (Mesh.IsValid() && IsValid(WeaponMesh))
	{
		WeaponMesh -> SetSkeletalMeshAsset(Mesh.Get());
		FMMAsyncLoader::UnloadAsset(Mesh);
	}
}

void AMMWeaponBase::InitializeParams(USceneComponent* Component, const int32& _Id, const TSoftObjectPtr<UTexture2D>& _HUDThumbnail, const FMMWeaponInfo& _WeaponInfo)
{
	CursorDecal = Component;
	
	Id = _Id;
	SlotType = _WeaponInfo.GetSlotType();
	HUDThumbnail = _HUDThumbnail;
	BaseParameters = _WeaponInfo.GetBaseParameters();
	DispersionData = _WeaponInfo.GetWeaponDispersionData();
	CurrentDispersionParams = DispersionData.GetIdleDispersionParams();
	CurrentDispersion = DispersionData.GetIdleDispersionParams().GetDispersionMin();
	WeaponTrackingInfo = _WeaponInfo.GetWeaponTrackingInfo();
	
	auto ProjectileParams = _WeaponInfo.GetProjectileBaseParameters();
	ProjectileParams.InitializeProjectileParams(BaseParameters);
	SpawnProjectilePool(_WeaponInfo.GetProjectileBaseClass(), ProjectileParams);
	SpawnClipPool(_WeaponInfo.GetWeaponFX().GetClipClass());
	SpawnShellPool(_WeaponInfo.GetWeaponFX().GetShellClass());

	
	LoadBaseFireAnim(_WeaponInfo.GetWeaponAnimations().GetBaseFire());
	LoadAimFireAnim(_WeaponInfo.GetWeaponAnimations().GetAimFire());
	LoadReloadAnim(_WeaponInfo.GetWeaponAnimations().GetReload());

	LoadMuzzleFlash(_WeaponInfo.GetWeaponFX().GetMuzzleFlash());
	LoadFireSound(_WeaponInfo.GetWeaponFX().GetFireSound());

	bIsEquipped = true;
}

void AMMWeaponBase::InitializeParams(USceneComponent* Component, const FMMWeaponInfo& _WeaponInfo)
{
	CursorDecal = Component;
	BaseParameters = _WeaponInfo.GetBaseParameters();
	
	auto ProjectileParams = _WeaponInfo.GetProjectileBaseParameters();
	ProjectileParams.InitializeProjectileParams(BaseParameters);
	SpawnProjectilePool(_WeaponInfo.GetProjectileBaseClass(), ProjectileParams);
	SpawnClipPool(_WeaponInfo.GetWeaponFX().GetClipClass());
	SpawnShellPool(_WeaponInfo.GetWeaponFX().GetShellClass());
	
	LoadBaseFireAnim(_WeaponInfo.GetWeaponAnimations().GetBaseFire());
	LoadReloadAnim(_WeaponInfo.GetWeaponAnimations().GetReload());

	LoadMuzzleFlash(_WeaponInfo.GetWeaponFX().GetMuzzleFlash());
	LoadFireSound(_WeaponInfo.GetWeaponFX().GetFireSound());

	bIsEquipped = true;
}

void AMMWeaponBase::LoadBaseFireAnim(TSoftObjectPtr<UAnimMontage> Anim)
{
	const auto OnLoadedDelegate = FStreamableDelegate::CreateUObject(this, &ThisClass::SetBaseFireAnim, Anim);
	FMMAsyncLoader::LoadAsset(Anim, OnLoadedDelegate);
}

void AMMWeaponBase::SetBaseFireAnim(TSoftObjectPtr<UAnimMontage> Anim)
{
	if (Anim.IsValid())
	{
		BaseFireAnim = Anim.Get();
		FMMAsyncLoader::UnloadAsset(Anim);
	}
}

void AMMWeaponBase::LoadAimFireAnim(TSoftObjectPtr<UAnimMontage> Anim)
{
	const auto OnLoadedDelegate = FStreamableDelegate::CreateUObject(this, &ThisClass::SetAimFireAnim, Anim);
	FMMAsyncLoader::LoadAsset(Anim, OnLoadedDelegate);
}

void AMMWeaponBase::SetAimFireAnim(TSoftObjectPtr<UAnimMontage> Anim)
{
	if (Anim.IsValid())
	{
		AimFireAnim = Anim.Get();
		FMMAsyncLoader::UnloadAsset(Anim);
	}
}

void AMMWeaponBase::LoadReloadAnim(TSoftObjectPtr<UAnimMontage> Anim)
{
	const auto OnLoadedDelegate = FStreamableDelegate::CreateUObject(this, &ThisClass::SetReloadAnim, Anim);
	FMMAsyncLoader::LoadAsset(Anim, OnLoadedDelegate);
}

void AMMWeaponBase::SetReloadAnim(TSoftObjectPtr<UAnimMontage> Anim)
{
	if (Anim.IsValid())
	{
		ReloadAnim = Anim.Get();
		ReloadTime = Anim.Get() -> CalculateSequenceLength();
		FMMAsyncLoader::UnloadAsset(Anim);
	}
}

void AMMWeaponBase::LoadMuzzleFlash(TSoftObjectPtr<UNiagaraSystem> Particle)
{
	const auto OnLoadedDelegate = FStreamableDelegate::CreateUObject(this, &ThisClass::SetMuzzleFlash, Particle);
	FMMAsyncLoader::LoadAsset(Particle, OnLoadedDelegate);
}

void AMMWeaponBase::SetMuzzleFlash(TSoftObjectPtr<UNiagaraSystem> Particle)
{
	if(IsValid(MuzzleFlash) && Particle.IsValid())
	{
		MuzzleFlash -> SetAsset(Particle.Get());
		FMMAsyncLoader::UnloadAsset(Particle);
	}
}

void AMMWeaponBase::LoadFireSound(TSoftObjectPtr<USoundCue> Sound)
{
	const auto OnLoadedDelegate = FStreamableDelegate::CreateUObject(this, &ThisClass::SetFireSound, Sound);
	FMMAsyncLoader::LoadAsset(Sound, OnLoadedDelegate);
}

void AMMWeaponBase::SetFireSound(TSoftObjectPtr<USoundCue> Sound)
{
	if(IsValid(AudioComponent) && Sound.IsValid())
	{
		AudioComponent -> SetSound(Sound.Get());
		FMMAsyncLoader::UnloadAsset(Sound);
	}
}

void AMMWeaponBase::SpawnProjectilePool(const TSubclassOf<AMMProjectileBase>& ProjectileBaseClass, const FMMProjectileBaseParams& ProjectileBaseParameters)
{
	if (IsValid(ProjectileBaseClass) && IsValid(GetWorld()) && IsValid(FireSpot))
	{
		const int32 ProjectilesToSpawn = BaseParameters.GetMaxRoundsInClip() * BaseParameters.GetRoundsPerShot();
		
		for (int i = 0; i < ProjectilesToSpawn; ++i)
		{
			FVector SpawnLocation = FireSpot -> GetComponentLocation();
			FRotator SpawnRotation = FRotator::ZeroRotator;

			FActorSpawnParameters SpawnParameters;
			SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParameters.Owner = this;
			SpawnParameters.Instigator = Cast<APawn>(GetOwner());
				
			TWeakObjectPtr<AMMProjectileBase> Projectile = Cast<AMMProjectileBase>(GetWorld() -> SpawnActor(ProjectileBaseClass, &SpawnLocation, &SpawnRotation, SpawnParameters));

			if (Projectile.IsValid() && IsValid(Cast<APawn>(GetOwner()) -> GetController()))
			{
				Projectile.Get() -> SetEventInstigator(Cast<APawn>(GetOwner()) -> GetController());
				Projectile.Get() -> SetBaseParameters(ProjectileBaseParameters);
				Projectile.Get() -> SetActive(false);
				Projectile.Get() -> AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);;
				Projectile.Get() -> OnDeactivateProjectile.AddUObject(this, &ThisClass::AddProjectileToInactivePool);
					
				InactiveProjectileQueue.Emplace(Projectile);
			}
		}
	}
}

void AMMWeaponBase::SpawnClipPool(const TSubclassOf<AMMEjectableObject>& ClipBaseClass)
{
	if (IsValid(ClipBaseClass) && IsValid(GetWorld()) && IsValid(ClipEjectionSpot))
	{
		const int32 ObjectToSpawn = 1;
		
		for (int i = 0; i < ObjectToSpawn; ++i)
		{
			FVector SpawnLocation = ClipEjectionSpot -> GetComponentLocation();
			FRotator SpawnRotation = FQuat::Identity.Rotator();

			FActorSpawnParameters SpawnParameters;
			SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParameters.Owner = this;
				
			TWeakObjectPtr<AMMEjectableObject> Object = Cast<AMMEjectableObject>(GetWorld() -> SpawnActor(ClipBaseClass, &SpawnLocation, &SpawnRotation, SpawnParameters));

			if (Object.IsValid())
			{
				Object.Get() -> SetActive(false);
				Object.Get() -> SetLaunchDirection(ClipEjectionSpot -> GetRightVector());
				Object.Get() -> AttachToComponent(ClipEjectionSpot, FAttachmentTransformRules::KeepWorldTransform);;
				Object.Get() -> OnDeactivateObject.AddUObject(this, &ThisClass::AddClipToInactivePool);
					
				InactiveClipQueue.Enqueue(Object);
			}
		}
	}
}

void AMMWeaponBase::SpawnShellPool(const TSubclassOf<AMMEjectableObject>& ShellBaseClass)
{
	if (IsValid(ShellBaseClass) && IsValid(GetWorld()) && IsValid(ShellEjectionSpot))
	{
		const int32 ObjectToSpawn = BaseParameters.GetMaxRoundsInClip();
		
		for (int i = 0; i < ObjectToSpawn; ++i)
		{
			FVector SpawnLocation = ShellEjectionSpot -> GetComponentLocation();
			FRotator SpawnRotation = FQuat::Identity.Rotator();

			FActorSpawnParameters SpawnParameters;
			SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParameters.Owner = this;
				
			TWeakObjectPtr<AMMEjectableObject> Object = Cast<AMMEjectableObject>(GetWorld() -> SpawnActor(ShellBaseClass, &SpawnLocation, &SpawnRotation, SpawnParameters));

			if (Object.IsValid())
			{
				Object.Get() -> SetActive(false);
				Object.Get() -> SetLaunchDirection(ShellEjectionSpot -> GetForwardVector());
				Object.Get() -> AttachToComponent(ShellEjectionSpot, FAttachmentTransformRules::KeepWorldTransform);;
				Object.Get() -> OnDeactivateObject.AddUObject(this, &ThisClass::AddShellToInactivePool);
					
				InactiveShellQueue.Enqueue(Object);
			}
		}
	}
}

void AMMWeaponBase::Deinitialize()
{
	if (!InactiveProjectileQueue.IsEmpty())
	{
		for (auto Element : InactiveProjectileQueue)
		{
			if (Element.IsValid())
			{
				Element.Get() -> Destroy();
			}	
		}
	}

	if (!ActiveProjectileQueue.IsEmpty())
	{
		for (auto Element : ActiveProjectileQueue)
		{
			if (Element.Key.IsValid())
			{
				Element.Key.Get()-> Destroy();
			}
		}
	}

	if (!InactiveClipQueue.IsEmpty())
	{
		while (!InactiveClipQueue.IsEmpty())
		{
			TWeakObjectPtr<AMMEjectableObject> Object;

			if (InactiveClipQueue.Dequeue(Object))
			{
				if (Object.IsValid())
				{
					Object.Get() -> Destroy();
				}
			}
		}
	}

	if (!ActiveClipQueue.IsEmpty())
	{
		while (!InactiveClipQueue.IsEmpty())
		{
			TWeakObjectPtr<AMMEjectableObject> Object;

			if (ActiveClipQueue.Dequeue(Object))
			{
				if (Object.IsValid())
				{
					Object.Get() -> Destroy();
				}
			}
		}
	}

	if (!InactiveShellQueue.IsEmpty())
	{
		while (!InactiveShellQueue.IsEmpty())
		{
			TWeakObjectPtr<AMMEjectableObject> Object;

			if (InactiveShellQueue.Dequeue(Object))
			{
				if (Object.IsValid())
				{
					Object.Get() -> Destroy();
				}
			}
		}
	}

	if (!ActiveShellQueue.IsEmpty())
	{
		while (!ActiveShellQueue.IsEmpty())
		{
			TWeakObjectPtr<AMMEjectableObject> Object;

			if (ActiveShellQueue.Dequeue(Object))
			{
				if (Object.IsValid())
				{
					Object.Get() -> Destroy();
				}
			}
		}
	}

	bIsEquipped = false;
}

//to do Reset Fire when start sprinting.
void AMMWeaponBase::SetCurrentDispersionParams(const EMMMovementState& MovementState)
{
	if (MovementState == EMMMovementState::MS_Idle)
	{
		CurrentDispersionParams = DispersionData.GetIdleDispersionParams();
	}
	else if (MovementState == EMMMovementState::MS_Aim)
	{
		CurrentDispersionParams = DispersionData.GetAimDispersionParams();
	}
	else if (MovementState == EMMMovementState::MS_Walk)
	{
		CurrentDispersionParams = DispersionData.GetWalkDispersionParams();
	}
	else
	{
		bCanFire = false;
	}
}

void AMMWeaponBase::ExecuteFire(const FOnUpdateWeaponTrackingInfo& Delegate)
{
	FireLogic(Delegate);

	if (BaseParameters.GetWeaponFireType() == EMMWeaponFireType::WFT_AutoFire)
	{
		AutoFire(Delegate);
	}
	else
	{
		SingleFire();
	}
}

void AMMWeaponBase::AutoFire(const FOnUpdateWeaponTrackingInfo& Delegate)
{
	if (IsValid(GetWorld()))
	{
		if (!GetWorld() -> GetTimerManager().TimerExists(FireTimerHandle))
		{
			GetWorld() -> GetTimerManager().SetTimer(FireTimerHandle, [&]{ FireLogic(Delegate); }, BaseParameters.GetRateOfFire(), true);
		}
	}
}

void AMMWeaponBase::SingleFire()
{
	if (IsValid(GetWorld()))
	{
		if (!GetWorld() -> GetTimerManager().TimerExists(FireTimerHandle))
		{
			bCanFire = false;
			GetWorld() -> GetTimerManager().SetTimer(FireTimerHandle,  this, &ThisClass::ResetFire, BaseParameters.GetRateOfFire(), false);
		}
	}
}

void AMMWeaponBase::ResetFire()
{
	if (IsValid(GetWorld()))
	{
		if (GetWorld() -> GetTimerManager().TimerExists(FireTimerHandle))
		{
			GetWorld() -> GetTimerManager().ClearTimer(FireTimerHandle);
		}

		SetDispersionReductionTimer();

		if (IsValid(MuzzleFlash)) MuzzleFlash -> Deactivate();
		if (IsValid(AudioComponent)) AudioComponent -> Stop();
		if (!bCanFire) bCanFire = true;
	}
}

void AMMWeaponBase::ResetWeaponStates()
{
	if (IsValid(GetWorld()))
	{
		ResetFire();
		GetWorld() -> GetTimerManager().ClearTimer(ReloadTimerHandle);
		bIsReload = false;
	}
}

void AMMWeaponBase::FireLogic(const FOnUpdateWeaponTrackingInfo& Delegate)
{
	if (bCanFire)
	{
		if (LaunchProjectile())
		{
			if (IsValid(MuzzleFlash) && IsValid(AudioComponent))
			{
				BaseParameters.SetRoundsInClip(BaseParameters.GetRoundsInClip() - 1);
				WeaponTrackingInfo.SetCurrentKills(WeaponTrackingInfo.GetCurrentKills() + 1);

				Delegate.ExecuteIfBound(BaseParameters.GetWeaponType(), WeaponTrackingInfo);
				
				OnUpdateWeaponUI.Broadcast(FInputActionValue(FVector2D(BaseParameters.GetRoundsInClip(), BaseParameters.GetRoundsInReserve())));
				AddDispersion();

				MuzzleFlash -> Activate();
				AudioComponent -> Play();
				LaunchShell();
				
				if (BaseParameters.GetRoundsInClip() == 0 && BaseParameters.GetWeaponType() != EMMWeaponType::WT_RocketLauncher)
				{
					OnReloadWeapon.Broadcast(FInputActionValue());
				}
			}
		}
	}
}

bool AMMWeaponBase::LaunchProjectile()
{
	if (!InactiveProjectileQueue.IsEmpty())
	{
		int8 Counter = 0;
		
		for (int8 i = 0; i < BaseParameters.GetRoundsPerShot(); ++i)
		{
			TWeakObjectPtr<AMMProjectileBase> Projectile = GetProjectileFromInactivePool();

			if (Projectile.IsValid())
			{
				Projectile.Get() -> Launch(CalcProjectileRotation(CursorDecal -> GetComponentLocation()));
				Counter++;
			}
		}

		if(Counter == 0) return false;
		
		return true;
	}

	return false;
}

void AMMWeaponBase::LaunchClip()
{
	if (!InactiveClipQueue.IsEmpty())
	{
		TWeakObjectPtr<AMMEjectableObject> Object = GetClipFromInactivePool();

		if (Object.IsValid())
		{
			Object.Get() -> Launch();
		}
	}
}

void AMMWeaponBase::LaunchShell()
{
	if (!InactiveShellQueue.IsEmpty())
	{
		TWeakObjectPtr<AMMEjectableObject> Object = GetShellFromInactivePool();

		if (Object.IsValid())
		{
			Object.Get() -> Launch();
		}
	}
}

FRotator AMMWeaponBase::CalcProjectileRotation(const FVector& DecalLocation)
{
	if (IsValid(FireSpot))
	{
		const FVector EndLocation = GetFireEndLocation(DecalLocation);
		
		FVector ShotDirection = EndLocation - FireSpot -> GetComponentLocation();
		ShotDirection.Normalize();
			
		const FMatrix DirectionMatrix(ShotDirection, FVector::RightVector, FVector::UpVector, FVector::ZeroVector);

		return DirectionMatrix.Rotator();
	}

	return FRotator::ZeroRotator;
}

void AMMWeaponBase::ExecuteReload()
{
	bIsReload = true;
	bCanFire = false;
	
	if (IsValid(GetWorld()))
	{
		if (!GetWorld() -> GetTimerManager().TimerExists(ReloadTimerHandle))
		{
			LaunchClip();
			GetWorld() -> GetTimerManager().SetTimer(ReloadTimerHandle, this, &ThisClass::ReloadTimer, ReloadTime, false);
		}
	}
}

void AMMWeaponBase::ReloadTimer()
{
	if (IsValid(GetWorld()))
	{
		Reload();
			
		bIsReload = false;
		bCanFire = true;

		if (GetWorld() -> GetTimerManager().TimerExists(ReloadTimerHandle))
		{
			GetWorld() -> GetTimerManager().ClearTimer(ReloadTimerHandle);
			OnUpdateWeaponUI.Broadcast(FInputActionValue(FVector2D(BaseParameters.GetRoundsInClip(), BaseParameters.GetRoundsInReserve())));
		}
	}
}

void AMMWeaponBase::Reload()
{
	const int8 NeededAmount = BaseParameters.GetMaxRoundsInClip() - BaseParameters.GetRoundsInClip();
	int32 Amount = BaseParameters.GetRoundsInReserve() - NeededAmount;
	
	Amount < 0 ? Amount = BaseParameters.GetRoundsInReserve() : Amount = NeededAmount;
	
	BaseParameters.SetRoundsInReserve(BaseParameters.GetRoundsInReserve() - Amount);
	BaseParameters.SetRoundsInClip(BaseParameters.GetRoundsInClip() + Amount);

	const float Percent = static_cast<float>(BaseParameters.GetRoundsInReserve()) / static_cast<float>(BaseParameters.GetMaxRoundsInReserve());
	const bool bRequestAmmo = Percent < 0.8f;

	if (bRequestAmmo)
	{
		UE_LOG(LogTemp, Log, TEXT("AMMWeaponBase::Reload: Requesting Ammo"));
		OnRequestWeaponAmmo.Broadcast(SlotType);
	}
}

FVector AMMWeaponBase::ApplyDispersion(const FVector& FireDirection)
{
	return FMath::VRandCone(FireDirection, CurrentDispersion * PI / 180.f);
}

void AMMWeaponBase::AddDispersion()
{
	if(IsValid(GetWorld()))
	{
		if (GetWorld() -> GetTimerManager().TimerExists(DispersionReductionTimerHandle))
		{
			GetWorld() -> GetTimerManager().ClearTimer(DispersionReductionTimerHandle);
		}
	}
	
	CurrentDispersion = FMath::Min(CurrentDispersion + CurrentDispersionParams.GetRecoil(), CurrentDispersionParams.GetDispersionMax());
}

void AMMWeaponBase::SetDispersionReductionTimer()
{
	if (IsValid(GetWorld()))
	{
		GetWorld() -> GetTimerManager().SetTimer(DispersionReductionTimerHandle, this, &AMMWeaponBase::DispersionReductionTimer, GetWorld() -> GetDeltaSeconds(), true);
	}
}

void AMMWeaponBase::DispersionReductionTimer()
{
	CurrentDispersion = FMath::Max(CurrentDispersion - CurrentDispersionParams.GetDispersionReduction(), CurrentDispersionParams.GetDispersionMin());

	if (CurrentDispersion == CurrentDispersionParams.GetDispersionMin())
	{
		if (IsValid(GetWorld()))
		{
			GetWorld() -> GetTimerManager().ClearTimer(DispersionReductionTimerHandle);
		}
	}
}

FVector AMMWeaponBase::GetFireEndLocation(const FVector& DecalLocation)
{
	if (IsValid(FireSpot))
	{
		FVector EndLocation;
		const FVector TempVector = DecalLocation - FireSpot -> GetComponentLocation();

		if (TempVector.Size() > FireDirectionThreshold)
		{
			EndLocation =  FireSpot -> GetComponentLocation() + ApplyDispersion(TempVector.GetSafeNormal()) * BaseParameters.GetMaxRange();

			//DrawDebugCone(GetWorld(), FireSpot -> GetComponentLocation(), TempVector, 2000.f, CurrentDispersion * PI / 180.f, CurrentDispersion * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
		}
		else
		{
			EndLocation = FireSpot -> GetComponentLocation() + ApplyDispersion(FireSpot -> GetForwardVector()) * BaseParameters.GetMaxRange();

			//DrawDebugCone(GetWorld(), FireSpot -> GetComponentLocation(), FireSpot -> GetForwardVector(), 2000.f, CurrentDispersion * PI / 180.f, CurrentDispersion * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
		}

		// //Weapon Direction
		// DrawDebugLine(GetWorld(), FireSpot->GetComponentLocation(), FireSpot->GetComponentLocation() + FireSpot->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		// //Normal Projectile Direction 
		// DrawDebugLine(GetWorld(), FireSpot->GetComponentLocation(), DecalLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		// //Dispersion Projectile Direction
		// DrawDebugLine(GetWorld(), FireSpot->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

		return EndLocation;
	}

	return FVector::ZeroVector;
}

TWeakObjectPtr<AMMProjectileBase> AMMWeaponBase::GetProjectileFromInactivePool()
{
	TWeakObjectPtr<AMMProjectileBase> Projectile = InactiveProjectileQueue.Last();
	
	if (Projectile.IsValid())
	{
		ActiveProjectileQueue.Emplace(Projectile);
		InactiveProjectileQueue.Pop();
		return Projectile;
	}

	return TWeakObjectPtr<AMMProjectileBase>(nullptr);
}

void AMMWeaponBase::AddProjectileToInactivePool(TWeakObjectPtr<AMMProjectileBase> Projectile)
{
	if (!ActiveProjectileQueue.IsEmpty())
	{
		if (IsValid(FireSpot))
		{
			if (ActiveProjectileQueue.Contains(Projectile))
			{
				InactiveProjectileQueue.Emplace(Projectile);
				ActiveProjectileQueue.FindAndRemoveChecked(Projectile);
				Projectile.Get() -> AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
				Projectile.Get() -> SetActorLocation(FireSpot -> GetComponentLocation());
			}
		}
	}
}

TWeakObjectPtr<AMMEjectableObject> AMMWeaponBase::GetClipFromInactivePool()
{
	TWeakObjectPtr<AMMEjectableObject> Object;
	
	if (InactiveClipQueue.Dequeue(Object))
	{
		ActiveClipQueue.Enqueue(Object);

		return Object;
	}
	
	return TWeakObjectPtr<AMMEjectableObject>(nullptr);
}

void AMMWeaponBase::AddClipToInactivePool()
{
	if (!ActiveClipQueue.IsEmpty())
	{
		if (IsValid(ClipEjectionSpot))
		{
			TWeakObjectPtr<AMMEjectableObject> Object;
			
			if (ActiveClipQueue.Dequeue(Object))
			{
				InactiveClipQueue.Enqueue(Object);

				Object.Get() -> AttachToComponent(ClipEjectionSpot, FAttachmentTransformRules::KeepWorldTransform);
				Object.Get() -> SetActorLocation(ClipEjectionSpot -> GetComponentLocation());
				Object.Get() -> SetActorRotation(ClipEjectionSpot -> GetComponentRotation());
			}
		}
	}
}

TWeakObjectPtr<AMMEjectableObject> AMMWeaponBase::GetShellFromInactivePool()
{
	TWeakObjectPtr<AMMEjectableObject> Object;
	
	if (InactiveShellQueue.Dequeue(Object))
	{
		ActiveShellQueue.Enqueue(Object);

		return Object;
	}

	return TWeakObjectPtr<AMMEjectableObject>(nullptr);
}

void AMMWeaponBase::AddShellToInactivePool()
{
	if (!ActiveShellQueue.IsEmpty())
	{
		if (IsValid(ShellEjectionSpot))
		{
			TWeakObjectPtr<AMMEjectableObject> Object;
			
			if (ActiveShellQueue.Dequeue(Object))
			{
				InactiveShellQueue.Enqueue(Object);

				Object.Get() -> AttachToComponent(ShellEjectionSpot, FAttachmentTransformRules::KeepWorldTransform);
				Object.Get() -> SetActorLocation(ShellEjectionSpot -> GetComponentLocation());
				Object.Get() -> SetActorRotation(ShellEjectionSpot -> GetComponentRotation());
			}
		}
	}
}

void AMMWeaponBase::SetActive(const bool& bIsActive)
{
	SetActorTickEnabled(bIsActive);
	SetActorHiddenInGame(!bIsActive);
	SetActorEnableCollision(bIsActive);
}