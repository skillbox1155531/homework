// Fill out your copyright notice in the Description page of Project Settings.

#include "Environment/MMItemSpawnPoint.h"
#include "Items/MMItem.h"
#include "Items/MMPickupActor.h"

void AMMItemSpawnPoint::BeginPlay()
{
	Super::BeginPlay();
}

void AMMItemSpawnPoint::Initialization()
{
	if (IsValid(GetWorld()))
	{
		const FTransform Transform = FTransform(GetActorRotation(), FVector(GetActorLocation().X, GetActorLocation().Y, 45.f));
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParameters.Owner = this;
	
		PickupActorPtr = GetWorld() -> SpawnActor<AMMPickupActor>(AMMPickupActor::StaticClass(), Transform, SpawnParameters);

		if (IsValid(PickupActorPtr))
		{
			PickupActorPtr -> AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
			PickupActorPtr -> SetActorHiddenInGame(true);
		}
	}
}

bool AMMItemSpawnPoint::SpawnItem(TWeakObjectPtr<UMMItem> _Item)
{
	if (IsValid(PickupActorPtr) && _Item.IsValid())
	{
		bIsActive = PickupActorPtr -> InitializePickup(this, _Item);

		if (bIsActive)
		{
			PickupActorPtr -> SetActorHiddenInGame(false);
			SetItemRotationTimer();
			
			return bIsActive;
		}

		return bIsActive;
	}

	return bIsActive;
}

void AMMItemSpawnPoint::DeactivateItem()
{
	if (IsValid(GetWorld()))
	{
		if (GetWorld() -> GetTimerManager().TimerExists(ItemRotationTimerHandle))
		{
			GetWorld() -> GetTimerManager().ClearTimer(ItemRotationTimerHandle);
		}

		bIsActive = false;
	}
}

void AMMItemSpawnPoint::SetItemRotationTimer()
{
	if (IsValid(GetWorld()))
	{
		if (!GetWorld() -> GetTimerManager().TimerExists(ItemRotationTimerHandle))
		{
			GetWorld() -> GetTimerManager().SetTimer(ItemRotationTimerHandle, this, &ThisClass::ItemRotation, RotationUpdateRate, true);
		}
	}
}

void AMMItemSpawnPoint::ItemRotation()
{
	if (IsValid(PickupActorPtr))
	{
		const FRotator Rotation = PickupActorPtr -> GetActorRotation();
		const FRotator TargetRotation = FRotator(Rotation.Pitch, Rotation.Yaw + RotationAngle, Rotation.Roll);
		const FRotator NewRotation = FMath::RInterpConstantTo(Rotation, TargetRotation, GetWorld() -> GetDeltaSeconds(), RotationInterpSpeed);
		

		PickupActorPtr -> SetActorRotation(NewRotation);
	}
}
