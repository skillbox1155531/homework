// Fill out your copyright notice in the Description page of Project Settings.


#include "Environment/MMDestructibleObject.h"
#include "Components/BoxComponent.h"
#include "Projectiles/MMProjectileBase.h"
#include "Visuals/MMDamageVisual.h"

// Sets default values
AMMDestructibleObject::AMMDestructibleObject()
{
	DamagedMesh = CreateDefaultSubobject<UGeometryCollectionComponent>("Damaged Mesh");
	DamagedMesh -> SetVisibility(false);
	DamagedMesh -> SetSimulatePhysics(false);
	DamagedMesh -> SetupAttachment(RootComponent);

	bIsMultiShot = false;
	CurrentShots = 0;
	ShotsToChangeMesh = 0;
	ShotsToDestruct = 0;
	DestroyDelay = 3.5f;
		
	DamageVisualsSpawnAmount = 5;
	DamageVisualOffset = 10.f;
}

void AMMDestructibleObject::BeginPlay()
{
	Super::BeginPlay();

	if (DamagedMesh)
	{
		DamagedMesh -> SetVisibility(false);
		
		if(ForceDirection == EMMDestructionDirection::DD_UP)
		{
			Direction = FQuat::Identity.Rotator();
		}
		else
		{
			Direction = FRotator(-90.f, 0.f, 0.f);
		}

		SpawnDamageVisualsPool();
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("GeometryCollectionComponent isn't Valid"));
	}
}

void AMMDestructibleObject::HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
                                        UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	UE_LOG(LogTemp, Log, TEXT("AMMDestructibleObject: %s Hit"), *GetActorLabel());
	
	if (Cast<AMMProjectileBase>(OtherActor))
	{
		if (bIsMultiShot)
		{
			Super::HandleOnHit(HitComponent, OtherActor, OtherComp, NormalImpulse, Hit);
			
			CurrentShots++;

			if (!bIsDamaged && CurrentShots == ShotsToChangeMesh)
			{
				bIsDamaged = true;
				EnableDestructibleMesh();
			}
			else if(CurrentShots == ShotsToDestruct)
			{
				Destruct();
			}
		}
		else
		{
			EnableDestructibleMesh();
			Destruct();
		}
	}
}

void AMMDestructibleObject::EnableDestructibleMesh()
{
	if (Mesh && DamagedMesh)
	{
		Mesh -> SetVisibility(false);
		DamagedMesh -> SetVisibility(true);
	}
}

void AMMDestructibleObject::Destruct()
{
	if (Collision && Mesh && DamagedMesh)
	{
		if (!DamagedMesh -> DamageThreshold.IsEmpty())
		{
			if (SpawnMasterField())
			{
				Collision -> SetCollisionEnabled(ECollisionEnabled::NoCollision);
				DamagedMesh -> SetSimulatePhysics(true);
			
				GetWorldTimerManager().SetTimer(DestroyTimerHandle, [&]
				{
					if (MasterFieldPtr)
					{
						MasterFieldPtr -> Destroy();
					}
					
					Destroy();
					
				}, DestroyDelay, false);
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("Field wasn't spawned"));
			}
		}
	}
}

bool AMMDestructibleObject::SpawnMasterField()
{
	if (MasterFieldClass && GetWorld())
	{
		const FVector Location = GetActorLocation();
		
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Owner = this;
			
		MasterFieldPtr = Cast<AFieldSystemActor>(GetWorld() -> SpawnActor(MasterFieldClass, &Location, &Direction, SpawnParameters));

		if (MasterFieldPtr)
		{
			MasterFieldPtr -> AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);

			return true;
		}
		
		return false;
	}

	return false;
}

float AMMDestructibleObject::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent,
	AController* EventInstigator, AActor* DamageCauser)
{
	const auto Damage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	
	if (IsValid(DamageCauser))
	{
		if (!ShowDamageVisual(Damage))
		{
			SpawnDamageVisual();
			ShowDamageVisual(Damage);
		}
		
		// if (IsValid(GEngine))
		// {
		// 	const auto Weapon = Cast<AMMWeaponBase>(DamageCauser -> GetOwner());
		// 	GEngine -> AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("Damaged by %s"), *UEnum::GetValueAsString(Weapon -> GetWeaponBaseParameters().GetWeaponType())));
		// }
	}
	
	return 0;
}

void AMMDestructibleObject::SpawnDamageVisualsPool()
{
	for (int i = 0; i < DamageVisualsSpawnAmount; ++i)
	{
		SpawnDamageVisual();
	}
}

void AMMDestructibleObject::SpawnDamageVisual()
{
	const FVector Location = FVector::ZeroVector;
	const FRotator Rotation = FQuat::Identity.Rotator();

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Owner = this;
	
	const auto Object = GetWorld() -> SpawnActor<AMMDamageVisual>(AMMDamageVisual::StaticClass(), Location, Rotation, SpawnParameters);

	if (IsValid(Object))
	{
		Object -> AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		DamageVisualsArr.Emplace(Object);
	}
}

bool AMMDestructibleObject::ShowDamageVisual(const float& DamageAmount)
{
	if (!DamageVisualsArr.IsEmpty())
	{
		for (const auto DamageVisual : DamageVisualsArr)
		{
			if (IsValid(DamageVisual) && !DamageVisual -> GetIsActive())
			{
				const FVector Location = GetActorLocation() + FVector(FMath::RandRange(-DamageVisualOffset, DamageVisualOffset), FMath::RandRange(-DamageVisualOffset, DamageVisualOffset), 0.f);
				DamageVisual -> SetActorLocation(Location);
				DamageVisual -> ShowDamage(DamageAmount);
				return true;
			}
		}
	}

	return false;
}