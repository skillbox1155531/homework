// Fill out your copyright notice in the Description page of Project Settings.

#include "Environment/MMEnvironmentalObjectBase.h"
#include "Base/MMBaseHitHandlerComponent.h"
#include "Components/BoxComponent.h"
#include "Core/MMGameInstance.h"
#include "Projectiles/MMProjectileBase.h"

// Sets default values
AMMEnvironmentalObjectBase::AMMEnvironmentalObjectBase()
{
	Root = CreateDefaultSubobject<USceneComponent>("Root");
	RootComponent = Root;
	
	Collision = CreateDefaultSubobject<UBoxComponent>("Collision");
	Collision -> SetupAttachment(RootComponent);
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh -> SetVisibility(true);
	Mesh -> SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh -> SetupAttachment(RootComponent);

	HitHandlerComponent = CreateDefaultSubobject<UMMBaseHitHandlerComponent>(TEXT("Hit Handler Component"));
	AddOwnedComponent(HitHandlerComponent);
}

// Called when the game starts or when spawned
void AMMEnvironmentalObjectBase::BeginPlay()
{
	Super::BeginPlay();

	if (HitHandlerComponent)
	{
		if (Cast<UMMGameInstance>(GetGameInstance()))
		{
			const TWeakObjectPtr<UMMGameInstance> GameInstance = Cast<UMMGameInstance>(GetGameInstance());
			HitHandlerComponent -> LoadImpactFX(GameInstance);
			Collision -> OnComponentHit.AddDynamic(this, &ThisClass::HandleOnHit);
		}
	}
	//SpawnDamageVisualsPool();
	// UE_LOG(LogTemp, Log, TEXT("BeginPlay: %s"), *GetActorLabel());
	

	//Mesh -> OnComponentBeginOverlap.AddDynamic(this, &ThisClass::HandleOverlap);
}

void AMMEnvironmentalObjectBase::HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	UE_LOG(LogTemp, Log, TEXT("AMMEnvironmentalObjectBase: %s Hit"), *GetActorLabel());
	
	if (Cast<AMMProjectileBase>(OtherActor))
	{
		if (HitHandlerComponent)
		{
			if (!HitHandlerComponent -> ShowImpactVisual(Hit.Location))
			{
				UE_LOG(LogTemp, Error, TEXT(" AMMEnvironmentalObjectBase::HandleOnHit: Impact Error"));
			}
		}
	}
}