// Fill out your copyright notice in the Description page of Project Settings.

#include "Player/MMCharacter.h"

#include "InputActionValue.h"
#include "MMPlayerController.h"
#include "Animations/MMCharacterAnimInstance.h"
#include "Base/MMBaseHitHandlerComponent.h"
#include "Camera/CameraComponent.h"
#include "Character/MMCharacterHealthComponent.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/DecalComponent.h"
#include "Components/SphereComponent.h"
#include "Enums/MMMovementState.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Inventory/WMMInventory.h"
#include "Items/MMPickupActor.h"
#include "Components/Character/MMInventoryComponent.h"
#include "Components/Character/MMWeaponsHandlerComponent.h"
#include "HUD/WMMCurrentWeapon.h"
#include "HUD/WMMBar.h"
#include "Weapons/MMWeaponInfo.h"
#include "Items/MMItemBase.h"
#include "Items/MMItem.h"

AMMCharacter::AMMCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera Boom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->TargetArmLength = 1000.f;
	CameraBoom->SetRelativeRotation(FRotator(-80.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false;
	
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = false;

	DecalComponent = CreateDefaultSubobject<UDecalComponent>(TEXT("Mouse Cursor"));
	DecalComponent -> SetupAttachment(RootComponent);

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Detection Sphere"));
	SphereComponent -> SetupAttachment(RootComponent);

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Interaction Box"));
	BoxComponent -> bAutoActivate = false;
	BoxComponent -> SetActive(false);
	BoxComponent -> SetBoxExtent(FVector(30.f, 30.f, 70.f));
	BoxComponent -> SetRelativeLocation(FVector(150.f, 0.f, -15.f));
	BoxComponent -> SetupAttachment(RootComponent);

	ShellMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>("Shell");
	ShellMeshComponent -> SetupAttachment(RootComponent);
	ShellMeshComponent -> SetVisibility(false);
	ShellMeshComponent -> SetLeaderPoseComponent(GetMesh());
	
	HealthComponent = CreateDefaultSubobject<UMMCharacterHealthComponent>(TEXT("Health"));
	AddOwnedComponent(HealthComponent);
	
	InventoryComponent = CreateDefaultSubobject<UMMInventoryComponent>(TEXT("Inventory"));
	AddOwnedComponent(InventoryComponent);

	WeaponsHandlerComponent = CreateDefaultSubobject<UMMWeaponsHandlerComponent>(TEXT("Weapons"));
	AddOwnedComponent(WeaponsHandlerComponent);

	HitHandlerComponent = CreateDefaultSubobject<UMMBaseHitHandlerComponent>(TEXT("Hit Handler Component"));
	AddOwnedComponent(HitHandlerComponent);

	ShieldHitHandlerComponent = CreateDefaultSubobject<UMMBaseHitHandlerComponent>(TEXT("Shield Hit Handler Component"));
	AddOwnedComponent(ShieldHitHandlerComponent);
	
	CurrentMovementState = EMMMovementState::MS_Idle;
}

void AMMCharacter::Initialization(const TWeakObjectPtr<AMMPlayerController>& _PlayerController, const TWeakObjectPtr<UWMMInventory>& _InventoryUIPtr,
		const TWeakObjectPtr<UWMMCurrentWeapon>& _CurrentWeaponUIPtr, const TWeakObjectPtr<UWMMBar>& _HealthBarUIPtr, const TWeakObjectPtr<UWMMBar>& _ShieldBarUIPtr)
{
	if (_PlayerController.IsValid())
	{
		PlayerControllerPtr = _PlayerController;

		PlayerControllerPtr -> OnSetMovementState.BindUObject(this, &ThisClass::SetMovementState);
		PlayerControllerPtr -> OnExecuteSwitchWeapon.AddUObject(this, &ThisClass::ExecuteWeaponSwitch);
		PlayerControllerPtr -> OnExecuteWeaponFire.AddUObject(this, &ThisClass::CallExecuteFireOnCurrentWeapon);
		PlayerControllerPtr -> OnExecuteWeaponReload.AddUObject(this, &ThisClass::CallExecuteReloadOnCurrentWeapon);
		PlayerControllerPtr -> OnCharacterInteract.AddUObject(this, &ThisClass::ExecuteInteract);
	}
	
	if (IsValid(Cast<UMMCharacterAnimInstance>(GetMesh() -> GetAnimInstance())))
	{
		CharacterAnimInstancePtr = Cast<UMMCharacterAnimInstance>( GetMesh() -> GetAnimInstance());
	}

	if (IsValid(WeaponsHandlerComponent) && _CurrentWeaponUIPtr.IsValid())
	{
		WeaponsHandlerComponent -> Initialization(TWeakObjectPtr<AMMCharacter>(this), _CurrentWeaponUIPtr);
	}
		
	if (IsValid(InventoryComponent) && _InventoryUIPtr.IsValid())
	{
		InventoryComponent -> Initialization(TWeakObjectPtr<AMMCharacter>(this), _InventoryUIPtr);
		InventoryComponent -> AddDefaultWeaponToInventory(DefaultWeaponID);
	}

	if (IsValid(HealthComponent) && _HealthBarUIPtr.IsValid() && _ShieldBarUIPtr.IsValid())
	{
		HealthComponent -> Initialization(TWeakObjectPtr<AMMCharacter>(this), _HealthBarUIPtr, _ShieldBarUIPtr);
	}

	if (Cast<UMMGameInstance>(GetGameInstance()))
	{
		if (HitHandlerComponent)
		{
			const TWeakObjectPtr<UMMGameInstance> GameInstance = Cast<UMMGameInstance>(GetGameInstance());
			HitHandlerComponent -> LoadImpactFX(GameInstance);
		}

		if (ShieldHitHandlerComponent)
		{
			const TWeakObjectPtr<UMMGameInstance> GameInstance = Cast<UMMGameInstance>(GetGameInstance());
			ShieldHitHandlerComponent -> LoadImpactFX(GameInstance);
		}
		
		GetCapsuleComponent() -> OnComponentHit.AddDynamic(this, &ThisClass::HandleOnHit);
	}
}

void AMMCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (IsValid(SphereComponent))
	{
		SphereComponent -> OnComponentBeginOverlap.AddDynamic(this, &ThisClass::HandleStartOverlap);
		SphereComponent -> OnComponentEndOverlap.AddDynamic(this, &ThisClass::HandleEndOverlap);
	}

	if (IsValid(BoxComponent))
	{
		BoxComponent -> OnComponentBeginOverlap.AddDynamic(this, &ThisClass::HandleStartInteract);
		BoxComponent -> OnComponentEndOverlap.AddDynamic(this, &ThisClass::HandleEndInteract);
	}
}

float AMMCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	const float Damage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	
	if (IsValid(HealthComponent))
	{
		HealthComponent -> DecreaseHealth(Damage);	
	}
	
	return 0;
}

void AMMCharacter::HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	FVector NormalImpulse, const FHitResult& Hit)
{
	if (HealthComponent)
	{
		if (HealthComponent -> GetIsShielded())
		{
			if (ShieldHitHandlerComponent)
			{
				ShieldHitHandlerComponent -> ShowImpactVisual(Hit.Location);
			}
		}
		else
		{
			if (HitHandlerComponent)
			{
				HitHandlerComponent -> ShowImpactVisual(Hit.Location);
			}
		}
	}
}

void AMMCharacter::ExecuteWeaponSwitch(const FInputActionValue& InputActionValue)
{
	if (IsValid(WeaponsHandlerComponent))
	{
		WeaponsHandlerComponent -> WeaponSwitch(InputActionValue);
	}
}

void AMMCharacter::ExecuteSetWeapon( const TWeakObjectPtr<UMMItemBase> Item, const FMMWeaponInfo& _WeaponInfo, const int8& Index)
{
	if (IsValid(WeaponsHandlerComponent))
	{
		WeaponsHandlerComponent -> SetWeapon(Item, _WeaponInfo, Index);
	}
}

void AMMCharacter::ExecuteSetCurrentWeaponUI(UTexture2D* Image, const int8& AmmoInClip,
	const int32& AmmoInReserve)
{
	if (IsValid(WeaponsHandlerComponent))
	{
		WeaponsHandlerComponent -> SetCurrentWeaponUI(Image, AmmoInClip, AmmoInReserve);
	}
}

void AMMCharacter::UpdateWeaponInfoInInventory(const int32& _Id, const FMMWeaponBaseParams& BaseParams, const FMMWeaponTrackingInfo& TrackingInfo) const
{
	if (IsValid(InventoryComponent))
	{
		InventoryComponent -> UpdateWeaponInfo(_Id, BaseParams, TrackingInfo);
	}
}

EMMWeaponType AMMCharacter::GetCurrentWeaponType() const
{
	if (IsValid(WeaponsHandlerComponent))
	{
		return WeaponsHandlerComponent -> GetCurrentWeaponType();
	}

	return EMMWeaponType();
}

TWeakObjectPtr<AMMWeaponBase> AMMCharacter::GetSecondaryWeapon() const
{
	if (IsValid(WeaponsHandlerComponent))
	{
		return WeaponsHandlerComponent -> GetSecondaryWeapon();
	}

	return TWeakObjectPtr<AMMWeaponBase>(nullptr);
}

TWeakObjectPtr<AMMWeaponBase> AMMCharacter::GetHeavyWeapon() const
{
	if (IsValid(WeaponsHandlerComponent))
	{
		return WeaponsHandlerComponent -> GetHeavyWeapon();
	}

	return TWeakObjectPtr<AMMWeaponBase>(nullptr);
}

void AMMCharacter::CallExecuteFireOnCurrentWeapon(const FInputActionValue& InputActionValue)
{
	if (IsValid(WeaponsHandlerComponent))
	{
		WeaponsHandlerComponent -> FireCurrentWeapon(InputActionValue, CurrentMovementState, OnUpdateWeaponTrackingInfo);
	}
}

void AMMCharacter::CallExecuteReloadOnCurrentWeapon(const FInputActionValue& InputActionValue)
{
	if (IsValid(WeaponsHandlerComponent))
	{
		WeaponsHandlerComponent -> StartReloadCurrentWeapon(InputActionValue);
	}
}

void AMMCharacter::HandleStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                      UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	

	if (OtherActor -> Implements<UInteractable>())
	{
		const auto& Pickup = Cast<IInteractable>(OtherActor);
		
		if (Pickup)
		{
			Pickup -> StartFocus();

			PickupsInRange.FindOrAdd(OtherActor);

			if (!PickupsInRange.IsEmpty() & IsValid(BoxComponent))
			{
				BoxComponent -> SetActive(true);
			}
		}
	}
}

void AMMCharacter::HandleEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor -> Implements<UInteractable>())
	{
		const auto& Pickup = Cast<IInteractable>(OtherActor);
		
		Pickup -> EndFocus();

		PickupsInRange.FindAndRemoveChecked(OtherActor);

			if (PickupsInRange.IsEmpty() && IsValid(BoxComponent))
			{
				BoxComponent -> SetActive(false);
			}
	}
}

void AMMCharacter::HandleStartInteract(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor -> Implements<UInteractable>())
	{
		CurrentPickup = Cast<IInteractable>(OtherActor);
	
		if (CurrentPickup)
		{
			if (CurrentPickup -> GetItemType() == EMMItemType::IT_Ammo && IsValid(WeaponsHandlerComponent))
			{
				const auto Item = CurrentPickup -> Interact();

				if (Item.Key.IsValid() && Item.Value.IsValid())
				{
					WeaponsHandlerComponent -> AddAmmoToEquippedWeapons(Item.Value.Get() -> GetItemParams<FMMAmmoInfo>().GetBaseParameters().GetAmmoType());
				
					Item.Key.Get() -> Deactivate();
					Item.Value.Get() -> RemoveFromRoot();
					CurrentPickup = nullptr;
				}
			}
			else if(CurrentPickup -> GetItemType() == EMMItemType::IT_Consumable && IsValid(HealthComponent))
			{
				const auto Item = CurrentPickup -> Interact();

				if (Item.Key.IsValid() && Item.Value.IsValid())
				{
					if (Item.Value.Get() -> GetItemParams<FMMConsumableInfo>().GetBaseParameters().GetType() == EMMConsumableType::CT_Health)
					{
						HealthComponent -> IncreaseHealth(Item.Value.Get() -> GetItemParams<FMMConsumableInfo>().GetBaseParameters().GetAmount());
					}
					else
					{
						HealthComponent -> IncreaseShield(Item.Value.Get() -> GetItemParams<FMMConsumableInfo>().GetBaseParameters().GetAmount());
					}

					Item.Key.Get() -> Deactivate();
					Item.Value.Get() -> RemoveFromRoot();
					CurrentPickup = nullptr;
				}
			}
			else
			{
				CurrentPickup -> StartInteract();
			}
		}
	}
}

void AMMCharacter::HandleEndInteract(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor -> Implements<UInteractable>())
	{
		if (CurrentPickup == Cast<IInteractable>(OtherActor))
		{
			if (CurrentPickup)
			{
				CurrentPickup -> EndInteract();
			}
		}
		else
		{
			const auto& Pickup = Cast<IInteractable>(OtherActor);

			if (Pickup)
			{
				Pickup -> EndInteract();
			}
		}
	}
}

void AMMCharacter::ExecuteInteract(const FInputActionValue& InputActionValue)
{
	if (InputActionValue.Get<bool>() && CurrentPickup)
	{
		const auto Item = CurrentPickup -> Interact();
		
		if (Item.Key.IsValid() && Item.Value.IsValid() && IsValid(InventoryComponent))
		{
			InventoryComponent -> AddItemToInventory(Item.Value.Get());
			Item.Key.Get() -> Deactivate();
		}
		
		CurrentPickup = nullptr;
	}
}

//to do Reset Fire when start sprinting.
void AMMCharacter::SetMovementState(const EMMMovementState& MovementState)
{
	if (IsValid(GetCharacterMovement()) && IsValid(WeaponsHandlerComponent))
	{
		if (MovementState == EMMMovementState::MS_Idle)
		{
			CurrentMovementState = MovementState;
			WeaponsHandlerComponent -> SetCurrentDispersionParams(MovementState);
		}
		else if (MovementState == EMMMovementState::MS_Aim)
		{
			CurrentMovementState = MovementState;
			GetCharacterMovement() -> MaxWalkSpeed = MovementParams.GetAimSpeed();
			WeaponsHandlerComponent -> SetCurrentDispersionParams(MovementState);
		}
		else if (MovementState == EMMMovementState::MS_Walk)
		{
			if (CurrentMovementState != EMMMovementState::MS_Aim)
			{
				CurrentMovementState = MovementState;
				GetCharacterMovement() -> MaxWalkSpeed = MovementParams.GetWalkSpeed();
				WeaponsHandlerComponent -> SetCurrentDispersionParams(MovementState);
			}
		}
		else
		{
			CurrentMovementState = MovementState;
			GetCharacterMovement() -> MaxWalkSpeed = MovementParams.GetSprintSpeed();
		}
	}
}