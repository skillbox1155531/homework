// Fill out your copyright notice in the Description page of Project Settings.

#include "Player/MMPlayerController.h"
#include "InputActionValue.h"
#include "Player/MMCharacter.h"
#include "Animations/MMCharacterAnimInstance.h"
#include "Blueprint/UserWidget.h"
#include "Components/DecalComponent.h"
#include "Components/PlayerController/MMCameraHandlerComponent.h"
#include "Components/PlayerController/MMInputHandlerComponent.h"
#include "Components/PlayerController/MMMovementHandlerComponent.h"
#include "Core/MMGameInstance.h"
#include "Enums/MMMovementState.h"
#include "GameFramework/SpringArmComponent.h"
#include "HUD/WMMBar.h"
#include "HUD/WMMCurrentWeapon.h"
#include "Inventory/WMMInventory.h"
#include "Kismet/GameplayStatics.h"
#include "Objects/WMMAchievementBlock.h"
#include "PlayerController/MMAchievementsTrackerComponent.h"
#include "PlayerController/MMAmmoRequestHandlerComponent.h"
#include "PlayerController/MMConsumableRequestHandlerComponent.h"

AMMPlayerController::AMMPlayerController()
{
	bCanSprint = false;

	DistanceThresholdMultiplayer = 0.5f;
	CameraDirectionMultiplayer = 10.f;
	CameraDistanceTolerance = 5.5f;
	CameraZeroDistance = 0.f;

	SecondaryAmmoThreshold = .35f;
	HeavyAmmoThreshold = .1f;
	
	StaminaReduceValue = 10.f;
	StaminaRestoreValue = 10.f;
	StaminaRestoreRate = 2.f;
	StaminaRestoreDelay = 5.f;
	
	InputHandlerComponent = CreateDefaultSubobject<UMMInputHandlerComponent>(TEXT("Input Handler Component"));
	AddOwnedComponent(InputHandlerComponent);
	MovementHandlerComponent = CreateDefaultSubobject<UMMMovementHandlerComponent>(TEXT("Movement Handler Component"));
	AddOwnedComponent(MovementHandlerComponent);
	CameraHandlerComponent = CreateDefaultSubobject<UMMCameraHandlerComponent>(TEXT("Camera Handler Component"));
	AddOwnedComponent(CameraHandlerComponent);
	AchievementsTrackerComponent = CreateDefaultSubobject<UMMAchievementsTrackerComponent>(TEXT("Achievements Tracker Component"));
	AddOwnedComponent(AchievementsTrackerComponent);
	AmmoRequestHandlerComponent = CreateDefaultSubobject<UMMAmmoRequestHandlerComponent>(TEXT("Ammo Request Handler Component"));
	AddOwnedComponent(AmmoRequestHandlerComponent);
	ConsumableRequestHandlerComponent = CreateDefaultSubobject<UMMConsumableRequestHandlerComponent>(TEXT("Consumable Request Handler Component"));
	AddOwnedComponent(ConsumableRequestHandlerComponent);
}

void AMMPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (IsValid(Cast<AMMCharacter>(GetPawn())))
	{
		CharacterPtr = Cast<AMMCharacter>(GetPawn());

		const TWeakObjectPtr<UMMGameInstance> GameInstancePtr = Cast<UMMGameInstance>(GetGameInstance());
		const TWeakObjectPtr<AMMGameMode> GameModePtr = Cast<AMMGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		const TWeakObjectPtr<AMMItemSpawner> ItemSpawnerPtr = GameModePtr.Get() -> GetItemSpawner();
		
		if (GameInstancePtr.IsValid() && GameModePtr.IsValid())
		{
			if (CharacterPtr.IsValid())
			{
				const TWeakObjectPtr<UWMMInventory> InventoryUIPtr = CreateWidget<UWMMInventory>(this, LoadClass<UWMMInventory>(this, *InventoryUIClassPath));
				const TWeakObjectPtr<UWMMCurrentWeapon> CurrentWeaponUIPtr =CreateWidget<UWMMCurrentWeapon>(this, LoadClass<UWMMCurrentWeapon>(this, *CurrentWeaponUIClassPath));
				const TWeakObjectPtr<UWMMBar> HealthBarUIPtr = CreateWidget<UWMMBar>(this, LoadClass<UWMMBar>(this, *HealthBarUIClassPath));
				const TWeakObjectPtr<UWMMBar> ShieldBarUIPtr = CreateWidget<UWMMBar>(this, LoadClass<UWMMBar>(this, *ShieldBarUIClassPath));
				CharacterPtr.Get() -> Initialization(this, InventoryUIPtr, CurrentWeaponUIPtr, HealthBarUIPtr, ShieldBarUIPtr);
			}
			
			if (IsValid(AchievementsTrackerComponent) && ItemSpawnerPtr.IsValid())
			{
				const TWeakObjectPtr<UWMMAchievementBlock> AchievementBlockPtr = CreateWidget<UWMMAchievementBlock>(this, LoadClass<UWMMAchievementBlock>(this, *AchievementBlockClassPath));
				
				AchievementsTrackerComponent -> Initialization(GameInstancePtr, TWeakObjectPtr<AMMPlayerController>(this),
					ItemSpawnerPtr, AchievementBlockPtr);
			}
			
			if (IsValid(AmmoRequestHandlerComponent) && ItemSpawnerPtr.IsValid())
			{
				AmmoRequestHandlerComponent -> Initialization(GameInstancePtr, TWeakObjectPtr<AMMPlayerController>(this), ItemSpawnerPtr);
			}

			if (IsValid(ConsumableRequestHandlerComponent) && ItemSpawnerPtr.IsValid())
			{
				ConsumableRequestHandlerComponent -> Initialization(GameInstancePtr, TWeakObjectPtr<AMMPlayerController>(this), ItemSpawnerPtr);
			}
		}
		
		if (IsValid(Cast<UMMCharacterAnimInstance>(CharacterPtr.Get() -> GetMesh() -> GetAnimInstance())))
		{
			CharacterAnimInstancePtr = Cast<UMMCharacterAnimInstance>(CharacterPtr.Get() -> GetMesh() -> GetAnimInstance());
		}
	}

	OnCharacterAim.AddUObject(this, &ThisClass::SetIsAim);
	
	SetGameInputMode();
}

void AMMPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	
	TrackCursorPosition(DeltaSeconds);
	TrackCursorDistance(DeltaSeconds);
	TrackCharacterDirectionForSprint();
	TrackCharacterSprint();
	TrackCharacterIdle();
	UpdateAnimInstanceParams();
	UpdateCursorDecalPosition();
	CallCameraAimZoom(DeltaSeconds);
}

void AMMPlayerController::SetGameInputMode()
{
	FInputModeGameOnly InputMode;
	InputMode.SetConsumeCaptureMouseDown(false);
	SetInputMode(InputMode);
	SetShowMouseCursor(true);
}

void AMMPlayerController::TrackCursorPosition(const float& DeltaSeconds)
{
	FVector MouseLocation, MouseDirection;
	
	if (DeprojectMousePositionToWorld(MouseLocation, MouseDirection))
	{
		if (CharacterPtr.IsValid())
		{
			const FRotator YawRotation(0.f, MouseDirection.Rotation().Yaw, 0.f);
			CharacterPtr.Get() -> SetActorRotation(FMath::RInterpConstantTo(CharacterPtr.Get() -> GetActorRotation(), YawRotation, DeltaSeconds, 200.f));;
		}
	}
}

void AMMPlayerController::TrackCursorDistance(const float& DeltaSeconds)
{
	if (CharacterPtr.IsValid() && IsValid(CharacterPtr.Get() -> GetMouseCursorDecal()) && IsValid(CharacterPtr.Get() -> GetSpringArm() ))
	{
		const FVector2d CharLoc = FVector2d(CharacterPtr.Get() -> GetActorLocation());
		const FVector2d DecalLoc = FVector2d(CharacterPtr.Get() -> GetMouseCursorDecal() -> GetComponentLocation());
		const FVector CamLoc = CharacterPtr.Get() -> GetSpringArm() -> GetComponentLocation();

		const float CamToCharDistance = FVector2d::Distance(CharLoc, FVector2d(CamLoc));
		const float CharToDecalDistance = FVector2d::Distance(CharLoc, DecalLoc);
		
		if (CharacterPtr.Get() -> GetCurrentMovementState() != EMMMovementState::MS_Sprint)
		{
			const float CursorDistanceThreshold = CharacterPtr.Get() -> GetSpringArm() -> TargetArmLength * DistanceThresholdMultiplayer;
			const float CameraDistanceThreshold = CursorDistanceThreshold * DistanceThresholdMultiplayer;

			if (CharToDecalDistance >= CursorDistanceThreshold)
			{
				const FVector Direction = FRotationMatrix(FRotator(0.f, CharacterPtr.Get() -> GetActorRotation().Yaw, 0.f)).GetUnitAxis(EAxis::X);

				FVector NewCamLoc;
				CamToCharDistance < CameraDistanceThreshold
					? NewCamLoc = FVector(CamLoc + (Direction * CameraDirectionMultiplayer))
					: NewCamLoc = FVector(CamLoc - (Direction * CameraDirectionMultiplayer));
				
				CallSetCameraPosition(CamToCharDistance, CameraDistanceThreshold, NewCamLoc, DeltaSeconds);
			}
			else if (CharToDecalDistance <= CameraDistanceThreshold)
			{
				const FVector NewCamLoc = FVector(CharLoc.X, CharLoc.Y, CamLoc.Z);
				
				CallSetCameraPosition(CamToCharDistance, CameraZeroDistance, NewCamLoc, DeltaSeconds);
			}
		}
		else
		{
			const FVector NewCamLoc = FVector(CharacterPtr.Get() -> GetActorLocation().X, CharacterPtr.Get() -> GetActorLocation().Y, CharacterPtr.Get() -> GetSpringArm() -> GetComponentLocation().Z);

			CallSetCameraPosition(CamToCharDistance, CameraZeroDistance, NewCamLoc, DeltaSeconds);
		}
	}
}


void AMMPlayerController::CallSetCameraPosition(const float& FromDistance, const float& ToDistance,
                                                const FVector& TargetPosition, const float& DeltaSeconds)
{
	if (!FMath::IsNearlyEqual(FromDistance, ToDistance, CameraDistanceTolerance))
	{
		OnCameraSetPosition.ExecuteIfBound(TargetPosition, DeltaSeconds);	
	}
}

void AMMPlayerController::UpdateCursorDecalPosition()
{
	if (CharacterPtr.IsValid() && IsValid(CharacterPtr.Get() -> GetMouseCursorDecal()))
	{
		FHitResult HitResult;
		GetHitResultUnderCursor(ECC_GameTraceChannel1, false, HitResult);
		
		CharacterPtr.Get() -> GetMouseCursorDecal() -> SetWorldLocation(HitResult.Location);
		CharacterPtr.Get() -> GetMouseCursorDecal() -> SetWorldRotation(HitResult.Normal.Rotation());
	}
}

void AMMPlayerController::TrackCharacterDirectionForSprint()
{
	if (CharacterPtr.IsValid())
	{
		if (CharacterStats.GetStamina() -> GetCurrentValue() > 0.f)
		{
			const FVector ForwardVector = CharacterPtr.Get() -> GetActorForwardVector();
			const FVector Velocity = CharacterPtr.Get() -> GetVelocity().GetSafeNormal();
			const auto Direction = FVector::DotProduct(ForwardVector, Velocity);

			Direction > 0.1 ? bCanSprint = true : bCanSprint = false;
		}
	}
}

void AMMPlayerController::UpdateAnimInstanceParams()
{
	if (CharacterAnimInstancePtr.IsValid() && CharacterPtr.IsValid())
	{
		// UE_LOG(LogTemp, Warning, TEXT("AMMPlayerController: Current Movement State: %s"), *UEnum::GetValueAsString(CharacterPtr.Get() -> GetCurrentMovementState()));
		CharacterAnimInstancePtr.Get() -> SetAnimParams(true, CharacterPtr.Get() -> GetVelocity(),
		                                          CharacterPtr.Get() -> GetActorRotation(), CharacterPtr.Get() -> GetCurrentMovementState(), CharacterPtr.Get() -> GetCurrentWeaponType());
	}
}

void AMMPlayerController::CallCameraAimZoom(const float& DeltaSeconds)
{
	if (bIsAiming)
	{
		OnCameraAimZoom.ExecuteIfBound(bIsAimZoomIn, FInputActionValue(DeltaSeconds));
	}
}

void AMMPlayerController::SetIsAim(const FInputActionValue& InputActionValue)
{
	bIsAiming = true;
	bIsAimZoomIn = InputActionValue.Get<bool>();
}

void AMMPlayerController::TrackCharacterSprint()
{
	if (!bIsSprinting && CharacterStats.GetStamina())
	{
		const FMMCharacterStatParams* Stamina = CharacterStats.GetStamina();
		
		if (Stamina -> GetCurrentValue() > 0.f && Stamina -> GetCurrentValue() < Stamina -> GetMaxValue())
		{
			if (GetWorld() && !GetWorld() -> GetTimerManager().TimerExists(StaminaRestoreTimerHandle))
			{
				GetWorld() -> GetTimerManager().SetTimer(StaminaRestoreTimerHandle, this, &ThisClass::RestoreCharacterStamina, StaminaRestoreRate, true);
			}
		}
	}
}

void AMMPlayerController::TrackCharacterIdle()
{
	if(CharacterPtr.IsValid())
	{
		if (FMath::IsNearlyEqual(CharacterPtr.Get() -> GetVelocity().Length(), 0) && CharacterPtr.Get() -> GetCurrentMovementState() != EMMMovementState::MS_Aim)
		{
			CharacterPtr.Get() -> SetCurrentMovementState(EMMMovementState::MS_Idle);
		}
	}
}

void AMMPlayerController::ReduceCharacterStamina()
{
	if (IsValid(GetWorld()))
	{
		if (CharacterStats.GetStamina() && CharacterStats.GetStamina() -> GetCurrentValue() > 0.f)
		{
			FMMCharacterStatParams* Stamina = CharacterStats.GetStamina();
			const float StaminaReduceAmount = Stamina -> GetCurrentValue() - (StaminaReduceValue * GetWorld() -> GetDeltaSeconds());
			
			if (GetWorld() -> GetTimerManager().TimerExists(StaminaRestoreTimerHandle))
			{
				GetWorld() -> GetTimerManager().ClearTimer(StaminaRestoreTimerHandle);
			}
			
			Stamina -> SetCurrentValue(FMath::Max(StaminaReduceAmount, Stamina -> GetMinValue()));
		}
		else
		{
			bCanSprint = false;
			GetWorld() -> GetTimerManager().SetTimer(StaminaRestoreTimerHandle, this,
			                                           &ThisClass::RestoreCharacterStamina, StaminaRestoreRate, true, StaminaRestoreDelay);
		}
	}
}

void AMMPlayerController::RestoreCharacterStamina()
{
	if(CharacterStats.GetStamina())
	{
		FMMCharacterStatParams* Stamina = CharacterStats.GetStamina();
		const float StaminaRestoreAmount = Stamina -> GetCurrentValue() + StaminaRestoreValue;
		
		Stamina -> SetCurrentValue(FMath::Min(StaminaRestoreAmount, Stamina -> GetMaxValue()));

		if (Stamina -> GetCurrentValue() == Stamina -> GetMaxValue())
		{
			if (IsValid(GetWorld()))
			{
				GetWorld() -> GetTimerManager().ClearTimer(StaminaRestoreTimerHandle);
			}
		}
	}
}