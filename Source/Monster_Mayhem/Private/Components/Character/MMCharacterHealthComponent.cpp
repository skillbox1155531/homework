// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/Character/MMCharacterHealthComponent.h"

#include "MMCharacter.h"
#include "HUD/WMMBar.h"

UMMCharacterHealthComponent::UMMCharacterHealthComponent()
{
	bIsShielded = false;
	MaxShield = 100.f;
	MaxShield = 0.f;
	CurrentShield = 0.f;
	DamageReductionValue = 0.f;
	InitShieldSpawnRequestTimer = 5.f;
	MinShieldSpawnRequestTimer = 60.f;
	MaxShieldSpawnRequestTimer = 300.f;
}

void UMMCharacterHealthComponent::Initialization(const TWeakObjectPtr<AMMCharacter>& CharacterPtr, const TWeakObjectPtr<UWMMBar>& _HealthBarPtr, const TWeakObjectPtr<UWMMBar>& _ShieldBarPtr)
{
	if (_HealthBarPtr.IsValid() && _ShieldBarPtr.IsValid())
	{
		HealthBarPtr = _HealthBarPtr;
		ShieldBarPtr = _ShieldBarPtr;
		
		if (HealthBarPtr.IsValid() && ShieldBarPtr.IsValid())
		{
			HealthBarPtr.Get() -> Initialization(MaxHealth);
			HealthBarPtr.Get() -> AddToViewport();
			ShieldBarPtr.Get() -> Initialization(MaxShield);
			ShieldBarPtr.Get() -> AddToViewport();

			bIsShielded ? ShieldBarPtr.Get() -> SetVisibility(ESlateVisibility::Visible) : ShieldBarPtr.Get() -> SetVisibility(ESlateVisibility::Hidden);
		}
	}

	if (CharacterPtr.IsValid() && CharacterPtr.Get() -> GetMesh() && CharacterPtr.Get() -> GetShellMesh().IsValid())
	{
		OnRequestConsumable.AddRaw(&CharacterPtr.Get() -> OnRequestConsumableSpawn, &FOnRequestConsumableSpawn::Broadcast);
		CharacterMeshPtr = CharacterPtr.Get() -> GetMesh();
		ShellMeshPtr = CharacterPtr.Get() -> GetShellMesh();
	}

	CurrentHealth = MaxHealth;
	CurrentShield = MaxShield;
	
	ScheduleShieldSpawnRequest(InitShieldSpawnRequestTimer);
}

bool UMMCharacterHealthComponent::DecreaseHealth(const float& Amount)
{
	// const float DamageReduction = Amount * (DamageReductionValue / 100.f);
	// const float Damage = Amount - DamageReduction;

	// if (CurrentHealth / MaxHealth < 0.3f)
	// {
	// 	OnRequestConsumable.Broadcast(EMMConsumableType::CT_Health);
	// }

	UE_LOG(LogTemp, Log, TEXT("Damage Applied: %f"), Amount);

	if (bIsShielded && ShieldBarPtr.IsValid() && ShellMeshPtr.IsValid())
	{
		ShieldBarPtr.Get() -> DecreaseBarValue(Amount);
		CurrentShield -= Amount;
		
		if (CurrentShield < 0.f)
		{
			const auto Remainder = FMath::Abs(CurrentShield);
			
			HealthBarPtr.Get() -> DecreaseBarValue(Remainder);
			CurrentShield = 0;
			SetIsShielded(false);
			ApplyShieldMaterial(CurrentShield);

			return Super::DecreaseHealth(Remainder);
		}

		return true;
	}
	
	if (HealthBarPtr.IsValid())
	{
		HealthBarPtr.Get() -> DecreaseBarValue(Amount);
		return Super::DecreaseHealth(Amount);
	}

	throw std::runtime_error("Something wrong"); 
}

void UMMCharacterHealthComponent::IncreaseHealth(const float& Amount)
{
	Super::IncreaseHealth(Amount);

	if (ShieldBarPtr.IsValid())
	{
		HealthBarPtr.Get() -> IncreaseBarValue(Amount);
	}
}

void UMMCharacterHealthComponent::IncreaseShield(const float& Amount)
{
	if (ShieldBarPtr.IsValid())
	{
		if (!bIsShielded)
		{
			SetIsShielded(true);
			MaxShield = Amount;
			CurrentShield = MaxShield;
			ShieldBarPtr.Get() -> SetBarValue(MaxShield);
			ApplyShieldMaterial(Amount);
		}
		else
		{
			if (MaxShield < Amount)
			{
				MaxShield = Amount;
				CurrentShield = MaxShield;
				ShieldBarPtr.Get() -> SetBarValue(MaxShield);
				ApplyShieldMaterial(Amount);
			}
			else if (MaxShield >= Amount)
			{
				CurrentShield = FMath::Min(CurrentShield + Amount, MaxShield);
				ShieldBarPtr.Get() -> IncreaseBarValue(Amount);
			}
		}
	}
}

void UMMCharacterHealthComponent::SetIsShielded(const bool& _bIsShielded)
{
	if(ShellMeshPtr.IsValid() && ShieldBarPtr.IsValid())
	{
		bIsShielded = _bIsShielded;
		ShellMeshPtr.Get() -> SetVisibility(_bIsShielded);
		bIsShielded ? ShieldBarPtr.Get() -> SetVisibility(ESlateVisibility::Visible) : ShieldBarPtr.Get() -> SetVisibility(ESlateVisibility::Hidden);
	}
}

void UMMCharacterHealthComponent::ScheduleShieldSpawnRequest(const float& Timer)
{
	if (GetWorld())
	{
		if (GetWorld() -> GetTimerManager().TimerExists(ShieldSpawnRequestTimerHandler))
		{
			GetWorld() -> GetTimerManager().ClearTimer(ShieldSpawnRequestTimerHandler);
		}
		
		GetWorld() -> GetTimerManager().SetTimer(ShieldSpawnRequestTimerHandler, this, &ThisClass::CallShieldSpawnRequest, Timer, false);
	}
}

void UMMCharacterHealthComponent::CallShieldSpawnRequest()
{
	OnRequestConsumable.Broadcast(EMMConsumableType::CT_Shield);
	ScheduleShieldSpawnRequest(FMath::RandRange(MinShieldSpawnRequestTimer, MaxShieldSpawnRequestTimer));
}

void UMMCharacterHealthComponent::ApplyShieldMaterial(const float& Amount)
{
	if (!ShieldsDic.IsEmpty())
	{
		if (ShieldsDic[Amount] != nullptr && ShellMeshPtr.IsValid())
		{
			for (int i = 0; i < ShellMeshPtr.Get() -> GetNumMaterials(); ++i)
			{
				ShellMeshPtr.Get() -> SetMaterial(i, ShieldsDic[Amount]);
			}
		}
	}
}