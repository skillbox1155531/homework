// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/Character/MMWeaponsHandlerComponent.h"
#include "InputActionValue.h"
#include "Player/MMCharacter.h"
#include "Animations/MMCharacterAnimInstance.h"
#include "Components/DecalComponent.h"
#include "HUD/WMMCurrentWeapon.h"
#include "Items/MMItem.h"
#include "Weapons/MMWeaponBase.h"

UMMWeaponsHandlerComponent::UMMWeaponsHandlerComponent()
{
	CurrentWeaponIndex = 0;
	EquippedWeaponsArr.SetNum(3, false);
}

void UMMWeaponsHandlerComponent::Initialization(const TWeakObjectPtr<AMMCharacter>& _CharacterPtr, const TWeakObjectPtr<UWMMCurrentWeapon>& CurrentWeaponUI)
{
	if (_CharacterPtr.IsValid())
	{
		CharacterPtr = _CharacterPtr;

		if (IsValid(Cast<UMMCharacterAnimInstance>(CharacterPtr.Get() -> GetMesh() -> GetAnimInstance())))
		{
			CharacterAnimInstancePtr = Cast<UMMCharacterAnimInstance>(CharacterPtr.Get() -> GetMesh() -> GetAnimInstance());
		}
	}

	CurrentWeaponUIPtr = CurrentWeaponUI;
	
	if (CurrentWeaponUIPtr.IsValid())
	{
		CurrentWeaponUIPtr.Get() -> AddToViewport();
	}

	InitializeWeaponSlots();
}

void UMMWeaponsHandlerComponent::InitializeWeaponSlots()
{
	if (CharacterPtr.IsValid())
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParameters.Owner = CharacterPtr.Get();
		SpawnParameters.Instigator = CharacterPtr.Get();

		if (IsValid(GetWorld()))
		{
			DefaultWeapon = Cast<AMMWeaponBase>(GetWorld()->SpawnActor(
				AMMWeaponBase::StaticClass(), &FVector::ZeroVector, &FRotator::ZeroRotator, SpawnParameters));
			SecondaryWeapon = Cast<AMMWeaponBase>(GetWorld()->SpawnActor(
				AMMWeaponBase::StaticClass(), &FVector::ZeroVector, &FRotator::ZeroRotator, SpawnParameters));
			HeavyWeapon = Cast<AMMWeaponBase>(GetWorld()->SpawnActor(
				AMMWeaponBase::StaticClass(), &FVector::ZeroVector, &FRotator::ZeroRotator, SpawnParameters));

			if (DefaultWeapon.IsValid() && SecondaryWeapon.IsValid() && HeavyWeapon.IsValid())
			{
				const FAttachmentTransformRules AttachmentTransformRules(EAttachmentRule::SnapToTarget, false);

				if (IsValid(CharacterPtr.Get() -> GetMesh()))
				{
					EquippedWeaponsArr[0] = DefaultWeapon;
					DefaultWeapon.Get() -> AttachToComponent(CharacterPtr.Get() -> GetMesh(), AttachmentTransformRules, WeaponSocket);
					EquippedWeaponsArr[1] = SecondaryWeapon;
					SecondaryWeapon.Get() -> AttachToComponent(CharacterPtr.Get() -> GetMesh(), AttachmentTransformRules, WeaponSocket);
					SecondaryWeapon.Get() -> SetActive(false);
					EquippedWeaponsArr[2] = HeavyWeapon;
					HeavyWeapon.Get() -> AttachToComponent(CharacterPtr.Get() -> GetMesh(), AttachmentTransformRules, WeaponSocket);
					HeavyWeapon.Get() -> SetActive(false);

					DefaultWeapon.Get() -> OnReloadWeapon.AddUObject(this, &ThisClass::StartReloadCurrentWeapon);
					DefaultWeapon.Get() -> OnUpdateWeaponUI.AddUObject(this, &ThisClass::UpdateCurrentWeaponUI);
					SecondaryWeapon.Get() -> OnReloadWeapon.AddUObject(this, &ThisClass::StartReloadCurrentWeapon);
					SecondaryWeapon.Get() -> OnUpdateWeaponUI.AddUObject(this, &ThisClass::UpdateCurrentWeaponUI);
					SecondaryWeapon.Get() -> OnRequestWeaponAmmo.AddRaw(&CharacterPtr -> OnRequestWeaponAmmoSpawn, &FOnRequestWeaponAmmoSpawn::Broadcast);
					HeavyWeapon.Get() -> OnReloadWeapon.AddUObject(this, &ThisClass::StartReloadCurrentWeapon);
					HeavyWeapon.Get() -> OnUpdateWeaponUI.AddUObject(this, &ThisClass::UpdateCurrentWeaponUI);
					HeavyWeapon.Get() -> OnRequestWeaponAmmo.AddRaw(&CharacterPtr -> OnRequestWeaponAmmoSpawn, &FOnRequestWeaponAmmoSpawn::Broadcast);
					
					CurrentWeapon = DefaultWeapon;
				}
			}
		}	
	}
}


void UMMWeaponsHandlerComponent::WeaponSwitch(const FInputActionValue& InputActionValue)
{
	if (CharacterAnimInstancePtr.IsValid() && CurrentWeapon.IsValid())
	{
		CharacterAnimInstancePtr.Get() -> StopAllMontages();
		CurrentWeapon.Get() -> ResetWeaponStates();
		CurrentWeapon.Get() -> SetActive(false);
	
		if (InputActionValue.Get<float>() > 0.f)
		{
			SelectNextWeapon();
		}
		else
		{
			SelectPreviousWeapon();
		}

		if (CurrentWeaponUIPtr.IsValid())
		{
			CurrentWeapon.Get() -> SetActive(true);

			UE_LOG(LogTemp, Log, TEXT("UMMWeaponsHandlerComponent::WeaponSwitch: Updating CW UI"));
			
			CurrentWeaponUIPtr.Get() -> SetWeapon(CurrentWeapon.Get() -> GetHUDThumbnail(), CurrentWeapon.Get() -> GetWeaponBaseParameters().GetRoundsInClip(),
			                                CurrentWeapon.Get() -> GetWeaponBaseParameters().GetRoundsInReserve());
		}
	}
}

void UMMWeaponsHandlerComponent::SelectNextWeapon()
{
	CurrentWeaponIndex++;
	
	if (EquippedWeaponsArr.IsValidIndex(CurrentWeaponIndex) && EquippedWeaponsArr[CurrentWeaponIndex] -> GetIsEquipped())
	{
		CurrentWeapon = EquippedWeaponsArr[CurrentWeaponIndex];
	}
	else
	{
		if (CurrentWeaponIndex < EquippedWeaponsArr.Num() - 1)
		{
			SelectNextWeapon();
		}
		else
		{
			CurrentWeaponIndex = 0;
			CurrentWeapon = EquippedWeaponsArr[CurrentWeaponIndex];
		}
	}
}

void UMMWeaponsHandlerComponent::SelectPreviousWeapon()
{
	CurrentWeaponIndex--;
	
	if (EquippedWeaponsArr.IsValidIndex(CurrentWeaponIndex) && EquippedWeaponsArr[CurrentWeaponIndex] -> GetIsEquipped())
	{
		CurrentWeapon = EquippedWeaponsArr[CurrentWeaponIndex];
	}
	else
	{
		if (CurrentWeaponIndex > 0)
		{
			SelectPreviousWeapon();
		}
		else
		{
			if (EquippedWeaponsArr[EquippedWeaponsArr.Num() - 1] -> GetIsEquipped())
			{
				CurrentWeaponIndex = EquippedWeaponsArr.Num() - 1;
				CurrentWeapon = EquippedWeaponsArr[CurrentWeaponIndex];
			}
			else
			{
				CurrentWeaponIndex = 0;
				SelectNextWeapon();
			}
		}
	}
}

void UMMWeaponsHandlerComponent::SetWeapon(const TWeakObjectPtr<UMMItemBase> Item, const FMMWeaponInfo& _WeaponInfo, const int8& Index)
{
	if (Index == 0)
	{
		InitializeWeapon(DefaultWeapon, Item, _WeaponInfo, Index);
	}
	else if (Index == 1)
	{
		InitializeWeapon(SecondaryWeapon, Item, _WeaponInfo, Index);
	}
	else if (Index == 2)
	{
		InitializeWeapon(HeavyWeapon, Item, _WeaponInfo, Index);
	}
}

void UMMWeaponsHandlerComponent::InitializeWeapon(TWeakObjectPtr<AMMWeaponBase> Weapon, const TWeakObjectPtr<UMMItemBase>& Item, const FMMWeaponInfo& _WeaponInfo, const int8& Index)
{
	if (Weapon.IsValid() && Item.IsValid())
	{
		if (Weapon->GetIsEquipped())
		{
			CharacterPtr-> UpdateWeaponInfoInInventory(Weapon.Get() -> GetWeaponId(), Weapon.Get() -> GetWeaponBaseParameters(), Weapon.Get() -> GetWeaponTrackingInfo());
		}

		Weapon->Deinitialize();
		Weapon->InitializeComponents(_WeaponInfo.GetComponentsParams());
		Weapon->InitializeParams(CharacterPtr->GetMouseCursorDecal(), Item.Get() -> GetItemId(), Item.Get() -> GetItemThumbnail(), _WeaponInfo);
	
		if (CurrentWeaponUIPtr.IsValid() && CurrentWeaponIndex == Index)
		{
			SetCurrentWeaponUI(Weapon.Get() -> GetHUDThumbnail().LoadSynchronous(),
							   Weapon.Get() -> GetWeaponBaseParameters().GetRoundsInClip(),
							   Weapon.Get() -> GetWeaponBaseParameters().GetRoundsInReserve());
		}
	}
}


void UMMWeaponsHandlerComponent::FireCurrentWeapon(const FInputActionValue& InputActionValue, const EMMMovementState& MovementState, const FOnUpdateWeaponTrackingInfo& Delegate)
{
	if (CurrentWeapon.IsValid() && CharacterAnimInstancePtr.IsValid() && CurrentWeaponUIPtr.IsValid())
	{
		// UE_LOG(LogTemp, Log, TEXT("Inout Value: %i"), InputActionValue.Get<bool>());
		
		if (InputActionValue.Get<bool>())
		{
			if (CurrentWeapon.Get() -> GetCurrentRoundsInClip() != 0)
			{
				if (CurrentWeapon.Get() -> GetCanFire())
				{
					MovementState == EMMMovementState::MS_Aim ? PlayAnimMontage(CurrentWeapon.Get() -> GetAimFireAnimation())
						: PlayAnimMontage(CurrentWeapon.Get() -> GetBaseFireAnimation());
					
					CurrentWeapon.Get() -> ExecuteFire(Delegate);
				}
			}
			else
			{
				if (!CurrentWeapon.Get() -> GetIsReload() && CurrentWeapon.Get() -> GetCanReload())
				{
					CurrentWeapon.Get() -> ResetFire();
					CurrentWeapon.Get() -> ExecuteReload();
					PlayAnimMontage(CurrentWeapon.Get() -> GetReloadAnimation());
				}
			}
		}
		else
		{
			if (CurrentWeapon.Get() -> GetWeaponFireType() == EMMWeaponFireType::WFT_AutoFire)
			{
				CharacterAnimInstancePtr.Get() -> StopAllMontages();
				CurrentWeapon.Get() -> ResetFire();
			}
		}
	}
}

void UMMWeaponsHandlerComponent::StartReloadCurrentWeapon(const FInputActionValue& InputActionValue)
{
	if (CurrentWeapon.IsValid() && CharacterAnimInstancePtr.IsValid())
	{
		if (CurrentWeapon.Get() -> GetCanReload())
		{
			CurrentWeapon.Get() -> ResetFire();
			CurrentWeapon.Get() -> ExecuteReload();
			PlayAnimMontage(CurrentWeapon.Get() -> GetReloadAnimation());
		}
	}
}

void UMMWeaponsHandlerComponent::UpdateCurrentWeaponUI(const FInputActionValue& InputActionValue)
{
	if (CurrentWeaponUIPtr.IsValid())
	{
		const int8 RoundsInClip = InputActionValue.Get<FVector2D>().X;
		const int32 RoundsInReserve = InputActionValue.Get<FVector2D>().Y;
		
		CurrentWeaponUIPtr.Get() -> UpdateAmmoText(RoundsInClip, RoundsInReserve);
	}
}

void UMMWeaponsHandlerComponent::SetCurrentDispersionParams(const EMMMovementState& MovementState)
{
	if (CurrentWeapon.IsValid())
	{
		CurrentWeapon.Get() -> SetCurrentDispersionParams(MovementState);
	}
}

void UMMWeaponsHandlerComponent::AddAmmoToEquippedWeapons(const EMMWeaponSlotType& WeaponType)
{
	if (WeaponType == EMMWeaponSlotType::WST_Secondary && SecondaryWeapon.Get() -> GetIsEquipped())
	{
		SecondaryWeapon.Get() -> SetRoundsInClip(SecondaryWeapon.Get() -> GetWeaponBaseParameters().GetMaxRoundsInClip());
		SecondaryWeapon.Get() -> SetRoundsInReserve(SecondaryWeapon.Get() -> GetWeaponBaseParameters().GetMaxRoundsInReserve());

		if (CurrentWeapon == SecondaryWeapon)
		{
			UE_LOG(LogTemp, Log, TEXT("UMMWeaponsHandlerComponent::AddAmmoToEquippedWeapons: Updating CurrentWeaponUI"));
			UpdateCurrentWeaponUI(FVector2D(SecondaryWeapon.Get() -> GetWeaponBaseParameters().GetRoundsInClip(), SecondaryWeapon.Get() -> GetWeaponBaseParameters().GetRoundsInReserve()));
		}
	}
	else if (WeaponType == EMMWeaponSlotType::WST_Heavy && HeavyWeapon.Get() -> GetIsEquipped())
	{
		HeavyWeapon.Get() -> SetRoundsInClip(HeavyWeapon.Get() -> GetWeaponBaseParameters().GetMaxRoundsInClip());
		HeavyWeapon.Get() -> SetRoundsInReserve(HeavyWeapon.Get() -> GetWeaponBaseParameters().GetMaxRoundsInReserve());

		if (CurrentWeapon == HeavyWeapon)
		{
			UpdateCurrentWeaponUI(FVector2D(HeavyWeapon.Get() -> GetWeaponBaseParameters().GetRoundsInClip(), HeavyWeapon.Get() -> GetWeaponBaseParameters().GetRoundsInReserve()));
		}
	}
}

void UMMWeaponsHandlerComponent::SetCurrentWeaponUI(UTexture2D* Image, const int8& AmmoInClip, const int32& AmmoInReserve)
{
	if (CurrentWeaponUIPtr.IsValid())
	{
		CurrentWeaponUIPtr.Get() -> SetWeapon(Image, AmmoInClip, AmmoInReserve);
	}
}

void UMMWeaponsHandlerComponent::PlayAnimMontage(TWeakObjectPtr<UAnimMontage> AnimMontage)
{
	if (CharacterAnimInstancePtr.IsValid() && AnimMontage.IsValid())
	{
		CharacterAnimInstancePtr.Get() -> PlayMontage(AnimMontage);
	}
}

EMMWeaponType UMMWeaponsHandlerComponent::GetCurrentWeaponType() const
{
	if (CurrentWeapon.IsValid())
	{
		return CurrentWeapon.Get() -> GetWeaponType();
	}

	return EMMWeaponType();
}

