// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/Character//MMInventoryComponent.h"
#include "MMCharacter.h"
#include "Core/MMGameMode.h"
#include "DataBase/MMWeaponsDataBase.h"
#include "Inventory/WMMInventory.h"
#include "Items/MMItem.h"
#include "Items/MMItemBase.h"
#include "Kismet/GameplayStatics.h"
#include "Spawners/MMItemSpawner.h"

UMMInventoryComponent::UMMInventoryComponent()
{
	InventorySlots = 0;
}

void UMMInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	InventorySlotsArr.SetNum(InventorySlots, false);

	WeaponSlotsIndexDic.Emplace(EMMWeaponSlotType::WST_Default, 0);
	WeaponSlotsIndexDic.Emplace(EMMWeaponSlotType::WST_Secondary, 1);
	WeaponSlotsIndexDic.Emplace(EMMWeaponSlotType::WST_Heavy, 2);
}

void UMMInventoryComponent::Initialization(const TWeakObjectPtr<AMMCharacter>& _CharacterPtr, const TWeakObjectPtr<UWMMInventory>& _InventoryUI)
{
	if (_CharacterPtr.IsValid())
	{
		CharacterPtr = _CharacterPtr;
	}

	InventoryUIPtr = _InventoryUI;
	
	if (InventoryUIPtr.IsValid())
	{
		InventoryUIPtr.Get() -> InitializeInventory(this);
		InventoryUIPtr.Get() -> AddToViewport();
	}
}

void UMMInventoryComponent::AddDefaultWeaponToInventory(const FDataTableRowHandle& DefaultWeaponID)
{
	const auto DefaultWeapon = DefaultWeaponID.GetRow<FMMWeaponsDataBase>(DefaultWeaponID.RowName.ToString());

	if (DefaultWeapon)
	{
		const AMMGameMode* GameMode = Cast<AMMGameMode>(UGameplayStatics::GetGameMode(GetWorld()));

		if (IsValid(GameMode))
		{
			const auto Spawner = GameMode -> GetItemSpawner();

			if (Spawner.IsValid())
			{
				TWeakObjectPtr<UMMItem> Item = Spawner.Get() -> SpawnItemToRequester(DefaultWeapon);

				if (Item.IsValid())
				{
					CharacterPtr -> ExecuteSetWeapon(Item, Item.Get() -> GetItemParams<FMMWeaponInfo>(),0);
					const TTuple<TWeakObjectPtr<UMMItemBase>, EMMWeaponSlotType> Weapon = MakeTuple(
						Item, Item.Get() -> GetItemParams<FMMWeaponInfo>().GetSlotType());
					InventoryUIPtr -> EquipWeapon(Weapon);
					OwnedWeaponsDic.Emplace(Item.Get() -> GetItemId(), Item.Get() -> GetItemParams<FMMWeaponInfo>());
				}
			}
		}
	}
}

void UMMInventoryComponent::AddItemToInventory(const TWeakObjectPtr<UMMItemBase>& Item)
{
	if (Item.IsValid() && InventoryUIPtr.IsValid())
	{
		if (Item.Get() -> GetItemType() == EMMItemType::IT_Weapon)
		{
			OwnedWeaponsDic.Emplace(Item.Get() -> GetItemId(), Item.Get() -> GetItemParams<FMMWeaponInfo>());

			if (!SetEmptySlotInInventory(0, Item))
			{
				UE_LOG(LogTemp, Error, TEXT("UMMInventoryComponent::AddItemToInventory: No empty space in Inventory"));
			}
		}
	}
}

bool UMMInventoryComponent::SetEmptySlotInInventory(const int8& Index, TWeakObjectPtr<UMMItemBase> Item)
{
	if (Index < InventorySlotsArr.Num())
	{
		if (InventorySlotsArr[Index] != nullptr)
		{
			return SetEmptySlotInInventory(Index + 1, Item);
		}
		
		InventorySlotsArr[Index] = MoveTemp(Item);
		InventoryUIPtr -> AddItemToSlot(Index, InventorySlotsArr[Index]);
			
		return true;
	}
	
	return false;
}

void UMMInventoryComponent::RemoveItemFromInventory(TWeakObjectPtr<UMMItemBase> Item)
{
	if (Item.IsValid() && InventoryUIPtr.IsValid())
	{
		if (!ClearSlotInInventory(0, Item))
		{
			UE_LOG(LogTemp, Error, TEXT("UMMInventoryComponent::RemoveItemFromInventory: Item isn't found in Inventory"));
		}
	}
}

bool UMMInventoryComponent::ClearSlotInInventory(const int8& Index, TWeakObjectPtr<UMMItemBase> Item)
{
	if (Index < InventorySlotsArr.Num())
	{
		if (InventorySlotsArr[Index] != Item)
		{
			return ClearSlotInInventory(Index + 1, Item);
		}
		
		InventoryUIPtr -> RemoveItemFromSlot(Index);
		InventorySlotsArr[Index].Reset();
		
		return true;
	}

	return false;
}

void UMMInventoryComponent::ReorderSlotsInInventory(const int8& Index)
{
	if (Index < InventorySlotsArr.Num() - 1)
	{
		if (InventorySlotsArr[Index] == nullptr && InventorySlotsArr[Index + 1] != nullptr)
		{
			InventoryUIPtr -> RemoveItemFromSlot(Index + 1);
			AddItemToInventory(InventorySlotsArr[Index + 1]);
			InventorySlotsArr[Index + 1] = nullptr;
			ReorderSlotsInInventory(Index + 1);
		}
	}
}

void UMMInventoryComponent::UseItemInInventory(TWeakObjectPtr<UMMItemBase> Item)
{
	const auto InventorySlot = FindSlotInInventory(0, Item);
	
	if (InventorySlot.Key.IsValid())
	{
		if (InventorySlot.Key.Get() -> GetItemAction() == EMMItemAction::IA_Equip)
		{
			if (InventorySlot.Key.Get() -> GetItemType() == EMMItemType::IT_Weapon)
			{
				EquipWeapon(InventorySlot);
			}
		}
	}
}

UMMInventoryComponent::FSlot UMMInventoryComponent::FindSlotInInventory(const int8& Index, TWeakObjectPtr<UMMItemBase> Item)
{
	if (Index < InventorySlotsArr.Num())
	{
		if (InventorySlotsArr[Index] != Item)
		{
			return FindSlotInInventory(Index + 1, Item);
		}
		
		return MakeTuple(InventorySlotsArr[Index], Index);
	}

	return MakeTuple(nullptr, -1);
}

void UMMInventoryComponent::EquipWeapon(const FSlot& InventorySlot)
{
	if (InventorySlot.Key.IsValid())
	{
		const auto Index = WeaponSlotsIndexDic.Find(InventorySlot.Key.Get() -> GetItemParams<FMMWeaponInfo>().GetSlotType());
	
		if (Index && CharacterPtr.IsValid() && InventoryUIPtr.IsValid())
		{
			if (OwnedWeaponsDic.Contains(InventorySlot.Key.Get() -> GetItemId()))
			{
				const TTuple<TWeakObjectPtr<UMMItemBase>, EMMWeaponSlotType> Weapon = MakeTuple(
					InventorySlot.Key, OwnedWeaponsDic[InventorySlot.Key.Get() -> GetItemId()].GetSlotType());

				RemoveItemFromInventory(InventorySlot.Key);
				InventoryUIPtr -> EquipWeapon(Weapon);
				CharacterPtr -> ExecuteSetWeapon(InventorySlot.Key, OwnedWeaponsDic[InventorySlot.Key.Get() -> GetItemId()], *Index);

				ReorderSlotsInInventory(InventorySlot.Value);
			}
		}
	}
}

void UMMInventoryComponent::UpdateWeaponInfo(const int32& _Id, const FMMWeaponBaseParams& BaseParams, const FMMWeaponTrackingInfo& TrackingInfo)
{
	for (auto Weapon : OwnedWeaponsDic)
	{
		if (Weapon.Key == _Id)
		{
			OwnedWeaponsDic[Weapon.Key].SetBaseParameters(BaseParams);
			OwnedWeaponsDic[Weapon.Key].SetTrackingInfo(TrackingInfo);
		}
	}
}
