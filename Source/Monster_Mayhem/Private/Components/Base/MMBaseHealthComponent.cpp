// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/Base/MMBaseHealthComponent.h"

UMMBaseHealthComponent::UMMBaseHealthComponent()
{
	MaxHealth = 100.f;
	CurrentHealth = MaxHealth;
}

void UMMBaseHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	CurrentHealth = MaxHealth;
}

bool UMMBaseHealthComponent::DecreaseHealth(const float& Amount)
{
	CurrentHealth = FMath::Max(CurrentHealth - Amount, 0);

	if (CurrentHealth == 0)
	{
		UE_LOG(LogTemp, Log, TEXT("Death"));
		return false;
	}
	

	return true;
}

void UMMBaseHealthComponent::IncreaseHealth(const float& Amount)
{
	CurrentHealth = FMath::Min(MaxHealth, CurrentHealth + Amount);
}
