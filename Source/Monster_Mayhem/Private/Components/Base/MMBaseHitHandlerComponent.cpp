﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/Base/MMBaseHitHandlerComponent.h"

#include "NiagaraSystem.h"
#include "Core/MMGameInstance.h"
#include "Engine/StreamableManager.h"
#include "Enums/MMObjectMaterialType.h"
#include "Helper/FMMAsyncLoader.h"
#include "Visuals/MMImpactVisual.h"


// Sets default values for this component's properties
UMMBaseHitHandlerComponent::UMMBaseHitHandlerComponent()
{
	MaterialType = EMMObjectMaterialType::OMT_None;
	ImpactSystem = nullptr;
	ImpactVisualsSpawnAmount = 10;
}

void UMMBaseHitHandlerComponent::LoadImpactFX(const TWeakObjectPtr<UMMGameInstance>& GameInstance)
{
	if (GameInstance.IsValid())
	{
		const TSoftObjectPtr<UNiagaraSystem>& Impact = GameInstance.Get() -> GetImpact(MaterialType);
				
		const auto OnLoadedDelegate = FStreamableDelegate::CreateUObject(this, &ThisClass::SpawnImpactFXPool, Impact);
		FMMAsyncLoader::LoadAsset(Impact, OnLoadedDelegate);
	}
}

void UMMBaseHitHandlerComponent::SpawnImpactFXPool(TSoftObjectPtr<UNiagaraSystem> Impact)
{
	if (Impact.IsValid())
	{
		ImpactSystem = Impact.Get();

		for (int i = 0; i < ImpactVisualsSpawnAmount; ++i)
		{
			SpawnImpact();
		}

		FMMAsyncLoader::UnloadAsset(Impact);
	}
}

void UMMBaseHitHandlerComponent::SpawnImpact()
{
	if (IsValid(ImpactSystem) && GetOwner())
	{
		const FVector Location = FVector::ZeroVector;
		const FRotator Rotation = FQuat::Identity.Rotator();
		
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Owner = GetOwner();

		auto Component = GetWorld() -> SpawnActor<AMMImpactVisual>(AMMImpactVisual::StaticClass(), Location, Rotation, SpawnParameters);
		
		if (IsValid(Component))
		{
			Component -> AttachToActor(GetOwner(), FAttachmentTransformRules::KeepRelativeTransform);
			Component -> Initialization(ImpactSystem);
			ImpactsArr.Emplace(Component);
		}
	}
}

bool UMMBaseHitHandlerComponent::ShowImpactVisual(const FVector& Location)
{
	if (!ImpactsArr.IsEmpty())
	{
		for (const auto Impact : ImpactsArr)
		{
			if (IsValid(Impact) && !Impact -> GetIsActive())
			{
				Impact -> SetActorLocation(Location);
				Impact -> ShowImpact();
				return true;
			}
		}

		SpawnImpact();
		ShowImpactVisual(Location);
	}

	return false;
}