// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/PlayerController/MMInputHandlerComponent.h"
#include "Player/MMPlayerController.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputData/MMInputConfigData.h"
#include "Player/MMCharacter.h"

UMMInputHandlerComponent::UMMInputHandlerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	InputConfigData = LoadObject<UMMInputConfigData>(NULL, *InputConfigDataPath, NULL, LOAD_None, NULL);
	
	bIsHoldingAim = false;
}

void UMMInputHandlerComponent::BeginPlay()
{
	Super::BeginPlay();

	Initialization();
}

void UMMInputHandlerComponent::Initialization()
{
	if (IsValid(Cast<AMMPlayerController>(GetOwner())))
	{
		PlayerControllerPtr = Cast<AMMPlayerController>(GetOwner());

		if (IsValid(PlayerControllerPtr))
		{
			if (IsValid(Cast<AMMCharacter>(PlayerControllerPtr -> GetPawn())))
			{
				CharacterPtr = Cast<AMMCharacter>(PlayerControllerPtr -> GetPawn());
			}

			AddInputContext(PlayerControllerPtr);
			BindInputEvents(PlayerControllerPtr);
		}
	}
}

void UMMInputHandlerComponent::AddInputContext(AMMPlayerController* PlayerController) const
{
	if (IsValid(PlayerController))
	{
		UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController -> GetLocalPlayer());

		if (IsValid(Subsystem))
		{
			if (InputConfigData -> IMC_BaseContext.IsValid())
			{
				Subsystem -> AddMappingContext(InputConfigData -> IMC_BaseContext.LoadSynchronous(), InputConfigData -> ContextPriority);
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("No Input Mapping Context found"));
			}
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("No Subsystem found"));
		}	
	}
}

void UMMInputHandlerComponent::BindInputEvents(AMMPlayerController* PlayerController)
{
	if (IsValid(PlayerControllerPtr))
	{
		if (IsValid(Cast<UEnhancedInputComponent>(PlayerController -> InputComponent)))
		{
			auto* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerController -> InputComponent);

			if (IsValid(EnhancedInputComponent))
			{
				if (InputConfigData -> IA_MoveForward.IsValid() && InputConfigData -> IA_MoveBackward.IsValid()
					&& InputConfigData -> IA_MoveLeft.IsValid() && InputConfigData -> IA_MoveRight.IsValid()
					&& InputConfigData -> IA_Sprint.IsValid() && InputConfigData -> IA_SwitchWeapon.IsValid()
					&& InputConfigData -> IA_Fire.IsValid() && InputConfigData -> IA_Reload.IsValid()
					&& InputConfigData -> IA_Aim.IsValid() && InputConfigData -> IA_ZoomIn.IsValid()
					&& InputConfigData -> IA_ZoomOut.IsValid() && InputConfigData -> IA_Interact.IsValid())
				{
					EnhancedInputComponent -> BindAction(InputConfigData -> IA_MoveForward.LoadSynchronous(), ETriggerEvent::Triggered, this, &ThisClass::HandleStartMoveForward);
					EnhancedInputComponent -> BindAction(InputConfigData -> IA_MoveBackward.LoadSynchronous(), ETriggerEvent::Triggered, this, &ThisClass::HandleStartMoveForward);
					EnhancedInputComponent -> BindAction(InputConfigData -> IA_MoveLeft.LoadSynchronous(), ETriggerEvent::Triggered, this, &ThisClass::HandleMoveRight);
					EnhancedInputComponent -> BindAction(InputConfigData -> IA_MoveRight.LoadSynchronous(), ETriggerEvent::Triggered, this, &ThisClass::HandleMoveRight);
					EnhancedInputComponent -> BindAction(InputConfigData -> IA_Sprint.LoadSynchronous(), ETriggerEvent::Triggered, this, &ThisClass::HandleSprint);
					EnhancedInputComponent -> BindAction(InputConfigData -> IA_Sprint.LoadSynchronous(), ETriggerEvent::Completed, this, &ThisClass::HandleSprint);
					EnhancedInputComponent -> BindAction(InputConfigData -> IA_SwitchWeapon.LoadSynchronous(), ETriggerEvent::Started, this, &ThisClass::HandleSwitchWeapon);
					EnhancedInputComponent -> BindAction(InputConfigData -> IA_Fire.LoadSynchronous(), ETriggerEvent::Triggered, this, &ThisClass::HandleFire);
					EnhancedInputComponent -> BindAction(InputConfigData -> IA_Reload.LoadSynchronous(), ETriggerEvent::Started, this, &ThisClass::HandleReload);
					EnhancedInputComponent -> BindAction(InputConfigData -> IA_Aim.LoadSynchronous(), ETriggerEvent::Triggered, this, &ThisClass::HandleAim);
					EnhancedInputComponent -> BindAction(InputConfigData -> IA_Aim.LoadSynchronous(), ETriggerEvent::Completed, this, &ThisClass::HandleAim);
					EnhancedInputComponent -> BindAction(InputConfigData -> IA_ZoomIn.LoadSynchronous(), ETriggerEvent::Triggered, this, &ThisClass::HandleZoom);
					EnhancedInputComponent -> BindAction(InputConfigData -> IA_ZoomOut.LoadSynchronous(), ETriggerEvent::Triggered, this, &ThisClass::HandleZoom);
					EnhancedInputComponent -> BindAction(InputConfigData -> IA_Interact.LoadSynchronous(), ETriggerEvent::Started, this, &ThisClass::HandleInteract);
				}
			}
		}
	}
}

void UMMInputHandlerComponent::HandleStartMoveForward(const FInputActionValue& InputActionValue)
{
	if (IsValid(PlayerControllerPtr))
	{
		PlayerControllerPtr -> OnMoveCharacterForward.Broadcast(InputActionValue);
	}
}

void UMMInputHandlerComponent::HandleMoveRight(const FInputActionValue& InputActionValue)
{
	if (IsValid(PlayerControllerPtr))
	{
		PlayerControllerPtr -> OnMoveCharacterRight.Broadcast(InputActionValue);
	}
}

void UMMInputHandlerComponent::HandleSprint(const FInputActionValue& InputActionValue)
{
	if (!bIsHoldingAim && IsValid(PlayerControllerPtr))
	{
		PlayerControllerPtr -> OnCharacterSprint.Broadcast(InputActionValue);
	}
}

void UMMInputHandlerComponent::HandleSwitchWeapon(const FInputActionValue& InputActionValue)
{
	if (IsValid(PlayerControllerPtr))
	{
		PlayerControllerPtr -> OnExecuteSwitchWeapon.Broadcast(InputActionValue);
	}
}

void UMMInputHandlerComponent::HandleFire(const FInputActionValue& InputActionValue)
{
	if (IsValid(PlayerControllerPtr))
	{
		UE_LOG(LogTemp, Log, TEXT("Inout Value: %i"), InputActionValue.Get<bool>());
		PlayerControllerPtr -> OnExecuteWeaponFire.Broadcast(InputActionValue);
	}
}

void UMMInputHandlerComponent::HandleReload(const FInputActionValue& InputActionValue)
{
	if (IsValid(PlayerControllerPtr))
	{
		PlayerControllerPtr -> OnExecuteWeaponReload.Broadcast(InputActionValue);
	}
}

void UMMInputHandlerComponent::HandleAim(const FInputActionValue& InputActionValue)
{
	if (IsValid(PlayerControllerPtr))
	{
		InputActionValue.Get<bool>() ? bIsHoldingAim = true : bIsHoldingAim = false;
		
		PlayerControllerPtr -> OnCharacterAim.Broadcast(InputActionValue);
	}
}

void UMMInputHandlerComponent::HandleZoom(const FInputActionValue& InputActionValue)
{
	if (!bIsHoldingAim && IsValid(PlayerControllerPtr))
	{
		PlayerControllerPtr -> OnCameraViewZoom.ExecuteIfBound(false, InputActionValue);
	}
}

void UMMInputHandlerComponent::HandleInteract(const FInputActionValue& InputActionValue)
{
	if (IsValid(PlayerControllerPtr))
	{
		PlayerControllerPtr -> OnCharacterInteract.Broadcast(InputActionValue);
	}
}
