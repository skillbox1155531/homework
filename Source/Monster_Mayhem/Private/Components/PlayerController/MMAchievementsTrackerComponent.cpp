// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/PlayerController/MMAchievementsTrackerComponent.h"
#include "MMCharacter.h"
#include "MMPlayerController.h"
#include "Core/MMGameInstance.h"
#include "DataBase/MMWeaponsDataBase.h"
#include "Objects/WMMAchievementBlock.h"
#include "Spawners/MMItemSpawner.h"

// Sets default values for this component's properties
UMMAchievementsTrackerComponent::UMMAchievementsTrackerComponent()
{
}

void UMMAchievementsTrackerComponent::Initialization(const TWeakObjectPtr<UMMGameInstance>& GameInstance, const TWeakObjectPtr<AMMPlayerController>& _PlayerControllerPtr,
                                                     const TWeakObjectPtr<AMMItemSpawner>& _ItemSpawnerPtr, const TWeakObjectPtr<UWMMAchievementBlock>& _AchievementBlockPtr)
{
	if (GameInstance.IsValid())
	{
		CreateWeaponSpawnList(GameInstance);
	}

	ItemSpawnerPtr = _ItemSpawnerPtr;
	AchievementBlockPtr = _AchievementBlockPtr;
	
	if (ItemSpawnerPtr.IsValid() && AchievementBlockPtr.IsValid())
	{
		AchievementBlockPtr.Get() -> AddToViewport();
	}

	if (_PlayerControllerPtr.IsValid() && 	_PlayerControllerPtr.Get(false) -> GetCharacterPtr().IsValid())
	{
		_PlayerControllerPtr.Get(false) -> GetCharacterPtr().Get() -> OnUpdateWeaponTrackingInfo.BindUObject(this, &ThisClass::TrackCurrentWeapon);
		SetWeaponTrackingInfo();
	}
}

void UMMAchievementsTrackerComponent::CreateWeaponSpawnList(const TWeakObjectPtr<UMMGameInstance>& _GameInstance)
{
	TArray<FMMWeaponsDataBase*> WeaponArr;
	
	if (_GameInstance.IsValid())
	{
		_GameInstance.Get() -> GetWeaponsDataBase().LoadSynchronous() -> GetAllRows("", WeaponArr);
	}

	if (!WeaponArr.IsEmpty())
	{
		EMMWeaponType WeaponType = WeaponArr[0] -> GetWeaponInfo().GetBaseParameters().GetWeaponType();
		TWeakPtr<FMMWeaponNode> CurrentNode;
		
		while (WeaponType != EMMWeaponType::WT_None)
		{
			for (const auto WeaponRow : WeaponArr)
			{
				if (WeaponRow && WeaponRow -> GetWeaponInfo().GetBaseParameters().GetWeaponType() == WeaponType)
				{
					//const auto Item = CreateItem(Weapon -> GetType(), Weapon -> GetItemInfo(), Weapon -> GetWeaponInfo());

					if (!HeadNode.IsValid())
					{
						HeadNode = MakeShared<FMMWeaponNode>(FMMWeaponNode(WeaponRow, nullptr));
						CurrentNode = HeadNode.ToWeakPtr();
					}
					else
					{
						if (CurrentNode.IsValid())
						{
							CurrentNode.Pin() -> SetNextNode(MakeShared<FMMWeaponNode>(FMMWeaponNode(WeaponRow, nullptr)));
							CurrentNode = CurrentNode.Pin() -> GetNextNode().ToWeakPtr();
						}
					}

					WeaponType = WeaponRow -> GetWeaponInfo().GetWeaponTrackingInfo().GetNextWeaponToUnlock();

					break;
				}
			}
		}
		
		if (HeadNode.IsValid())
		{
			CurrentUnlockedNode = HeadNode.ToWeakPtr();
		
			while (CurrentUnlockedNode.IsValid())
			{
				if (CurrentUnlockedNode.Pin() -> GetValue())
				{
					FString String = UEnum::GetValueAsString(CurrentUnlockedNode.Pin() -> GetValue() -> GetWeaponInfo().GetBaseParameters().GetWeaponType());
					UE_LOG(LogTemp, Log, TEXT("%s"), *String);
				}
				
				CurrentUnlockedNode = CurrentUnlockedNode.Pin() -> GetNextNode().ToWeakPtr();
			}

			CurrentUnlockedNode = HeadNode.ToWeakPtr();
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Head isn't valid"));
		}
	}
}

void UMMAchievementsTrackerComponent::TrackCurrentWeapon(const EMMWeaponType& WeaponType,
	const FMMWeaponTrackingInfo& TrackingInfo)
{
	if(CurrentUnlockedNode.IsValid())
	{
		if (TrackingWeaponInfo.GetCurrentTrackingWeapon() == WeaponType && AchievementBlockPtr.IsValid())
		{
			TrackingWeaponInfo.SetCurrentTrackingWeaponKills(TrackingInfo.GetCurrentKills());
			AchievementBlockPtr.Get() -> UpdateAmmoText(TrackingInfo.GetCurrentKills(), TrackingInfo.GetKillsToUnlockNextWeapon());

			if (TrackingWeaponInfo.GetCurrentWeaponKills() >= TrackingWeaponInfo.GetWeaponsKillsToUnlock())
			{
				if(ItemSpawnerPtr.IsValid() && CurrentUnlockedNode.Pin() -> GetNextNode().IsValid())
				{
					if (CurrentUnlockedNode.Pin() -> GetNextNode().Get() -> GetValue())
					{
						if (ItemSpawnerPtr.Get() -> SpawnItemToPoint(CurrentUnlockedNode.Pin() -> GetNextNode().Get() -> GetValue()))
						{
							CurrentUnlockedNode = CurrentUnlockedNode.Pin() -> GetNextNode();

							if (SetWeaponTrackingInfo())
							{
								UE_LOG(LogTemp, Log, TEXT("AMMGameMode::TrackCurrentWeapon: Tracking Info updated successfuly"));
							}
							else
							{
								CurrentUnlockedNode = nullptr;
								AchievementBlockPtr.Get() -> SetVisibility(ESlateVisibility::Hidden);
							}
						}
					}
				}
			}
		}
	}
}

bool UMMAchievementsTrackerComponent::SetWeaponTrackingInfo()
{
if (CurrentUnlockedNode.IsValid() && CurrentUnlockedNode.Pin() -> GetValue())
	{
		UE_LOG(LogTemp, Log, TEXT("AMMGameMode::SetWeaponTrackingInfo: Current Node and its Value is Valid"));
		
		if (CurrentUnlockedNode.Pin() -> GetNextNode().IsValid() && CurrentUnlockedNode.Pin() -> GetNextNode().Get() -> GetValue())
		{
			UE_LOG(LogTemp, Log, TEXT("AMMGameMode::SetWeaponTrackingInfo: Next Node and its Value is Valid"));

			const auto CurrentWeapon = CurrentUnlockedNode.Pin() -> GetValue();
			const auto NextWeapon = CurrentUnlockedNode.Pin() -> GetNextNode().Get() -> GetValue();
		
			if (CurrentWeapon && NextWeapon)
			{
				const auto CurrentWeaponInfo = CurrentWeapon -> GetWeaponInfo();
			
				TrackingWeaponInfo.SetCurrentTrackingWeapon(CurrentWeaponInfo.GetBaseParameters().GetWeaponType());
				TrackingWeaponInfo.SetCurrentTrackingWeaponName(CurrentWeapon -> GetItemInfo().GetName());
				TrackingWeaponInfo.SetCurrentTrackingWeaponKills(CurrentWeaponInfo.GetWeaponTrackingInfo().GetCurrentKills());
				TrackingWeaponInfo.SetWeaponsKillsToUnlock(CurrentWeaponInfo.GetWeaponTrackingInfo().GetKillsToUnlockNextWeapon());

				TrackingWeaponInfo.SetNextTrackingWeapon(NextWeapon -> GetWeaponInfo().GetBaseParameters().GetWeaponType());
				TrackingWeaponInfo.SetNextTrackingWeaponName(NextWeapon -> GetItemInfo().GetName());
				TrackingWeaponInfo.SetThumbnail(NextWeapon -> GetItemInfo().GetInventoryThumbnail());

				if (AchievementBlockPtr.IsValid())
				{
					AchievementBlockPtr.Get() -> SetBlock(TrackingWeaponInfo.GetThumbnail(),
	TrackingWeaponInfo.GetCurrentTrackingWeaponName(), TrackingWeaponInfo.GetNextTrackingWeaponName(),
	TrackingWeaponInfo.GetCurrentWeaponKills(), TrackingWeaponInfo.GetWeaponsKillsToUnlock());

					UE_LOG(LogTemp, Log, TEXT("AMMGameMode::SetWeaponTrackingInfo: Tracking Info was successfuly set up"));
					return true;
				}

				UE_LOG(LogTemp, Error, TEXT("AMMGameMode::SetWeaponTrackingInfo: Achievement block is null"));
				return false;
			}

			UE_LOG(LogTemp, Error, TEXT("AMMGameMode::SetWeaponTrackingInfo: Either Next Node or its Value isn't Valid"));
			return false;
		}
	}

	UE_LOG(LogTemp, Error, TEXT("AMMGameMode::SetWeaponTrackingInfo: Either Current Node or its Value isn't Valid"));
	return false;
}