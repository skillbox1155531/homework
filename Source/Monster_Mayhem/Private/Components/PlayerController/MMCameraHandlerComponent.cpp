// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/PlayerController/MMCameraHandlerComponent.h"
#include "InputActionValue.h"
#include "GameFramework/SpringArmComponent.h"
#include "Player/MMCharacter.h"
#include "Player/MMPlayerController.h"

UMMCameraHandlerComponent::UMMCameraHandlerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	CurrentCameraHeight = 0.f;
}

void UMMCameraHandlerComponent::BeginPlay()
{
	Super::BeginPlay();

	if (IsValid(Cast<AMMPlayerController>(GetOwner())))
	{
		PlayerControllerPtr = Cast<AMMPlayerController>(GetOwner());

		if (IsValid(PlayerControllerPtr))
		{
			if (IsValid(Cast<AMMCharacter>(PlayerControllerPtr -> GetPawn()) -> GetSpringArm()))
			{
				CameraBoomPtr = Cast<AMMCharacter>(PlayerControllerPtr -> GetPawn()) -> GetSpringArm();
				CurrentCameraHeight = CameraBoomPtr -> TargetArmLength;
			}
		}
	}

	if (IsValid(PlayerControllerPtr))
	{
		PlayerControllerPtr -> OnCameraAimZoom.BindUObject(this, &ThisClass::CameraAimZoom);
		PlayerControllerPtr -> OnCameraViewZoom.BindUObject(this, &ThisClass::CameraViewZoom);
		PlayerControllerPtr -> OnCameraSetPosition.BindUObject(this, &ThisClass::SetCameraPosition);
	}
}

void UMMCameraHandlerComponent::CameraAimZoom(const bool& bIsAim, const FInputActionValue& InputActionValue)
{
	const float Value = InputActionValue.Get<float>();
	
	bIsAim ? CameraAimZoomIn(Value) : CameraAimZoomOut(Value);
}

void UMMCameraHandlerComponent::CameraViewZoom(const bool& bIsAim, const FInputActionValue& InputActionValue)
{
	if (IsValid(CameraBoomPtr))
	{
		InputActionValue.Get<float>() > 0 ? CameraViewZoomIn() : CameraViewZoomOut();
	}
}

void UMMCameraHandlerComponent::CameraAimZoomIn(const float& DeltaSeconds)
{
	if (IsValid(CameraBoomPtr))
	{
		float ArmLength = FMath::FInterpTo(CameraBoomPtr -> TargetArmLength, CameraParams.GetAimCameraHeight(),
									 DeltaSeconds, CameraParams.GetCameraAimInterpSpeed());

		if (FMath::IsNearlyEqual(CameraBoomPtr -> TargetArmLength, CameraParams.GetAimCameraHeight(), CameraParams.GetCameraAimTolerance()))
		{
			CameraBoomPtr -> TargetArmLength = CameraParams.GetAimCameraHeight();
			return;
		}

		CameraBoomPtr -> TargetArmLength = ArmLength;
	}
}

void UMMCameraHandlerComponent::CameraAimZoomOut(const float& DeltaSeconds)
{
	if (IsValid(CameraBoomPtr))
	{
		float ArmLength = FMath::FInterpTo(CameraBoomPtr -> TargetArmLength, CurrentCameraHeight,
							DeltaSeconds, CameraParams.GetCameraAimInterpSpeed());
			
		if (FMath::IsNearlyEqual(CameraBoomPtr -> TargetArmLength, CurrentCameraHeight, CameraParams.GetCameraAimTolerance()))
		{
			if (IsValid(PlayerControllerPtr))
			{
				PlayerControllerPtr -> SetIsAiming(false);
			}
			
			CameraBoomPtr -> TargetArmLength = CurrentCameraHeight;
			return;
		}

		CameraBoomPtr -> TargetArmLength = ArmLength;
	}
}

void UMMCameraHandlerComponent::CameraViewZoomIn()
{
	if (IsValid(CameraBoomPtr))
	{
		const float TargetLength = FMath::Max(CameraBoomPtr -> TargetArmLength - CameraParams.GetCameraZoomStep(),
											  CameraParams.GetMinCameraHeight());

		CurrentCameraHeight = FMath::FInterpTo(CameraBoomPtr -> TargetArmLength, TargetLength,
											   GetWorld() -> GetDeltaSeconds(),
											   CameraParams.GetCameraViewInterpSpeed());

		CameraBoomPtr -> TargetArmLength = CurrentCameraHeight;
	}
}

void UMMCameraHandlerComponent::CameraViewZoomOut()
{
	if (IsValid(CameraBoomPtr))
	{
		const float TargetLength = FMath::Min(CameraBoomPtr -> TargetArmLength + CameraParams.GetCameraZoomStep(),
											  CameraParams.GetMaxCameraHeight());

		CurrentCameraHeight = FMath::FInterpTo(CameraBoomPtr -> TargetArmLength, TargetLength,
											   GetWorld() -> GetDeltaSeconds(),
											   CameraParams.GetCameraViewInterpSpeed());

		CameraBoomPtr -> TargetArmLength = CurrentCameraHeight;
	}
}

void UMMCameraHandlerComponent::SetCameraPosition(const FVector& TargetPos, const float& DeltaSeconds)
{
	if (IsValid(CameraBoomPtr))
	{
		const FVector CameraPos = CameraBoomPtr -> GetComponentLocation();
		CameraBoomPtr -> SetWorldLocation(FMath::VInterpConstantTo(CameraPos, TargetPos, DeltaSeconds, CameraParams.GetCameraPosInterpSpeed()));

		if (CameraPos.Equals(TargetPos, 10.f))
		{
			CameraBoomPtr -> SetWorldLocation(TargetPos);
		}
	}
}

