// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/PlayerController/MMMovementHandlerComponent.h"
#include "InputActionValue.h"
#include "Enums/MMMovementState.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Player/MMCharacter.h"
#include "Player/MMPlayerController.h"

UMMMovementHandlerComponent::UMMMovementHandlerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	
	PlayerControllerPtr = nullptr;
	CharacterPtr = nullptr;
}

void UMMMovementHandlerComponent::BeginPlay()
{
	Super::BeginPlay();

	if (IsValid(Cast<AMMPlayerController>(GetOwner())))
	{
		PlayerControllerPtr = Cast<AMMPlayerController>(GetOwner());

		if (IsValid(PlayerControllerPtr))
		{
			if (IsValid(Cast<AMMCharacter>(PlayerControllerPtr -> GetPawn())))
			{
				CharacterPtr = Cast<AMMCharacter>(PlayerControllerPtr -> GetPawn());
			}
		}
	}

	if (IsValid(PlayerControllerPtr))
	{
		PlayerControllerPtr -> OnMoveCharacterForward.AddUObject(this, &ThisClass::MoveCharacterForward);
		PlayerControllerPtr -> OnMoveCharacterRight.AddUObject(this, &ThisClass::MoveCharacterRight);
		PlayerControllerPtr -> OnCharacterSprint.AddUObject(this, &ThisClass::CharacterSprint);
		PlayerControllerPtr -> OnCharacterAim.AddUObject(this, &ThisClass::CharacterAim);
	}
}

void UMMMovementHandlerComponent::MoveCharacterForward(const FInputActionValue& InputActionValue)
{
	if (IsValid(CharacterPtr))
	{
		const FRotator Direction = CharacterPtr -> GetActorRotation();
		const FRotator YawRotation(0, Direction.Yaw, 0);
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		CharacterPtr -> AddMovementInput(ForwardDirection,  InputActionValue.Get<float>());

		if (IsValid(PlayerControllerPtr))
		{
			PlayerControllerPtr -> OnSetMovementState.ExecuteIfBound(EMMMovementState::MS_Walk);
		}
	}
}

void UMMMovementHandlerComponent::MoveCharacterRight(const FInputActionValue& InputActionValue)
{
	if (IsValid(CharacterPtr))
	{
		const FRotator Direction = CharacterPtr -> GetActorRotation();
		const FRotator YawRotation(0, Direction.Yaw, 0);
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		CharacterPtr -> AddMovementInput(RightDirection, InputActionValue.Get<float>());

		if (IsValid(PlayerControllerPtr))
		{
			PlayerControllerPtr -> OnSetMovementState.ExecuteIfBound(EMMMovementState::MS_Walk);
		}
	}
}

void UMMMovementHandlerComponent::CharacterSprint(const FInputActionValue& InputActionValue)
{
	if (IsValid(PlayerControllerPtr))
	{
		if (InputActionValue.Get<bool>())
		{
			if (PlayerControllerPtr -> GetCanSprint())
			{
				PlayerControllerPtr -> SetIsSprinting(InputActionValue.Get<bool>());
				PlayerControllerPtr -> ReduceCharacterStamina();
				PlayerControllerPtr -> OnSetMovementState.ExecuteIfBound(EMMMovementState::MS_Sprint);
			}
			else
			{
				PlayerControllerPtr -> SetIsSprinting(false);
				PlayerControllerPtr -> OnSetMovementState.ExecuteIfBound(EMMMovementState::MS_Walk);
			}
		}
		else
		{
			PlayerControllerPtr -> SetIsSprinting(InputActionValue.Get<bool>());
			PlayerControllerPtr -> OnSetMovementState.ExecuteIfBound(EMMMovementState::MS_Walk);
		}
	}
}

void UMMMovementHandlerComponent::CharacterAim(const FInputActionValue& InputActionValue)
{
	if (IsValid(PlayerControllerPtr))
	{
		if(InputActionValue.Get<bool>())
		{
			PlayerControllerPtr -> OnSetMovementState.ExecuteIfBound(EMMMovementState::MS_Aim);
		}
		else
		{
			PlayerControllerPtr -> OnSetMovementState.ExecuteIfBound(EMMMovementState::MS_Idle);
		}
	}
}