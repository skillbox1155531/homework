// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/PlayerController/MMConsumableRequestHandlerComponent.h"
#include "MMPlayerController.h"
#include "MMCharacter.h"
#include "Core/MMGameInstance.h"
#include "DataBase/MMConsumablesDataBase.h"
#include "Kismet/KismetMathLibrary.h"
#include "Spawners/MMItemSpawner.h"

// Sets default values for this component's properties
UMMConsumableRequestHandlerComponent::UMMConsumableRequestHandlerComponent()
{
	HealthDropWeight = .4f;
	DefaultHealthDropWeightMultiplier = 1.f;
	HealthDropWeightMultiplier = DefaultHealthDropWeightMultiplier;
	ShieldDropWeight = 3.f;
	DefaultShieldDropWeightMultiplier = 1.f;
	ShieldDropWeightMultiplier = DefaultShieldDropWeightMultiplier;
}


void UMMConsumableRequestHandlerComponent::Initialization(const TWeakObjectPtr<UMMGameInstance>& GameInstance,
	const TWeakObjectPtr<AMMPlayerController>& _PlayerControllerPtr,
	const TWeakObjectPtr<AMMItemSpawner>& _ItemSpawnerPtr)
{
	if (GameInstance.IsValid())
	{
		GameInstance.Get() -> GetConsumablesDataBase().LoadSynchronous() -> GetAllRows("", ConsumablesArr);

		if (ConsumablesArr.IsEmpty())
		{
			UE_LOG(LogTemp, Error, TEXT("UMMAmmoTrackerComponent::Initialization: Ammo Array is Empty"));
		}
	}

	if (_ItemSpawnerPtr.IsValid())
	{
		ItemSpawnerPtr = _ItemSpawnerPtr;
	}

	if (_PlayerControllerPtr.IsValid() && _PlayerControllerPtr.Get() -> GetCharacterPtr().IsValid())
	{
		_PlayerControllerPtr.Get() -> GetCharacterPtr().Get() -> OnRequestConsumableSpawn.AddUObject(this, &ThisClass::CallSpawnConsumable);
	}
}

void UMMConsumableRequestHandlerComponent::CallSpawnConsumable(const EMMConsumableType& ConsumableType)
{
	UE_LOG(LogTemp, Log, TEXT("UMMConsumableRequestHandlerComponent::CallSpawnConsumable: Request Received"));
	const float Weight = ConsumableType == EMMConsumableType::CT_Health ? HealthDropWeight * HealthDropWeightMultiplier : ShieldDropWeight * ShieldDropWeightMultiplier;

	if (!TryToSpawnConsumable(ConsumableType, Weight))
	{
		ConsumableType == EMMConsumableType::CT_Health ? HealthDropWeightMultiplier += .5f : ShieldDropWeightMultiplier += .5f;
	}
	else
	{
		ConsumableType == EMMConsumableType::CT_Health ? HealthDropWeightMultiplier = DefaultHealthDropWeightMultiplier : ShieldDropWeightMultiplier = DefaultShieldDropWeightMultiplier;
	}
}

bool UMMConsumableRequestHandlerComponent::TryToSpawnConsumable(const EMMConsumableType& ConsumableType, const float& Weight)
{
	if (FMMConsumablesDataBase* HealthToSpawn = GetConsumableFromArr(ConsumableType))
	{
		const bool bSpawnConsumable = UKismetMathLibrary::RandomBoolWithWeight(Weight);
		
		if(bSpawnConsumable && !ConsumableDic.Contains(ConsumableType))
		{
			return SendConsumableToSpawner(HealthToSpawn);
		}
		if(bSpawnConsumable && ConsumableDic.Contains(ConsumableType))
		{
			ConsumableDic.Remove(ConsumableType);
			return SendConsumableToSpawner(HealthToSpawn);
		}
		if (!bSpawnConsumable && ConsumableDic.Contains(ConsumableType))
		{
			return false;
		}
		if(!bSpawnConsumable && !ConsumableDic.Contains(ConsumableType))
		{
			ConsumableDic.EmplaceUnique(ConsumableType, HealthToSpawn);
			return false;
		}
	}

	return false;
}

bool UMMConsumableRequestHandlerComponent::SendConsumableToSpawner(const FMMConsumablesDataBase* Consumable)
{
	if (ItemSpawnerPtr.IsValid() && Consumable)
	{
		UE_LOG(LogTemp, Log, TEXT("UMMConsumableRequestHandlerComponent::SendConsumableToSpawner: Sending Consumable To Spawner"));
		return ItemSpawnerPtr.Get() -> SpawnItemToPoint(Consumable);
	}

	UE_LOG(LogTemp, Error, TEXT("UMMConsumableRequestHandlerComponent::SendConsumableToSpawner: Couldn't send Consumable To Spawner"));
	return false;
}

FMMConsumablesDataBase* UMMConsumableRequestHandlerComponent::GetConsumableFromArr(const EMMConsumableType& ConsumableType)
{
	if (!ConsumablesArr.IsEmpty())
	{
		TArray<FMMConsumablesDataBase*> Arr;
		
		for (const auto Consumable : ConsumablesArr)
		{
			if (Consumable -> GetConsumableInfo().GetBaseParameters().GetType() == ConsumableType)
			{
				Arr.Emplace(Consumable);
			}
		}
		
		return RandomizeConsumable(Arr);
	}

	UE_LOG(LogTemp, Error, TEXT("UMMConsumableRequestHandlerComponent::GetConsumableFromArr: Consumable wasn't found"));
	return nullptr;
}

FMMConsumablesDataBase* UMMConsumableRequestHandlerComponent::RandomizeConsumable(const TArray<FMMConsumablesDataBase*>& Arr)
{
	if (!Arr.IsEmpty())
	{
		const auto Num = FMath::RandRange(0, Arr.Num() - 1);
		
		return Arr.IsValidIndex(Num) ? Arr[Num] : nullptr;
	}

	return nullptr;
}
