// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/PlayerController/MMAmmoRequestHandlerComponent.h"

#include "MMCharacter.h"
#include "MMPlayerController.h"
#include "Core/MMGameInstance.h"
#include "Kismet/KismetMathLibrary.h"
#include "Spawners/MMItemSpawner.h"

// Sets default values for this component's properties
UMMAmmoRequestHandlerComponent::UMMAmmoRequestHandlerComponent()
{
	SpecialAmmoDropWeight = .3f;
	DefaultSpecialAmmoDropWeightMultiplier = 1.f;
	SpecialAmmoDropWeightMultiplier = DefaultSpecialAmmoDropWeightMultiplier;
	HeavyAmmoDropWeight = .15f;
	DefaultHeavyAmmoDropWeightMultiplier = 1.f;
	HeavyAmmoDropWeightMultiplier = DefaultHeavyAmmoDropWeightMultiplier;
}

void UMMAmmoRequestHandlerComponent::Initialization(const TWeakObjectPtr<UMMGameInstance>& GameInstance, const TWeakObjectPtr<AMMPlayerController>& PlayerController,
	const TWeakObjectPtr<AMMItemSpawner>& _ItemSpawnerPtr)
{
	if (GameInstance.IsValid())
	{
		GameInstance.Get() -> GetAmmoDataBase().LoadSynchronous() -> GetAllRows("", AmmoArr);

		if (AmmoArr.IsEmpty())
		{
			UE_LOG(LogTemp, Error, TEXT("UMMAmmoTrackerComponent::Initialization: Ammo Array is Empty"));
		}
	}

	if (_ItemSpawnerPtr.IsValid())
	{
		ItemSpawnerPtr = _ItemSpawnerPtr;
	}

	if (PlayerController.IsValid() && PlayerController.Get() -> GetCharacterPtr().IsValid())
	{
		PlayerController.Get() -> GetCharacterPtr().Get() -> OnRequestWeaponAmmoSpawn.AddUObject(this, &ThisClass::CallSpawnAmmo);
	}
}

void UMMAmmoRequestHandlerComponent::CallSpawnAmmo(const EMMWeaponSlotType& Slot)
{
	UE_LOG(LogTemp, Log, TEXT("UMMAmmoTrackerComponent::SpawnAmmo: Request Received"));
	const float Weight = Slot == EMMWeaponSlotType::WST_Secondary ? SpecialAmmoDropWeight * SpecialAmmoDropWeightMultiplier : HeavyAmmoDropWeightMultiplier * HeavyAmmoDropWeightMultiplier;

	if (!TryToSpawnAmmo(Slot, Weight))
	{
		Slot == EMMWeaponSlotType::WST_Secondary ? SpecialAmmoDropWeightMultiplier++ : HeavyAmmoDropWeightMultiplier++;
	}
	else
	{
		Slot == EMMWeaponSlotType::WST_Secondary ? SpecialAmmoDropWeightMultiplier = DefaultSpecialAmmoDropWeightMultiplier : HeavyAmmoDropWeightMultiplier = DefaultHeavyAmmoDropWeightMultiplier;
	}
}

bool UMMAmmoRequestHandlerComponent::TryToSpawnAmmo(const EMMWeaponSlotType& Slot, const float& Weight)
{
	if (FMMAmmoDataBase* AmmoToSpawn = GetAmmoFromArr(Slot))
	{
		const bool bSpawnAmmo = UKismetMathLibrary::RandomBoolWithWeight(Weight);
		
		if(bSpawnAmmo && !AmmoDic.Contains(Slot))
		{
			return SendAmmoToSpawner(AmmoToSpawn);
		}
		if(bSpawnAmmo && AmmoDic.Contains(Slot))
		{
			AmmoDic.Remove(Slot);
			return SendAmmoToSpawner(AmmoToSpawn);
		}
		if (!bSpawnAmmo && AmmoDic.Contains(Slot))
		{
			return false;
		}
		if(!bSpawnAmmo && !AmmoDic.Contains(Slot))
		{
			AmmoDic.EmplaceUnique(Slot, AmmoToSpawn);
			return false;
		}
	}

	return false;
}

bool UMMAmmoRequestHandlerComponent::SendAmmoToSpawner(const FMMAmmoDataBase* AmmoToSpawn)
{
	if (ItemSpawnerPtr.IsValid() && AmmoToSpawn)
	{
		return ItemSpawnerPtr.Get() -> SpawnItemToPoint(AmmoToSpawn);
	}

	return false;
}

FMMAmmoDataBase* UMMAmmoRequestHandlerComponent::GetAmmoFromArr(const EMMWeaponSlotType& AmmoType)
{
	if (!AmmoArr.IsEmpty())
	{
		for (const auto Ammo : AmmoArr)
		{
			if (Ammo -> GetAmmoInfo().GetBaseParameters().GetAmmoType() == AmmoType)
			{
				return Ammo;
			}
		}
	}

	return nullptr;
}
