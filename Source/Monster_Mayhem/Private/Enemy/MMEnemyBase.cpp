﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "MMEnemyBase.h"

#include "MMCharacter.h"
#include "Animations/MMCharacterAnimInstance.h"
#include "Base/MMBaseHealthComponent.h"
#include "Base/MMBaseHitHandlerComponent.h"
#include "Components/CapsuleComponent.h"
#include "Core/MMGameInstance.h"
#include "DataBase/MMWeaponsDataBase.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

struct FMMWeaponsDataBase;

// Sets default values
AMMEnemyBase::AMMEnemyBase()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bIsAlive = true;
	
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	HealthComponent = CreateDefaultSubobject<UMMBaseHealthComponent>(TEXT("Health Component"));
	AddOwnedComponent(HealthComponent);

	HitHandlerComponent = CreateDefaultSubobject<UMMBaseHitHandlerComponent>(TEXT("Hit Handler Component"));
	AddOwnedComponent(HitHandlerComponent);

	
}

// Called when the game starts or when spawned
void AMMEnemyBase::BeginPlay()
{
	Super::BeginPlay();

	if (GetMesh() -> GetAnimInstance() && Cast<UMMCharacterAnimInstance>(GetMesh() -> GetAnimInstance()))
	{
		CharacterAnimInstancePtr = Cast<UMMCharacterAnimInstance>(GetMesh() -> GetAnimInstance());
	}

	if (IsValid(Cast<AMMCharacter>(UGameplayStatics::GetPlayerCharacter(this, 0))))
	{
		Character = Cast<AMMCharacter>(UGameplayStatics::GetPlayerCharacter(this, 0));
	}

	if (HitHandlerComponent)
	{
		if (Cast<UMMGameInstance>(GetGameInstance()))
		{
			const TWeakObjectPtr<UMMGameInstance> GameInstance = Cast<UMMGameInstance>(GetGameInstance());
			HitHandlerComponent -> LoadImpactFX(GameInstance);

			GetCapsuleComponent() -> OnComponentHit.AddDynamic(this, &ThisClass::HandleOnHit);
		}
	}

	InitWeapon();
}

// Called every frame
void AMMEnemyBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsAlive)
	{
		if (CharacterAnimInstancePtr.IsValid())
		{
			CharacterAnimInstancePtr.Get() -> SetAnimParams(bIsAlive, GetVelocity(),
			                                                GetActorRotation(), CurrentMovementState, Weapon.Get() -> GetWeaponType());
		}

		TrackPlayer();
	}
}

void AMMEnemyBase::InitWeapon()
{
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParameters.Owner = this;
	SpawnParameters.Instigator = this;

	if (IsValid(GetWorld()))
	{
		Weapon = Cast<AMMWeaponBase>(GetWorld() -> SpawnActor(
			AMMWeaponBase::StaticClass(), &FVector::ZeroVector, &FRotator::ZeroRotator, SpawnParameters));

		if (Weapon.IsValid())
		{
			const FAttachmentTransformRules AttachmentTransformRules(EAttachmentRule::SnapToTarget, false);
			Weapon.Get() -> AttachToComponent(GetMesh(), AttachmentTransformRules, WeaponSocket);

			if (const auto DefaultWeapon = WeaponID.GetRow<FMMWeaponsDataBase>(WeaponID.RowName.ToString()))
			{
				Weapon.Get() -> InitializeComponents(DefaultWeapon -> GetWeaponInfo().GetComponentsParams());

				if (Character.IsValid())
				{
					Weapon.Get() -> InitializeParams(Character.Get() -> GetMesh(), DefaultWeapon -> GetWeaponInfo());
					Weapon.Get() -> OnReloadWeapon.AddUObject(this, &AMMEnemyBase::StartReloadCurrentWeapon);
				}
			}
		}
	}
}

void AMMEnemyBase::TrackPlayer()
{
	if (Character.IsValid())
	{
		if (FVector::Distance(GetActorLocation(), Character.Get() -> GetActorLocation()) < 500.f)
		{
			SetActorRotation(UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Character.Get() -> GetActorLocation()));
		}

		if (Weapon.IsValid())
		{
			if (FVector::Distance(GetActorLocation(), Character.Get() -> GetActorLocation()) < Weapon.Get() -> GetWeaponBaseParameters().GetMaxRange() + 100.f)
			{
				if (Weapon.Get() -> GetCurrentRoundsInClip() != 0)
				{
					if (Weapon.Get() -> GetCanFire())
					{
						Weapon.Get() -> ExecuteFire(nullptr);
					}
				}
			}
		}
	}
}

float AMMEnemyBase::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	const float Damage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (IsValid(HealthComponent))
	{
		bIsAlive = HealthComponent -> DecreaseHealth(Damage);
		
		if (!bIsAlive)
		{
			GetCapsuleComponent() -> SetCollisionEnabled(ECollisionEnabled::NoCollision);

			auto Num = FMath::RandRange(0, Deaths.Num() - 1);
			
			if (Deaths.IsValidIndex(Num))
			{
				PlayAnimation(Deaths[Num]);
			}
		}
	}

	return 0;
}

void AMMEnemyBase::HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	FVector NormalImpulse, const FHitResult& Hit)
{
	if (HitHandlerComponent)
	{
		HitHandlerComponent -> ShowImpactVisual(Hit.Location);
	}
}

void AMMEnemyBase::StartReloadCurrentWeapon(const FInputActionValue& InputActionValue)
{
	if (Weapon.IsValid() && CharacterAnimInstancePtr.IsValid())
	{
		if (Weapon.Get() -> GetCanReload())
		{
			Weapon.Get() -> ResetFire();
			Weapon.Get() -> ExecuteReload();
			PlayAnimation(Weapon.Get() -> GetReloadAnimation());
		}
	}
}

void AMMEnemyBase::PlayAnimation(TWeakObjectPtr<UAnimMontage> AnimMontage)
{
	if (CharacterAnimInstancePtr.IsValid() && AnimMontage.IsValid())
	{
		CharacterAnimInstancePtr.Get() -> PlayMontage(AnimMontage);
	}
}
