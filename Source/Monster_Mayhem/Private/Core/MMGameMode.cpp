// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/MMGameMode.h"
#include "Spawners/MMItemSpawner.h"

void AMMGameMode::BeginPlay()
{
	Super::BeginPlay();

	CreateItemSpawner();
}

TWeakObjectPtr<AMMItemSpawner> AMMGameMode::GetItemSpawner() const
{
	if (ItemSpawner.IsValid())
	{
		return ItemSpawner;
	}

	return TWeakObjectPtr<AMMItemSpawner>(nullptr);
}

void AMMGameMode::CreateItemSpawner()
{
	FActorSpawnParameters ActorSpawnParameters;
	ActorSpawnParameters.Owner = this;
	
	ItemSpawner = GetWorld() -> SpawnActor<AMMItemSpawner>(AMMItemSpawner::StaticClass(), ActorSpawnParameters);
	
	if (ItemSpawner.IsValid())
	{
		ItemSpawner -> InitializeSpawnPoints();
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("ItemSpawner wasn't created"));
	}
}
