// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/MMGameInstance.h"
#include "DataBase/MMImpactsDataBase.h"
#include "DataBase/MMItemsDataBase.h"

void UMMGameInstance::Init()
{
	Super::Init();

	if (!IsValid(WeaponsDataBase = LoadObject<UDataTable>(this, *WeaponsDataBasePath)))
	{
		UE_LOG(LogTemp, Error, TEXT("Weapon DB wasn't loaded"));
	}

	if (!IsValid(AmmoDataBase = LoadObject<UDataTable>(this, *AmmoDataBasePath)))
	{
		UE_LOG(LogTemp, Error, TEXT("Ammo DB wasn't loaded"));
	}

	if (!IsValid(ImpactsDataBase = LoadObject<UDataTable>(this, *ImpactsDataBasePath)))
	{
		UE_LOG(LogTemp, Error, TEXT("Impacts DB wasn't loaded"));
	}

	if (!IsValid(ConsumablesDataBase = LoadObject<UDataTable>(this, *ConsumablesDataBasePath)))
	{
		UE_LOG(LogTemp, Error, TEXT("Consumables DB wasn't loaded"));
	}
}

TSoftObjectPtr<UNiagaraSystem> UMMGameInstance::GetImpact(const EMMObjectMaterialType& MaterialType) const
{
	if (ImpactsDataBase)
	{
		TArray<FMMImpactsDataBase*> Impacts;
		
		ImpactsDataBase -> GetAllRows("", Impacts);

		if (!Impacts.IsEmpty())
		{
			for (const auto Impact : Impacts)
			{
				if (Impact -> GetImpactInfo().GetType() == MaterialType)
				{
					return Impact -> GetImpactInfo().GetImpactFX();
				}
			}
			
			return TSoftObjectPtr<UNiagaraSystem>(nullptr);
		}

		return TSoftObjectPtr<UNiagaraSystem>(nullptr);
	}
	
	return TSoftObjectPtr<UNiagaraSystem>(nullptr);
}
