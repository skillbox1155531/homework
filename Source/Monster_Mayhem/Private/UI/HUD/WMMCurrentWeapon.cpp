// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/HUD/WMMCurrentWeapon.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Engine/AssetManager.h"
#include "Helper/FMMAsyncLoader.h"

void UWMMCurrentWeapon::SetWeapon(TSoftObjectPtr<UTexture2D> Image, const int8& AmmoInClip, const int32& AmmoInReserve)
{
	LoadTextureToSlot(Image);
	SetAmmoText(AmmoInClip, AmmoInReserve);
}

void UWMMCurrentWeapon::LoadTextureToSlot(TSoftObjectPtr<UTexture2D> Texture)
{
	const auto OnLoadedDelegate = FStreamableDelegate::CreateUObject(this, &ThisClass::SetTextureToSlot, Texture);
	FMMAsyncLoader::LoadAsset(Texture, OnLoadedDelegate);
}

void UWMMCurrentWeapon::SetTextureToSlot(TSoftObjectPtr<UTexture2D> Texture)
{
	if (Texture.IsValid() && Thumbnail)
	{
		auto Image = Texture.Get();
		Thumbnail -> SetBrushFromTexture(Image);
	}
}

void UWMMCurrentWeapon::SetAmmoText(const int8& AmmoInClip, const int32& AmmoInReserve)
{
	if (IsValid(AmmoTextBlock))
	{
		const FText FormatPattern = FText::FromString("{0} / {1}");
		FText FormattedText;
		
		if (AmmoInReserve > 1000)
		{
			FormattedText = FText::Format(FormatPattern, FText::AsNumber(AmmoInClip), FText::FromString("-"));
		}
		else
		{
			FormattedText = FText::Format(FormatPattern, FText::AsNumber(AmmoInClip), FText::AsNumber(AmmoInReserve));
		}

		AmmoTextBlock -> SetText(FormattedText);
	}
}

void UWMMCurrentWeapon::UpdateAmmoText(const int8& AmmoInClip, const int32& AmmoInReserve)
{
	SetAmmoText(AmmoInClip, AmmoInReserve);
}

