// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/HUD/WMMBar.h"

#include "Components/HorizontalBox.h"
#include "Objects/WMMBarSegment.h"

void UWMMBar::NativeConstruct()
{
	Super::NativeConstruct();
}

void UWMMBar::Initialization(const float& MaxValue)
{
	if (IsValid(LoadClass<UWMMBarSegment>(nullptr, *HealthSegmentClassPath)))
	{
		const auto HealthSegmentClass = LoadClass<UWMMBarSegment>(this, *HealthSegmentClassPath);

		if (IsValid(HealthSegmentClass))
		{
			for (int i = 0; i < SegmentsNumber; ++i)
			{
				TWeakObjectPtr<UWMMBarSegment> HealthSegment = CreateWidget<UWMMBarSegment>(this, HealthSegmentClass);

				if (HealthSegment.IsValid() && IsValid(BarSegmentsHolder))
				{
					SegmentsArr.Emplace(HealthSegment);
					BarSegmentsHolder -> AddChild(HealthSegment.Get());
					HealthSegment -> SetColor(SegmentColor);
				}
				else
				{
					UE_LOG(LogTemp, Error, TEXT("UWMMHealthBar::Initialization: Bar Segment creation failed"));
				}
			}
		}
	}

	SegmentValue = MaxValue / SegmentsNumber;
}

void UWMMBar::SetBarValue(const float& Amount)
{
	SegmentValue = Amount / SegmentsNumber;

	for (int i = 0; i < SegmentsArr.Num(); ++i)
	{
		if (SegmentsArr.IsValidIndex(i) && SegmentsArr[i].IsValid())
		{
			SegmentsArr[i].Get() -> SetValue(1);

			if (Type == EMMBarType::BT_Shield)
			{
				SegmentsArr[i].Get() -> SetVisibility(ESlateVisibility::Visible);
			}
		}
	}
}

void UWMMBar::IncreaseBarValue(float Amount)
{
	for (int i = 0; i < SegmentsArr.Num(); ++i)
	{
		if (SegmentsArr.IsValidIndex(i) && SegmentsArr[i].IsValid())
		{
			if (SegmentsArr[i].Get() -> GetValue() != 1)
			{
				const auto CurrentSegmentValue = SegmentValue - SegmentValue * SegmentsArr[i].Get() -> GetValue();
				
				if (Amount < SegmentValue)
				{
					Amount /= SegmentValue;
					SegmentsArr[i].Get() -> IncreaseValue(Amount);
					return;
				}

				Amount -= CurrentSegmentValue;
				SegmentsArr[i].Get() -> SetValue(1);
				
				if (Type == EMMBarType::BT_Shield)
				{
					SegmentsArr[i].Get() -> SetVisibility(ESlateVisibility::Visible);
				}
			}
		}
	}
}

void UWMMBar::DecreaseBarValue(float Amount)
{
	for (int i = SegmentsArr.Num() - 1; i >= 0; --i)
	{
		if (SegmentsArr.IsValidIndex(i) && SegmentsArr[i].IsValid())
		{
			if (SegmentsArr[i].Get() -> GetValue() > 0)
			{
				const auto CurrentSegmentValue = SegmentValue * SegmentsArr[i].Get() -> GetValue();
				
				if (Amount < SegmentValue)
				{
					Amount /= SegmentValue;
					SegmentsArr[i].Get() -> DecreaseValue(Amount);
					return;
				}

				Amount -= CurrentSegmentValue;
				SegmentsArr[i].Get() -> SetValue(0);
				
				if (Type == EMMBarType::BT_Shield)
				{
					SegmentsArr[i].Get() -> SetVisibility(ESlateVisibility::Hidden);
				}
			}
		}
	}
}
