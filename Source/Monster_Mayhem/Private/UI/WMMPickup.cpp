// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/WMMPickup.h"
#include "Components/TextBlock.h"

void UWMMPickup::NativeConstruct()
{
	Super::NativeConstruct();

	ShowText(false);
}

void UWMMPickup::SetText(const FString& Text)
{
	if (IsValid(PickupText))
	{
		PickupText -> SetText(FText::FromString(Text));
	}
}

void UWMMPickup::ShowText(const bool& bIsVisible)
{
	if (IsValid(PickupText))
	{
		bIsVisible ? PickupText -> SetVisibility(ESlateVisibility::Visible)
			: PickupText -> SetVisibility(ESlateVisibility::Hidden);
	}
}