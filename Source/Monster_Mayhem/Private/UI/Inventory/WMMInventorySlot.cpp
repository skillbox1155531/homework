// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Inventory/WMMInventorySlot.h"
#include "UI/Inventory/WMMInventory.h"
#include "Components/Image.h"
#include "Engine/AssetManager.h"
#include "Helper/FMMAsyncLoader.h"
#include "Objects/WMMButton.h"

void UWMMInventorySlot::NativeConstruct()
{
	Super::NativeConstruct();

	if (ActionButton)
	{
		ActionButton -> OnButtonClickedDelegate.AddUObject(this, &ThisClass::ExecuteSlotAction);
	}
}

void UWMMInventorySlot::SetInventoryOwner(UWMMInventory* _Inventory)
{
	if (_Inventory)
	{
		Inventory = _Inventory;
	}
}

void UWMMInventorySlot::ExecuteSlotAction(UWMMButton* Button)
{
	if (Button == ActionButton)
	{
		if (Inventory)
		{
			Inventory -> UseItemInSlot(Item);
		}
	}
}

void UWMMInventorySlot::SetThumbnail(TSoftObjectPtr<UTexture2D> Texture)
{
	LoadTextureToSlot(Texture);
}

void UWMMInventorySlot::DeleteThumbnail()
{
	if (Thumbnail)
	{
		Thumbnail -> SetBrushFromTexture(nullptr);
	}
}

void UWMMInventorySlot::LoadTextureToSlot(TSoftObjectPtr<UTexture2D> Texture)
{
	const auto OnLoadedDelegate = FStreamableDelegate::CreateUObject(this, &ThisClass::SetTextureToSlot, Texture);
	FMMAsyncLoader::LoadAsset(Texture, OnLoadedDelegate);
}

void UWMMInventorySlot::SetTextureToSlot(TSoftObjectPtr<UTexture2D> Texture)
{
	if (Texture.IsValid() && Thumbnail)
	{
		auto Image = Texture.Get(); 
		Thumbnail -> SetBrushFromTexture(Image);
		FMMAsyncLoader::UnloadAsset(Texture);
	}
}

void UWMMInventorySlot::SetButtonText(const FString& Text)
{
	if (ActionButton)
	{
		ActionButton -> SetText(Text);
	}
}

void UWMMInventorySlot::SetItem(TWeakObjectPtr<UMMItemBase> _Item)
{
	if (Item != _Item)
	{
		Item = _Item;
	}
}

void UWMMInventorySlot::SetSlotVisibility(const bool& bIsVisible)
{
	if (Thumbnail && ActionButton)
	{
		if (bIsVisible)
		{
			Thumbnail -> SetVisibility(ESlateVisibility::Visible);
			ActionButton -> SetVisibility(ESlateVisibility::Visible);
		}
		else
		{
			Thumbnail -> SetVisibility(ESlateVisibility::Hidden);
			ActionButton -> SetVisibility(ESlateVisibility::Hidden);
		}
	}
}
