// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Inventory/WMMInventory.h"
#include "Blueprint/WidgetTree.h"
#include "Components/WrapBox.h"
#include "Inventory/WMMEquipmentSlot.h"
#include "Inventory/WMMInventorySlot.h"
#include "Items/MMItemBase.h"
#include "Components/Character//MMInventoryComponent.h"

void UWMMInventory::NativeConstruct()
{
	Super::NativeConstruct();
}

void UWMMInventory::InitializeInventory(UMMInventoryComponent* InventoryComponent)
{
	if (InventoryComponent)
	{
		InventoryComponentPtr = InventoryComponent;

		if (IsValid(LoadClass<UWMMInventorySlot>(nullptr, *InventorySlotClassPath)))
		{
			const auto InventorySlotClass = LoadClass<UWMMInventorySlot>(this, *InventorySlotClassPath);
			InventorySlotArr.SetNum(InventoryComponent -> GetInventorySlots(), false);
			
			for (int i = 0; i < InventorySlotArr.Num(); ++i)
			{
				if (IsValid(InventorySlotClass))
				{
					const auto InventorySlot = Cast<UWMMInventorySlot>(CreateWidget(this, InventorySlotClass));

					if (IsValid(InventorySlot) && IsValid(InventoryBox))
					{
						InventorySlotArr[i] = InventorySlot;
						InventoryBox -> AddChild(InventorySlotArr[i]);
						InventorySlotArr[i] -> SetInventoryOwner(this);
						InventorySlotArr[i] -> SetSlotVisibility(false);
					}
					else
					{
						UE_LOG(LogTemp, Error, TEXT("UWMMInventory::InitializeInventory: Slot Creation failed"));
					}
				}
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("UWMMInventory::InitializeInventory: Setting up UI failed"));
	}

	TArray<UWidget*> Widgets;
	
	this -> WidgetTree -> GetAllWidgets(Widgets);

	for (const auto Widget : Widgets)
	{
		if (UWMMEquipmentSlot* EquipmentSlot = Cast<UWMMEquipmentSlot>(Widget))
		{
			EquipmentSlot -> Initialization(false);
			EquipmentSlotDic.Emplace(EquipmentSlot, EquipmentSlot -> GetSlotType());
		}
	}
}

void UWMMInventory::AddItemToSlot(const int8& Index, const TWeakObjectPtr<UMMItemBase>& Item)
{
	if (InventorySlotArr.IsValidIndex(Index) && Item.IsValid())
	{
		InventorySlotArr[Index] -> SetItem(Item);
		InventorySlotArr[Index] -> SetThumbnail(Item.Get() -> GetItemThumbnail());
		InventorySlotArr[Index] -> SetButtonText(GetDisplayNameFromEnum(Item.Get() -> GetItemAction()));
		InventorySlotArr[Index] -> SetSlotVisibility(true);
	}
}


FString UWMMInventory::GetDisplayNameFromEnum(EMMItemAction Enum)
{
	if (const UEnum* EnumPtr = FindFirstObject<UEnum>(TEXT("EMMItemAction")))
	{
		const FString DisplayName = EnumPtr -> GetNameByValue(static_cast<int64>(Enum)).ToString();

		TArray<FString> OutArray;

		DisplayName.ParseIntoArray(OutArray, TEXT("_"));

		const FString NewString = OutArray.Last();
	
		return NewString;
	}

	throw std::runtime_error("Invalid Enum");
}

void UWMMInventory::RemoveItemFromSlot(const int8& Index)
{
	if(InventorySlotArr.IsValidIndex(Index))
	{
		InventorySlotArr[Index] -> SetItem(nullptr);
		InventorySlotArr[Index] -> DeleteThumbnail();
		InventorySlotArr[Index] -> SetButtonText(TEXT(""));
		InventorySlotArr[Index] -> SetSlotVisibility(false);
	}
}

void UWMMInventory::UseItemInSlot(const TWeakObjectPtr<UMMItemBase>& Item)
{
	if (IsValid(InventoryComponentPtr))
	{
		UE_LOG(LogTemp, Log, TEXT("Using Item"));
		InventoryComponentPtr -> UseItemInInventory(Item);
	}
}

void UWMMInventory::EquipWeapon(const FWeapon& Item)
{
	for (const auto EquipmentSlot : EquipmentSlotDic)
	{
		if (IsValid(EquipmentSlot.Key) && Item.Key.IsValid())
		{
			if (EquipmentSlot.Key -> GetWeaponSlotType() == Item.Value)
			{
				if (IsValid(InventoryComponentPtr) && EquipmentSlot.Key -> GetItemInSlot().IsValid())
				{
					InventoryComponentPtr -> AddItemToInventory(EquipmentSlot.Key -> GetItemInSlot());
				}
	
				EquipmentSlot.Key -> SetThumbnail(Item.Key.Get() -> GetItemThumbnail());
				EquipmentSlot.Key -> SetItem(Item.Key);
				EquipmentSlot.Key -> SetSlotVisibility(true);
			}
		}
	}
}

void UWMMInventory::EquipArmor(FArmor Item)
{
}
