// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Inventory/WMMEquipmentSlot.h"
#include "Components/Image.h"
#include "DataBase/MMItemsDataBase.h"
#include "Engine/AssetManager.h"
#include "Helper/FMMAsyncLoader.h"

void UWMMEquipmentSlot::Initialization(const bool& bIsVisible)
{
	if (GetSlotType() == EMMItemType::IT_Weapon)
	{
		ArmorType = EMMArmorType::AT_None;
	}
	else
	{
		WeaponType = EMMWeaponSlotType::WST_None;
	}

	SetSlotVisibility(bIsVisible);
}

void UWMMEquipmentSlot::SetThumbnail(TSoftObjectPtr<UTexture2D> Texture)
{
	LoadTextureToSlot(Texture);
}

void UWMMEquipmentSlot::LoadTextureToSlot(TSoftObjectPtr<UTexture2D> Texture)
{
	const auto OnLoadedDelegate = FStreamableDelegate::CreateUObject(this, &ThisClass::SetTextureToSlot, Texture);
	FMMAsyncLoader::LoadAsset(Texture, OnLoadedDelegate);
}

void UWMMEquipmentSlot::SetTextureToSlot(TSoftObjectPtr<UTexture2D> Texture)
{
	if (Texture.IsValid() && Thumbnail)
	{
		auto Image = Texture.Get();
		Thumbnail -> SetBrushFromTexture(Image);
		FMMAsyncLoader::UnloadAsset(Texture);
	}
}

void UWMMEquipmentSlot::SetItem(TWeakObjectPtr<UMMItemBase> _Item)
{
	if (Item != _Item)
	{
		Item = _Item;
	}
}

void UWMMEquipmentSlot::SetSlotVisibility(const bool& bIsVisible)
{
	if (Thumbnail)
	{
		bIsVisible ? Thumbnail -> SetVisibility(ESlateVisibility::Visible) : Thumbnail -> SetVisibility(ESlateVisibility::Hidden);
	}
}
