// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/Objects/WMMDamageVisual.h"
#include "Animation/WidgetAnimation.h"
#include "Components/TextBlock.h"

void UWMMDamageVisual::EnableDamageAnim(const float& DamageNumber)
{
	if (DamageTextBlock && WidgetAnimation)
	{
		//UE_LOG(LogTemp, Log, TEXT("UWMMDamageVisual::EnableDamageAnim"));
		
		DamageTextBlock -> SetText(FText::AsNumber(DamageNumber));
		PlayAnimation(WidgetAnimation);
	}
}
