// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Objects/WMMBarSegment.h"

#include "Components/ProgressBar.h"

void UWMMBarSegment::SetColor(const FLinearColor& Color)
{
	if (Segment)
	{
		Segment -> SetFillColorAndOpacity(Color);
	}
}

void UWMMBarSegment::SetValue(const float& _Amount)
{
	if (Segment)
	{
		Segment -> SetPercent(_Amount);
	}
}

void UWMMBarSegment::IncreaseValue(const float& _Amount)
{
	if (Segment)
	{
		const auto Percent = Segment -> GetPercent() + _Amount;
		Segment -> SetPercent(Percent);
	}
}

void UWMMBarSegment::DecreaseValue(const float& _Amount)
{
	if (Segment)
	{
		const auto Percent = Segment -> GetPercent() - _Amount;
		Segment -> SetPercent(Percent);
	}
}

float UWMMBarSegment::GetValue()
{
	if (Segment)
	{
		return Segment -> GetPercent();
	}

	return -1;
}
