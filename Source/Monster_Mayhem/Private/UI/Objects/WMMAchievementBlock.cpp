// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Objects/WMMAchievementBlock.h"

#include <stdexcept>

#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Core/MMGameMode.h"
#include "Helper/FMMAsyncLoader.h"
#include "Weapons/MMWeaponInfo.h"

void UWMMAchievementBlock::SetBlock(const TSoftObjectPtr<UTexture2D>& Image, const FString& CurrentWeapon, const FString& NextWeapon, const int32& CurrentKills, const int32& KillsToUnlock)
{
	LoadTextureToSlot(Image);
	SetWeaponToUnlockText(CurrentWeapon, NextWeapon);
	SetKillsToUnlockText(CurrentKills, KillsToUnlock);
}

void UWMMAchievementBlock::LoadTextureToSlot(TSoftObjectPtr<UTexture2D> Texture)
{
	const auto OnLoadedDelegate = FStreamableDelegate::CreateUObject(this, &ThisClass::SetTextureToSlot, Texture);
	FMMAsyncLoader::LoadAsset(Texture, OnLoadedDelegate);
}

void UWMMAchievementBlock::SetTextureToSlot(TSoftObjectPtr<UTexture2D> Texture)
{
	if (Texture.IsValid() && Thumbnail)
	{
		auto Image = Texture.Get();
		Thumbnail -> SetBrushFromTexture(Image);
	}
}


void UWMMAchievementBlock::SetKillsToUnlockText(const int32& CurrentKills, const int32& KillsToUnlock)
{
	if (IsValid(KillsToUnlockTextBlock))
	{
		const FText FormatPattern = FText::FromString("{0} / {1}");

		const FText FormattedText = FText::Format(FormatPattern, FText::AsNumber(CurrentKills), FText::AsNumber(KillsToUnlock));
		KillsToUnlockTextBlock -> SetText(FormattedText);
	}
}

void UWMMAchievementBlock::SetWeaponToUnlockText(const FString& CurrentWeapon, const FString& NextWeapon)
{
	if (IsValid(GoalDescriptionTextBlock))
	{
		const FText FormatPattern = FText::FromString("Kills with {0} to unlock {1}");

		const FText FormattedText = FText::Format(FormatPattern, FText::FromString(CurrentWeapon), FText::FromString(NextWeapon));

		GoalDescriptionTextBlock -> SetText(FormattedText);
	}
}

FString UWMMAchievementBlock::GetDisplayNameFromEnum(EMMWeaponType Enum)
{
	if (const UEnum* EnumPtr = FindFirstObject<UEnum>(TEXT("EMMWeaponType")))
	{
		const FString DisplayName = EnumPtr -> GetNameByValue(static_cast<int64>(Enum)).ToString();

		TArray<FString> OutArray;

		DisplayName.ParseIntoArray(OutArray, TEXT("_"));

		const FString NewString = OutArray.Last();

		return NewString;
	}

	throw std::runtime_error("Invalid Enum");
}

void UWMMAchievementBlock::UpdateAmmoText(const int32& CurrentKills, const int32& KillsToUnlock)
{
	SetKillsToUnlockText(CurrentKills, KillsToUnlock);
}
