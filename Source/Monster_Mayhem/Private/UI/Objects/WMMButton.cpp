// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Objects/WMMButton.h"

#include "Components/Button.h"

void UWMMButton::NativeConstruct()
{
	Super::NativeConstruct();

	if (MainButton)
	{
		MainButton -> OnClicked.AddDynamic(this, &ThisClass::OnButtonClicked);
	}
}

void UWMMButton::OnButtonClicked()
{
	OnButtonClickedDelegate.Broadcast(this);
}
