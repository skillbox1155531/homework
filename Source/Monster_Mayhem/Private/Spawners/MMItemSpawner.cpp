// Fill out your copyright notice in the Description page of Project Settings.

#include "Spawners/MMItemSpawner.h"
#include "Environment/MMItemSpawnPoint.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AMMItemSpawner::AMMItemSpawner()
{
 	NextItemID = INT32_MIN;
}

void AMMItemSpawner::InitializeSpawnPoints()
{
	TArray<AActor*> ActorsArr;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AMMItemSpawnPoint::StaticClass(), ActorsArr);

	if (!ActorsArr.IsEmpty())
	{
		for (const auto Actor : ActorsArr)
		{
			AMMItemSpawnPoint* SpawnPoint;
			
			if (IsValid(SpawnPoint = Cast<AMMItemSpawnPoint>(Actor)))
			{
				SpawnPoint -> Initialization();
				SpawnPointsArr.Emplace(TWeakObjectPtr<AMMItemSpawnPoint>(SpawnPoint));
			}
		}
	}
}

TWeakObjectPtr<AMMItemSpawnPoint> AMMItemSpawner::GetSpawnPoint(const EMMSpawnPointType& Type)
{
	if (!SpawnPointsArr.IsEmpty())
	{
		for (auto SpawnPoint : SpawnPointsArr)
		{
			if (SpawnPoint.IsValid() && !SpawnPoint-> GetIsActive() && SpawnPoint -> GetType() == Type)
			{
				return SpawnPoint;
			}
		}
	}

	UE_LOG(LogTemp, Error, TEXT("Spawn Point wasn't found"));
	return TWeakObjectPtr<AMMItemSpawnPoint>(nullptr);
}

bool AMMItemSpawner::SpawnItem(const TWeakObjectPtr<AMMItemSpawnPoint>& SpawnPoint, const TWeakObjectPtr<UMMItem>& Item)
{
	if (SpawnPoint.IsValid())
	{
		UE_LOG(LogTemp, Log, TEXT("AMMItemSpawner::SpawnItem: Spawn Point is Valid"));

		if (Item.IsValid())
		{
			UE_LOG(LogTemp, Log, TEXT("AMMItemSpawner::SpawnItem: Item is Valid"));
					
			if (SpawnPoint.Get() -> SpawnItem(Item))
			{
				UE_LOG(LogTemp, Log, TEXT("AMMItemSpawner::SpawnItem: Item Spawned"));
				return true;
			}

			UE_LOG(LogTemp, Error, TEXT("AMMItemSpawner::SpawnItem: Couldn't spawn Item"));
			return false;
		}

		UE_LOG(LogTemp, Error, TEXT("AMMItemSpawner::SpawnItem: Item is NULL"));
		return false;
	}

	UE_LOG(LogTemp, Error, TEXT("AMMItemSpawner::SpawnItem: Spawn Point is NULL"));
	return false;
}

int32 AMMItemSpawner::GenerateItemID()
{
	return NextItemID++;
}
