// Fill out your copyright notice in the Description page of Project Settings.


#include "FX/Visuals/MMImpactVisual.h"
#include "NiagaraComponent.h"
#include "NiagaraSystem.h"

AMMImpactVisual::AMMImpactVisual()
{
	NiagaraComponent = CreateDefaultSubobject<UNiagaraComponent>("Niagara Component");
	NiagaraComponent -> SetAutoDestroy(false);
	RootComponent = NiagaraComponent;

 	bIsActive = false;
}

void AMMImpactVisual::Initialization(UNiagaraSystem* Impact)
{
	if (Impact && NiagaraComponent)
	{
		UE_LOG(LogTemp, Log, TEXT("AMMImpactVisual::Initialization"));
		
		NiagaraComponent -> SetAsset(Impact);
		NiagaraComponent -> Deactivate();
		NiagaraComponent -> OnSystemFinished.AddDynamic(this, &ThisClass::Deactivate);
	}
}

void AMMImpactVisual::ShowImpact()
{
	if (NiagaraComponent)
	{
		bIsActive = true;
		NiagaraComponent -> Activate();
	}
}


void AMMImpactVisual::Deactivate(UNiagaraComponent* Impact)
{
	if (Impact)
	{
		bIsActive = false;
		Impact -> Deactivate();
	}
}
