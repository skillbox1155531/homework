// Fill out your copyright notice in the Description page of Project Settings.


#include "FX/Visuals/MMDamageVisual.h"
#include "Animation/WidgetAnimation.h"
#include "Components/WidgetComponent.h"
#include "Objects/WMMDamageVisual.h"

// Sets default values
AMMDamageVisual::AMMDamageVisual()
{
	DamageWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("Damage Widget");
	DamageWidgetComponent -> SetWidgetSpace(EWidgetSpace::Screen);
	RootComponent = DamageWidgetComponent;

	if (IsValid(DamageWidgetComponent))
	{
		DamageWidgetComponent -> SetWidgetClass(LoadClass<UWMMDamageVisual>(this, *DamageVisualWidgetPath));
		DamageWidgetComponent -> SetDrawSize(FVector2D(25.f, 25.f));
	}

	bIsActive = false;
}

// Called when the game starts or when spawned
void AMMDamageVisual::BeginPlay()
{
	Super::BeginPlay();

	InitWidget();
}

void AMMDamageVisual::InitWidget()
{
	if (IsValid(DamageWidgetComponent))
	{
		DamageVisualWidget = Cast<UWMMDamageVisual>(DamageWidgetComponent -> GetUserWidgetObject());

		if (DamageVisualWidget.IsValid())
		{
			DamageVisualWidget.Get() -> SetVisibility(ESlateVisibility::Hidden);
			OnAnimationEnded.BindDynamic(this, &ThisClass::Deactivate);
			DamageVisualWidget.Get() -> BindToAnimationFinished(DamageVisualWidget.Get() -> GetWidgetAnimation(), OnAnimationEnded);
		}
	}
}

void AMMDamageVisual::Deactivate()
{
	//UE_LOG(LogTemp, Log, TEXT("Anim finished"));
	bIsActive = false;
}

void AMMDamageVisual::ShowDamage(const float& DamageNumber)
{
	if (DamageVisualWidget.IsValid())
	{
		//UE_LOG(LogTemp, Log, TEXT("AMMDamageVisual::ShowDamage"));
		bIsActive = true;
		DamageVisualWidget -> SetVisibility(ESlateVisibility::Visible);
		DamageVisualWidget.Get() -> EnableDamageAnim(DamageNumber);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("AMMDamageVisual::ShowDamage: No damage visual"));
	}
}

