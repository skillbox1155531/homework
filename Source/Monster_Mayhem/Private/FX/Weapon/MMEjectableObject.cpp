// Fill out your copyright notice in the Description page of Project Settings.


#include "FX/Weapon/MMEjectableObject.h"

// Sets default values
AMMEjectableObject::AMMEjectableObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = MeshComponent;

	MinInitialVelocity = 2.f;
	MaxInitialVelocity = 5.f;
	LifeSpan = 2.f;
}

void AMMEjectableObject::Launch()
{
	if (IsValid(MeshComponent))
	{
		SetActive(true);
		MeshComponent -> AddImpulse(LaunchDirection * FMath::RandRange(MinInitialVelocity, MaxInitialVelocity));
		SetDeactivationTimer();
	}
}

void AMMEjectableObject::SetDeactivationTimer()
{
	if (IsValid(GetWorld()))
	{
		GetWorld() -> GetTimerManager().SetTimer(DeactivationTimerHandle, this, &ThisClass::DeactivationTimer, LifeSpan, false);
	}
}

void AMMEjectableObject::DeactivationTimer()
{
	SetActive(false);
	OnDeactivateObject.Broadcast();

	if (IsValid(GetWorld()) && GetWorld() -> GetTimerManager().TimerExists(DeactivationTimerHandle))
	{
		GetWorld() -> GetTimerManager().ClearTimer(DeactivationTimerHandle);
	}
}

void AMMEjectableObject::SetActive(const bool& IsActive)
{
	if(IsValid(MeshComponent))
	{
		MeshComponent -> SetSimulatePhysics(IsActive);
	}
	
	SetActorTickEnabled(IsActive);
	SetActorHiddenInGame(!IsActive);
	SetActorEnableCollision(IsActive);
}
