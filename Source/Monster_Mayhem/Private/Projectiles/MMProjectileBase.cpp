// Fill out your copyright notice in the Description page of Project Settings.

#include "Projectiles/MMProjectileBase.h"
#include "DrawDebugHelpers.h"
#include "NiagaraComponent.h"
#include "Components/CapsuleComponent.h"

// Sets default values
AMMProjectileBase::AMMProjectileBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule Component"));
	CapsuleComponent -> SetSimulatePhysics(false);
	CapsuleComponent -> SetEnableGravity(false);
	CapsuleComponent -> SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	RootComponent = CapsuleComponent;
	
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("Static Mesh");
	StaticMesh -> SetEnableGravity(false);
	StaticMesh -> SetGenerateOverlapEvents(false);
	StaticMesh -> SetCollisionEnabled(ECollisionEnabled::NoCollision);
	StaticMesh -> SetupAttachment(RootComponent);

	Trail = CreateDefaultSubobject<UNiagaraComponent>("Trail");
	Trail -> SetAutoActivate(false);
	Trail -> SetupAttachment(RootComponent);
}


// Called when the game starts or when spawned
void AMMProjectileBase::BeginPlay()
{
	Super::BeginPlay();
	
	CapsuleComponent -> OnComponentHit.AddDynamic(this, &ThisClass::HandleOnHit);
}

void AMMProjectileBase::BeginDestroy()
{
	Super::BeginDestroy();
}

// Called every frame
void AMMProjectileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//DrawDebugCapsule(GetWorld(), StaticMesh -> GetComponentLocation(), 10.f, 10.f, FQuat::Identity, FColor::Red);
}

void AMMProjectileBase::Launch(const FRotator& Direction)
{
	SetActive(true);

	SpawnLocation = GetActorLocation();
	SetActorRotation(FRotator( 0.f, Direction.Yaw, 0.f));
	
	if (CapsuleComponent -> GetBodyInstance() -> IsValidBodyInstance())
	{
		CapsuleComponent -> GetBodyInstance() -> bLockRotation = true;
		CapsuleComponent -> GetBodyInstance() -> CreateDOFLock();
	}
	
	CapsuleComponent -> AddImpulse(GetActorForwardVector() * BaseParameters.GetInitialVelocity(), TEXT(""), false);
}

void AMMProjectileBase::TrackProjectile() {}

void AMMProjectileBase::HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) {}

void AMMProjectileBase::SetActive(const bool& _bIsActive)
{
	if(IsValid(CapsuleComponent))
	{
		CapsuleComponent -> SetSimulatePhysics(_bIsActive);
	}
	
	SetActorTickEnabled(_bIsActive);
	SetActorHiddenInGame(!_bIsActive);
	SetActorEnableCollision(_bIsActive);
}