// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectiles/MMRocket.h"
#include "NiagaraComponent.h"
#include "Components/AudioComponent.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"

AMMRocket::AMMRocket()
{
	Explosion = CreateDefaultSubobject<UNiagaraComponent>("Explosion");
	Explosion -> SetAutoActivate(false);
	Explosion -> SetupAttachment(RootComponent);

	AudioComponent = CreateDefaultSubobject<UAudioComponent>("Audio Component");
	AudioComponent -> SetAutoActivate(false);
	AudioComponent -> SetupAttachment(RootComponent);

	bIsHit = false;
	DeactivationDelay = 0.f;
}

void AMMRocket::BeginPlay()
{
	Super::BeginPlay();

	if (IsValid(AudioComponent))
	{
		DeactivationDelay = AudioComponent -> GetSound() -> GetDuration();
	}
}

void AMMRocket::HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!bIsHit)
	{
		bIsHit = true;
		
		DrawDebugSphere(GetWorld(), GetActorLocation(), BaseParameters.GetDamageInnerRadius(), 10, FColor::Red, false, 10.f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), BaseParameters.GetDamageOuterRadius(), 10, FColor::Orange, false, 10.f);

		const TArray<AActor*> IgnoreActors;
		UGameplayStatics::ApplyRadialDamageWithFalloff(this, BaseParameters.GetCurrentDamage(), BaseParameters.GetMinDamage(), GetActorLocation(), BaseParameters.GetDamageInnerRadius(), BaseParameters.GetDamageOuterRadius(), BaseParameters.GetDamageFallOffAmount(), nullptr, IgnoreActors, GetOwner());
	
		OnExplode();
	}
}

void AMMRocket::TrackProjectile()
{
	const float Distance = FVector::Distance(GetActorLocation(), SpawnLocation);
	
	if (Distance >= BaseParameters.GetMaxRange())
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), BaseParameters.GetDamageInnerRadius(), 10, FColor::Red, false, 10.f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), BaseParameters.GetDamageOuterRadius(), 10, FColor::Orange, false, 10.f);

		TArray<AActor*> IgnoreActors;
		UGameplayStatics::ApplyRadialDamageWithFalloff(this, BaseParameters.GetCurrentDamage(), BaseParameters.GetMinDamage(), GetActorLocation(), BaseParameters.GetDamageInnerRadius(), BaseParameters.GetDamageOuterRadius(), BaseParameters.GetDamageFallOffAmount(), nullptr, IgnoreActors);
		
		OnExplode();
	}
}

void AMMRocket::OnExplode()
{
	if (IsValid(CapsuleComponent) && IsValid(StaticMesh) && IsValid(Trail) && IsValid(Explosion) && IsValid(AudioComponent))
	{
		CapsuleComponent -> SetSimulatePhysics(false);
		StaticMesh -> SetVisibility(false);
	
		Trail -> Deactivate();
		Explosion -> Activate();
		AudioComponent -> Play();
			
		if (IsValid(GetWorld()))
		{
			if (GetWorld() -> GetTimerManager().TimerExists(ProjectileTrackerTimerHandle))
			{
				GetWorld() -> GetTimerManager().ClearTimer(ProjectileTrackerTimerHandle);
			}
				
			GetWorld() -> GetTimerManager().SetTimer(DeactivationTimerHandle, [&] { SetActive(false), OnDeactivateProjectile.Broadcast(this); },DeactivationDelay , false);
		}
	}
}

void AMMRocket::SetActive(const bool& bIsActive)
{
	Super::SetActive(bIsActive);

	if (!bIsActive)
	{
		if (CapsuleComponent -> GetBodyInstance())
		{
			CapsuleComponent -> GetBodyInstance() -> bLockRotation = false;
			CapsuleComponent -> GetBodyInstance() -> CreateDOFLock();
		}
		if (IsValid(Trail) && IsValid(Explosion) && IsValid(AudioComponent) && IsValid(StaticMesh))
		{
			StaticMesh -> SetVisibility(true);
			Trail -> Deactivate();
			Explosion -> Deactivate();
			AudioComponent -> Stop();
		}
		
		if (GetWorld() -> GetTimerManager().TimerExists(DeactivationTimerHandle))
		{
			GetWorld() -> GetTimerManager().ClearTimer(DeactivationTimerHandle);
		}

		bIsHit = false;
		BaseParameters.SetCurrentDamage(BaseParameters.GetMaxDamage());
	}
	else
	{
		if (IsValid(GetWorld()) && IsValid(Trail) && IsValid(StaticMesh))
		{
			Trail -> Activate();
			
			GetWorld() -> GetTimerManager().SetTimer(ProjectileTrackerTimerHandle, this, &ThisClass::TrackProjectile, GetWorld() -> GetDeltaSeconds(), true);
		}
	}
}