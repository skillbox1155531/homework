// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectiles/MMBullet.h"
#include "NiagaraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Interfaces/IDamageable.h"
#include "Kismet/GameplayStatics.h"

void AMMBullet::TrackProjectile()
{
	const float Distance = FVector::Distance(GetActorLocation(), SpawnLocation);
	
	if (Distance >= BaseParameters.GetDamageFallOffRange())
	{
		BaseParameters.SetCurrentDamage(FMath::Max(BaseParameters.GetCurrentDamage() - BaseParameters.GetDamageFallOffAmount(), BaseParameters.GetMinDamage()));
	}
	if (Distance >= BaseParameters.GetMaxRange())
	{
		SetActive(false);
		OnDeactivateProjectile.Broadcast(this);
	}
}

void AMMBullet::HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if(OtherActor -> Implements<UDamageable>())
	{
		UGameplayStatics::ApplyPointDamage(OtherActor, BaseParameters.GetCurrentDamage(), GetActorLocation(), FHitResult(), nullptr, GetOwner(), nullptr);
	}

	SetActive(false);
	OnDeactivateProjectile.Broadcast(this);
}

void AMMBullet::SetActive(const bool& bIsActive)
{
	Super::SetActive(bIsActive);

	if (!bIsActive)
	{
		if (CapsuleComponent -> GetBodyInstance())
		{
			CapsuleComponent -> GetBodyInstance() -> bLockRotation = false;
			CapsuleComponent -> GetBodyInstance() -> CreateDOFLock();
		}
		if (IsValid(Trail))
		{
			Trail -> Deactivate();
		}
		
		if (GetWorld() -> GetTimerManager().TimerExists(ProjectileTrackerTimerHandle))
		{
			GetWorld() -> GetTimerManager().ClearTimer(ProjectileTrackerTimerHandle);
		}

		BaseParameters.SetCurrentDamage(BaseParameters.GetMaxDamage());
	}
	else
	{
		if (IsValid(GetWorld()) && IsValid(Trail))
		{
			Trail -> Activate();
			
			GetWorld() -> GetTimerManager().SetTimer(ProjectileTrackerTimerHandle, this, &ThisClass::TrackProjectile, GetWorld() -> GetDeltaSeconds(), true);
		}
	}
}