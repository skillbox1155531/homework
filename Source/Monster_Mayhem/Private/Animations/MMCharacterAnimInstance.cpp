// Fill out your copyright notice in the Description page of Project Settings.


#include "Animations/MMCharacterAnimInstance.h"
#include "KismetAnimationLibrary.h"
#include "Enums/MMMovementState.h"
#include "Structs//Weapons/MMWeaponInfo.h"

void UMMCharacterAnimInstance::SetAnimParams(const bool& _bIsAlive, const FVector& Velocity, const FRotator& Rotation, const EMMMovementState& State, const EMMWeaponType& _CurrentWeaponType)
{
	bIsAlive = _bIsAlive;
	CurrenWeaponType = _CurrentWeaponType;
	MovementState = State;
	MovementSpeed = Velocity.Size();
	MovementDirection = UKismetAnimationLibrary::CalculateDirection(Velocity, Rotation);
}

void UMMCharacterAnimInstance::SetVelocityParam(const FVector& Velocity)
{
	MovementSpeed = Velocity.Size();
}

void UMMCharacterAnimInstance::SetMovementDirParam(const FVector& Velocity, const FRotator& Rotation)
{
	MovementDirection = UKismetAnimationLibrary::CalculateDirection(Velocity, Rotation);
}

void UMMCharacterAnimInstance::SetMovementStateParam(const EMMMovementState& State)
{
	MovementState = State;
}

void UMMCharacterAnimInstance::SetWeaponTypeParam(const EMMWeaponType& _CurrentWeaponType)
{
	CurrenWeaponType = _CurrentWeaponType;
}

void UMMCharacterAnimInstance::SetIsAliveParam(const bool& _bIsAlive)
{
	bIsAlive = _bIsAlive;
}

void UMMCharacterAnimInstance::PlayMontage(TWeakObjectPtr<UAnimMontage> AnimMontage)
{
	if (AnimMontage.IsValid() && GetCurrentActiveMontage() != AnimMontage.Get())
	{
		// UE_LOG(LogTemp, Warning, TEXT("UMMCharacterAnimInstance::PlayMontage: Montage Name: %s"), *Montage -> GetName());
		Montage_Play(AnimMontage.Get(), AnimMontage.Get() -> RateScale);
	}
}

void UMMCharacterAnimInstance::StopMontage(const UAnimMontage* Montage)
{
	if (IsValid(Montage) && GetCurrentActiveMontage() == Montage)
	{
		Montage_Stop(Montage -> BlendOut.GetBlendTime(), Montage);
	}
}

void UMMCharacterAnimInstance::StopAllMontages()
{
	Montage_Stop(.25f);
}