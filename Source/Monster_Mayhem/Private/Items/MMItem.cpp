// Fill out your copyright notice in the Description page of Project Settings.


#include "Items/MMItem.h"

void UMMItem::Use()
{
	if (GEngine)
	{
		GEngine -> AddOnScreenDebugMessage(-1, 2.f, FColor::Green, FString::Printf(TEXT("Available to Use %s"), *GetItemName()));
	}
}
