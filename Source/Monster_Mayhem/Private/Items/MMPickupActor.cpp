// Fill out your copyright notice in the Description page of Project Settings.

#include "Items/MMPickupActor.h"
#include "Components/WidgetComponent.h"
#include "Engine/StreamableManager.h"
#include "Environment/MMItemSpawnPoint.h"
#include "Helper/FMMAsyncLoader.h"
#include "Items/MMItem.h"
#include "UI/WMMPickup.h"

AMMPickupActor::AMMPickupActor()
{
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	MeshComponent -> SetMobility(EComponentMobility::Movable);
	MeshComponent -> SetGenerateOverlapEvents(true);
	MeshComponent -> SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent -> SetCollisionObjectType(ECC_GameTraceChannel3);
	RootComponent = MeshComponent;

	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("PickupText"));
	WidgetComponent -> SetWorldLocation(FVector(0.f, 0.f, 50.f));
	WidgetComponent -> SetWidgetSpace(EWidgetSpace::Screen);
	WidgetComponent -> SetupAttachment(RootComponent);
	
	if (IsValid(LoadClass<UWMMPickup>(nullptr, *WidgetClassPath)))
	{
		WidgetComponent -> SetWidgetClass(LoadClass<UWMMPickup>(nullptr, *WidgetClassPath));
	}

	InRangeStencilValue = 0;
	InteractStencilValue = 1;
}

void AMMPickupActor::BeginPlay()
{
	Super::BeginPlay();
	
	if (IsValid(WidgetComponent) && IsValid(Cast<UWMMPickup>(WidgetComponent -> GetUserWidgetObject())))
	{
		PickupUIPtr = Cast<UWMMPickup>(WidgetComponent -> GetUserWidgetObject());
	}
}

bool AMMPickupActor::InitializePickup(AMMItemSpawnPoint* _OwningPoint, TWeakObjectPtr<UMMItem> _Item)
{
	if (_OwningPoint && _Item.IsValid())
	{
		OwningPoint = _OwningPoint;
		
		if (IsValid(MeshComponent) && _Item.IsValid())
		{
			TSoftObjectPtr<UStaticMesh> Mesh = _Item.Get() -> GetItemMesh();
			const auto OnLoadedDelegate = FStreamableDelegate::CreateUObject(this, &ThisClass::SetItemMesh, Mesh);
			FMMAsyncLoader::LoadAsset(Mesh, OnLoadedDelegate);
		}
	
		if (IsValid(PickupUIPtr))
		{
			PickupUIPtr -> SetText(_Item.Get() -> GetItemName());
		}

		PickupItem = _Item;
	}

	if (PickupItem.IsValid())
	{
		return true;
	}

	return false;
}

void AMMPickupActor::SetItemMesh(TSoftObjectPtr<UStaticMesh> _Mesh)
{
	if (_Mesh.IsValid() && IsValid(MeshComponent))
	{
		//UE_LOG(LogTemp, Log, TEXT("AMMPickupActor::InitializePickup: %s Mesh Is Set"), *GetActorLabel());
		const auto Mesh = _Mesh.Get();
		MeshComponent -> SetStaticMesh(Mesh);
		SetActorHiddenInGame(false);
		FMMAsyncLoader::UnloadAsset(_Mesh);
	}
}

void AMMPickupActor::StartFocus()
{
	if (IsValid(PickupUIPtr) && IsValid(WidgetComponent))
	{
		WidgetComponent -> SetRelativeLocation(FVector::ZeroVector);
		MeshComponent -> SetRenderCustomDepth(true);
		MeshComponent -> SetCustomDepthStencilValue(InRangeStencilValue);
		PickupUIPtr -> ShowText(true);
	}
}

void AMMPickupActor::EndFocus()
{
	if (IsValid(PickupUIPtr))
	{
		MeshComponent -> SetRenderCustomDepth(false);
		PickupUIPtr -> ShowText(false);
	}
}

void AMMPickupActor::StartInteract()
{
	if (IsValid(MeshComponent))
	{
		MeshComponent -> SetCustomDepthStencilValue(InteractStencilValue);
	}
}

void AMMPickupActor::EndInteract()
{
	if (IsValid(MeshComponent))
	{
		MeshComponent -> SetCustomDepthStencilValue(InRangeStencilValue);
	}
}

TTuple<TWeakObjectPtr<AMMPickupActor>, TWeakObjectPtr<UMMItem>> AMMPickupActor::Interact()
{
	if (PickupItem.IsValid())
	{
		SetActorRotation(FRotator::ZeroRotator);
		SetActorHiddenInGame(true);
		
		return MakeTuple(this, PickupItem);
	}
	
	throw std::runtime_error("Invalid Item");
}

void AMMPickupActor::Deactivate()
{
	if (PickupItem.IsValid())
	{
		PickupItem.Reset();
	}

	if (IsValid(OwningPoint))
	{
		OwningPoint -> DeactivateItem();
	}
}

 EMMItemType AMMPickupActor::GetItemType()
{
	if (PickupItem.IsValid())
	{
		return PickupItem.Get() -> GetItemType();
	}

	return EMMItemType::IT_None;
}
 