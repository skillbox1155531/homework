// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <stdexcept>
#include "CoreMinimal.h"
#include "Ammo/MMAmmoInfo.h"
#include "Armor/MMArmorInfo.h"
#include "Consumables/MMConsumableInfo.h"
#include "Enums/MMItemType.h"
#include "Weapons/MMWeaponInfo.h"
#include "MMItemBase.generated.h"

UCLASS(Abstract, BlueprintType, Blueprintable, EditInlineNew, DefaultToInstanced)
class MONSTER_MAYHEM_API UMMItemBase : public UObject
{
	GENERATED_BODY()

public:
	UMMItemBase() : ItemID(0), ItemType(EMMItemType::IT_None), ItemAction(EMMItemAction::IA_None), Name(TEXT("")), Description(TEXT("")),
	Mesh(nullptr), Thumbnail(nullptr), bIsStackable(false), Quantity(1),
	WeaponInfo(FMMWeaponInfo()), ArmorInfo(FMMArmorInfo()), ConsumableInfo(FMMConsumableInfo()) {}

	virtual ~UMMItemBase() override;
	
	virtual void Use() PURE_VIRTUAL(UMMItemBase, )

	FORCEINLINE int32 GetItemId() const { return ItemID; }
	FORCEINLINE EMMItemType GetItemType() const { return ItemType; }
	FORCEINLINE EMMItemAction GetItemAction() const { return ItemAction; }
	FORCEINLINE FString GetItemName() const { return  Name; }
	FORCEINLINE FString GetItemDescription() const { return Description; }
	FORCEINLINE TSoftObjectPtr<UStaticMesh>  GetItemMesh() const { return Mesh; }
	FORCEINLINE TSoftObjectPtr<UTexture2D> GetItemThumbnail() const { return Thumbnail; }
	FORCEINLINE bool GetIsStackable() const { return bIsStackable; }
	FORCEINLINE int8 GetItemQuantity() const { return Quantity; }

	template <typename T> T GetItemParams() const
	{
		if constexpr (std::is_same_v<T, FMMWeaponInfo>)
		{
			return WeaponInfo;
		}
		else if constexpr (std::is_same_v<T, FMMArmorInfo>)
		{
			return ArmorInfo;
		}
		else if constexpr (std::is_same_v<T, FMMConsumableInfo>)
		{
			return ConsumableInfo;
		}
		else if constexpr (std::is_same_v<T, FMMAmmoInfo>)
		{
			return AmmoInfo;
		}

		throw std::runtime_error("Invalid item type");
	}
	
protected:
	UPROPERTY(EditAnywhere, Category= Parameters)
	int32 ItemID;
	UPROPERTY(EditAnywhere, Category= Parameters)
	EMMItemType ItemType;
	UPROPERTY(EditAnywhere, Category= Parameters)
	EMMItemAction ItemAction;
	UPROPERTY(EditAnywhere, Category= Parameters)
	FString Name;
	UPROPERTY(EditAnywhere, Category= Parameters, meta=(MultiLine = true))
	FString Description;
	UPROPERTY(EditAnywhere, Category= Parameters)
	TSoftObjectPtr<UStaticMesh> Mesh;
	UPROPERTY(EditAnywhere, Category= Parameters)
	TSoftObjectPtr<UTexture2D> Thumbnail;
	UPROPERTY(EditAnywhere, Category= Parameters)
	bool bIsStackable;
	UPROPERTY(EditAnywhere, Category= Parameters)
	int8 Quantity;
	UPROPERTY(EditAnywhere, Category= Parameters)
	FMMWeaponInfo WeaponInfo;
	UPROPERTY(EditAnywhere, Category= Parameters)
	FMMArmorInfo ArmorInfo;
	UPROPERTY(EditAnywhere, Category= Parameters)
	FMMConsumableInfo ConsumableInfo;
	UPROPERTY(EditAnywhere, Category= Parameters)
	FMMAmmoInfo AmmoInfo;
};
