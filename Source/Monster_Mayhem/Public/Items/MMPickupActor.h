// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UI/WMMPickup.h"
#include "Interfaces/IInteractable.h"
#include "MMPickupActor.generated.h"

class AMMItemSpawnPoint;
class UWidgetComponent;
class UMMItem;

UCLASS()
class MONSTER_MAYHEM_API AMMPickupActor : public AActor, public IInteractable
{
	GENERATED_BODY()


protected:
	virtual void BeginPlay() override;

public:	
	// Sets default values for this actor's properties
	AMMPickupActor();
	
	bool InitializePickup(AMMItemSpawnPoint* _OwningPoint, TWeakObjectPtr<UMMItem> _Item);
	void SetItemMesh(TSoftObjectPtr<UStaticMesh> _Mesh);
	virtual void StartFocus() override;
	virtual void EndFocus() override;
	virtual void StartInteract() override;
	virtual void EndInteract() override;
	virtual EMMItemType GetItemType() override;
	virtual TTuple<TWeakObjectPtr<AMMPickupActor>, TWeakObjectPtr<UMMItem>> Interact() override;
	void Deactivate();
	FORCEINLINE TWeakObjectPtr<UMMItem> GetPickupItem() const { return PickupItem; }


private:
	UPROPERTY()
	AMMItemSpawnPoint* OwningPoint;
	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* MeshComponent;
	UPROPERTY(EditDefaultsOnly)
	UWidgetComponent* WidgetComponent;
	UPROPERTY(VisibleAnywhere)
	UWMMPickup* PickupUIPtr;
	UPROPERTY(VisibleAnywhere)
	TWeakObjectPtr<UMMItem> PickupItem;
	UPROPERTY(EditDefaultsOnly)
	int8 InRangeStencilValue;
	UPROPERTY(EditDefaultsOnly)
	int8 InteractStencilValue;
	const FString WidgetClassPath = TEXT("/Game/BPs/UI/BP_UW_Pickup.BP_UW_Pickup_C");
};
