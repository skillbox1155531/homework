// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Items/MMItemBase.h"
#include "MMItem.generated.h"

UCLASS()
class MONSTER_MAYHEM_API UMMItem : public UMMItemBase
{
	GENERATED_BODY()

public:
	UMMItem()
	{
		AddToRoot();
	}

	virtual ~UMMItem() override
	{
		RemoveFromRoot();
	}
	
	virtual void Use() override;

	template <typename T>
	void Initialization(const int32& _ItemID, const EMMItemType& _ItemType, const FMMItemInfo& ItemInfo, const T& ItemParams)
	{
		ItemID = _ItemID;
		ItemType = _ItemType;
		Mesh = ItemInfo.GetStaticMesh();
		ItemAction = ItemInfo.GetItemAction();
		Thumbnail = ItemInfo.GetInventoryThumbnail();
		bIsStackable = ItemInfo.GetIsStackable();
		Quantity = ItemInfo.GetQuantity();
		Name = ItemInfo.GetName();
		Description = ItemInfo.GetDescription();

		if constexpr (std::is_same_v<T, FMMWeaponInfo>)
		{
			WeaponInfo = ItemParams;
		}
		else if constexpr (std::is_same_v<T, FMMArmorInfo>)
		{
			ArmorInfo = ItemParams;
		}
		else if  constexpr (std::is_same_v<T, FMMConsumableInfo>)
		{
			ConsumableInfo = ItemParams;
		}
		else if  constexpr (std::is_same_v<T, FMMAmmoInfo>)
		{
			AmmoInfo = ItemParams;
		}
	}
	
private:
	
};
