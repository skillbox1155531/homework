// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MMCharacter.h"
#include "Components/ActorComponent.h"
#include "Helper/DelegateRegistry.h"
#include "HUD/WMMCurrentWeapon.h"
#include "Items/MMItem.h"
#include "Weapons/MMWeaponInfo.h"
#include "MMWeaponsHandlerComponent.generated.h"

class UMMCharacterAnimInstance;
class AMMWeaponBase;
class UWMMCurrentWeapon;

enum class EMMMovementState : uint8;
struct FInputActionValue;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MONSTER_MAYHEM_API UMMWeaponsHandlerComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this component's properties
	UMMWeaponsHandlerComponent();
	
	void Initialization(const TWeakObjectPtr<AMMCharacter>& _CharacterPtr, const TWeakObjectPtr<UWMMCurrentWeapon>& CurrentWeaponUI);
	void InitializeWeaponSlots();
	void WeaponSwitch(const FInputActionValue& InputActionValue);
	void SelectNextWeapon();
	void SelectPreviousWeapon();
	void SetWeapon(const TWeakObjectPtr<UMMItemBase> Item, const FMMWeaponInfo& _WeaponInfo, const int8& Index);
	void InitializeWeapon(TWeakObjectPtr<AMMWeaponBase> Weapon, const TWeakObjectPtr<UMMItemBase>& Item, const FMMWeaponInfo& _WeaponInfo, const
	                      int8& Index);
	void FireCurrentWeapon(const FInputActionValue& InputActionValue, const EMMMovementState& MovementState, const FOnUpdateWeaponTrackingInfo& Delegate);
	void StartReloadCurrentWeapon(const FInputActionValue& InputActionValue);
	void UpdateCurrentWeaponUI(const FInputActionValue& InputActionValue);
	void SetCurrentDispersionParams(const EMMMovementState& MovementState);
	void AddAmmoToEquippedWeapons(const EMMWeaponSlotType& WeaponType);
	void SetCurrentWeaponUI(UTexture2D* Image, const int8& AmmoInClip, const int32& AmmoInReserve);
	void PlayAnimMontage(TWeakObjectPtr<UAnimMontage> AnimMontage);
	
	EMMWeaponType GetCurrentWeaponType() const;
	FORCEINLINE TWeakObjectPtr<AMMWeaponBase> GetCurrentWeapon() const { return CurrentWeapon; }
	FORCEINLINE TWeakObjectPtr<AMMWeaponBase> GetSecondaryWeapon() const { return SecondaryWeapon; }
	FORCEINLINE TWeakObjectPtr<AMMWeaponBase> GetHeavyWeapon() const { return HeavyWeapon; }
private:
	UPROPERTY()
	TWeakObjectPtr<AMMCharacter> CharacterPtr;
	UPROPERTY()
	TWeakObjectPtr<UMMCharacterAnimInstance> CharacterAnimInstancePtr;
	UPROPERTY()
	TWeakObjectPtr<UWMMCurrentWeapon> CurrentWeaponUIPtr;
	UPROPERTY(VisibleAnywhere, Category= Weapon)
	TWeakObjectPtr<AMMWeaponBase> CurrentWeapon;
	UPROPERTY(VisibleAnywhere, Category= Weapon)
	TWeakObjectPtr<AMMWeaponBase> DefaultWeapon;
	UPROPERTY(VisibleAnywhere, Category= Weapon)
	TWeakObjectPtr<AMMWeaponBase> SecondaryWeapon;
	UPROPERTY(VisibleAnywhere, Category= Weapon)
	TWeakObjectPtr<AMMWeaponBase> HeavyWeapon;
	UPROPERTY(VisibleAnywhere, Category= Weapon)
	int8 CurrentWeaponIndex;
	UPROPERTY()
	TArray<TWeakObjectPtr<AMMWeaponBase>> EquippedWeaponsArr;

	
	const FName WeaponSocket = TEXT("hand_rSocket");
};
