// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DataBase/MMItemsDataBase.h"
//#include "Weapons/MMWeaponInfo.h"
#include "MMInventoryComponent.generated.h"

class UMMItemBase;
class AMMCharacter;
class UWMMInventory;
enum class EMMWeaponSlotType : uint8;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MONSTER_MAYHEM_API UMMInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UMMInventoryComponent();

private:
protected:
	virtual void BeginPlay() override;

public:
	void Initialization(const TWeakObjectPtr<AMMCharacter>& _CharacterPtr, const TWeakObjectPtr<UWMMInventory>& _InventoryUI);
	void AddDefaultWeaponToInventory(const FDataTableRowHandle& DefaultWeaponID);
	void AddItemToInventory(const TWeakObjectPtr<UMMItemBase>& Item);
	bool SetEmptySlotInInventory(const FPlatformTypes::int8& Index,TWeakObjectPtr<UMMItemBase> Item);
	void RemoveItemFromInventory(TWeakObjectPtr<UMMItemBase> Item);
	bool ClearSlotInInventory(const int8& Index, TWeakObjectPtr<UMMItemBase> Item);
	void ReorderSlotsInInventory(const int8& Index);
	void UseItemInInventory(TWeakObjectPtr<UMMItemBase> Item);
	typedef TTuple<TWeakObjectPtr<UMMItemBase>, const int8> FSlot;
	FSlot FindSlotInInventory(const int8& Index, TWeakObjectPtr<UMMItemBase> Item);
	void EquipWeapon(const FSlot& InventorySlot);
	void UpdateWeaponInfo(const int32& _Id, const FMMWeaponBaseParams& BaseParams, const FMMWeaponTrackingInfo& TrackingInfo);
	
	FORCEINLINE int8 GetInventorySlots() const { return InventorySlots; }
private:
	UPROPERTY()
	TWeakObjectPtr<AMMCharacter> CharacterPtr;
	UPROPERTY()
	TWeakObjectPtr<UWMMInventory> InventoryUIPtr;
	UPROPERTY(VisibleAnywhere)
	TArray<TWeakObjectPtr<UMMItemBase>> InventorySlotsArr;
	UPROPERTY(EditDefaultsOnly, Category= Inventory)
	int8 InventorySlots;

	UPROPERTY(VisibleAnywhere)
	TMap<int32, FMMWeaponInfo> OwnedWeaponsDic;
	TMap<EMMWeaponSlotType, int8> WeaponSlotsIndexDic;
};
