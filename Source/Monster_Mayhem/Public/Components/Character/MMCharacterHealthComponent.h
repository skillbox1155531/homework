// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/Base/MMBaseHealthComponent.h"
#include "Helper/DelegateRegistry.h"
#include "MMCharacterHealthComponent.generated.h"

class UWMMBar;
class UMaterialInstanceDynamic;

UCLASS()
class MONSTER_MAYHEM_API UMMCharacterHealthComponent : public UMMBaseHealthComponent
{
	GENERATED_BODY()

	UMMCharacterHealthComponent();
private:
	void ApplyShieldMaterial(const float& Amount);
public:
	void Initialization(const TWeakObjectPtr<AMMCharacter>& CharacterPtr, const TWeakObjectPtr<UWMMBar>& _HealthBarPtr, const TWeakObjectPtr<
	                    UWMMBar>& _ShieldBarPtr);
	virtual bool DecreaseHealth(const float& Amount) override;
	virtual void IncreaseHealth(const float& Amount) override;
	void IncreaseShield(const float& Amount);
	void SetIsShielded(const bool& _bIsShielded);
	void CallShieldSpawnRequest();
	void ScheduleShieldSpawnRequest(const float& Timer);

	FORCEINLINE bool GetIsShielded() const { return bIsShielded; }
private:
	TWeakObjectPtr<UWMMBar> HealthBarPtr;
	TWeakObjectPtr<UWMMBar> ShieldBarPtr;
	TWeakObjectPtr<USkeletalMeshComponent> ShellMeshPtr;
	TWeakObjectPtr<USkeletalMeshComponent> CharacterMeshPtr;
	UPROPERTY(EditAnywhere, Category= Parameters)
	bool bIsShielded;
	UPROPERTY(VisibleAnywhere, Category= Parameters)
	float CurrentShield;
	UPROPERTY(EditAnywhere, Category= Parameters)
	float MaxShield;
	UPROPERTY(EditAnywhere, Category= Parameters)
	float DamageReductionValue;

	
	UPROPERTY(EditDefaultsOnly)
	TMap<float, UMaterialInstance*> ShieldsDic;
	FTimerHandle ShieldSpawnRequestTimerHandler;
	UPROPERTY(EditAnywhere, Category= Parameters)
	float InitShieldSpawnRequestTimer;
	UPROPERTY(EditAnywhere, Category= Parameters)
	float MinShieldSpawnRequestTimer;
	UPROPERTY(EditAnywhere, Category= Parameters)
	float MaxShieldSpawnRequestTimer;
public:
	FOnRequestConsumableSpawn OnRequestConsumable;
};
