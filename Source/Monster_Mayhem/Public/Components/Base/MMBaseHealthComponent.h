// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MMBaseHealthComponent.generated.h"

class AMMCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MONSTER_MAYHEM_API UMMBaseHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMMBaseHealthComponent();
	
	virtual bool DecreaseHealth(const float& Amount);
	virtual void IncreaseHealth(const float& Amount);
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = Parameters)
	float MaxHealth;
	UPROPERTY(VisibleAnywhere, Category = Parameters)
	float CurrentHealth;
};
