﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Core/MMGameInstance.h"
#include "MMBaseHitHandlerComponent.generated.h"

class AMMImpactVisual;
enum class EMMObjectMaterialType : uint8;
class UNiagaraSystem;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class MONSTER_MAYHEM_API UMMBaseHitHandlerComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UMMBaseHitHandlerComponent();

private:
	void SpawnImpactFXPool(TSoftObjectPtr<UNiagaraSystem> Impact);
	
public:
	void LoadImpactFX(const TWeakObjectPtr<UMMGameInstance>& GameInstance);
	bool ShowImpactVisual(const FVector& Location);
	void SpawnImpact();
	
private:
	UPROPERTY(VisibleAnywhere)	
	UNiagaraSystem* ImpactSystem;

	UPROPERTY(EditAnywhere)
	EMMObjectMaterialType MaterialType; 
	UPROPERTY(EditDefaultsOnly)
	int8 ImpactVisualsSpawnAmount;
	UPROPERTY(VisibleAnywhere)
	TArray<AMMImpactVisual*> ImpactsArr;
};
