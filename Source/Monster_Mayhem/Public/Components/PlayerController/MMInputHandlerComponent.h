// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MMInputHandlerComponent.generated.h"

class AMMPlayerController;
class AMMCharacter;
class UMMInputConfigData;

struct FInputActionValue;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MONSTER_MAYHEM_API UMMInputHandlerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UMMInputHandlerComponent();

protected:
	virtual void BeginPlay() override;

private:
	void Initialization();
	void AddInputContext(AMMPlayerController* PlayerController) const;
	void BindInputEvents(AMMPlayerController* PlayerController);

	void HandleStartMoveForward(const FInputActionValue& InputActionValue);
	void HandleMoveRight(const FInputActionValue& InputActionValue);
	void HandleSprint(const FInputActionValue& InputActionValue);
	void HandleSwitchWeapon(const FInputActionValue& InputActionValue);
	void HandleFire(const FInputActionValue& InputActionValue);
	void HandleReload(const FInputActionValue& InputActionValue);
	void HandleAim(const FInputActionValue& InputActionValue);
	void HandleZoom(const FInputActionValue& InputActionValue);
	void HandleInteract(const FInputActionValue& InputActionValue);
	
public:
private:
	UPROPERTY()
	AMMPlayerController* PlayerControllerPtr;
	UPROPERTY()
	AMMCharacter* CharacterPtr;
	UPROPERTY(VisibleDefaultsOnly, Category="Input Config")
	UMMInputConfigData* InputConfigData;
	const FString InputConfigDataPath = TEXT("/Script/Monster_Mayhem.MMInputConfigData'/Game/Input/InputConfigData/DA_InputConfigData.DA_InputConfigData'");
	
	UPROPERTY(VisibleAnywhere)
	bool bIsHoldingAim;
};