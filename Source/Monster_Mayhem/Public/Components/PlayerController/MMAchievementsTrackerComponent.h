// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Weapons/MMWeaponInfo.h"
#include "MMAchievementsTrackerComponent.generated.h"

class UMMGameInstance;
class UWMMAchievementBlock;
class AMMItemSpawner;
struct FMMWeaponsDataBase;
class AMMPlayerController;

USTRUCT()
struct FMMTrackingWeaponInfo
{
	GENERATED_BODY()

	FMMTrackingWeaponInfo() :
	CurrentTrackingWeapon(EMMWeaponType::WT_None),NextTrackingWeapon(EMMWeaponType::WT_None), Thumbnail(nullptr),
	CurrentWeaponKills(0), WeaponsKillsToUnlock(0) {}

	FORCEINLINE EMMWeaponType GetCurrentTrackingWeapon() const { return CurrentTrackingWeapon; }
	FORCEINLINE EMMWeaponType GetNextTrackingWeapon() const { return NextTrackingWeapon; }
	FORCEINLINE FString GetCurrentTrackingWeaponName() const { return CurrentTrackingWeaponName; }
	FORCEINLINE FString GetNextTrackingWeaponName() const { return NextTrackingWeaponName; }
	FORCEINLINE TSoftObjectPtr<UTexture2D> GetThumbnail() const { return Thumbnail; }
	FORCEINLINE int32 GetCurrentWeaponKills() const { return CurrentWeaponKills; }
	FORCEINLINE int32 GetWeaponsKillsToUnlock() const { return WeaponsKillsToUnlock; }

	FORCEINLINE void SetCurrentTrackingWeapon(const EMMWeaponType& _CurrentTrackingWeapon) { CurrentTrackingWeapon = _CurrentTrackingWeapon; }
	FORCEINLINE void SetNextTrackingWeapon(const EMMWeaponType& _NextTrackingWeapon) { NextTrackingWeapon = _NextTrackingWeapon; }
	FORCEINLINE void SetCurrentTrackingWeaponName(const FString& _CurrentTrackingWeaponName) { CurrentTrackingWeaponName = _CurrentTrackingWeaponName; }
	FORCEINLINE void SetNextTrackingWeaponName(const FString& _NextTrackingWeaponName) { NextTrackingWeaponName = _NextTrackingWeaponName; }
	FORCEINLINE void SetThumbnail(const TSoftObjectPtr<UTexture2D>& _Thumbnail) { Thumbnail = _Thumbnail; }
	FORCEINLINE void SetCurrentTrackingWeaponKills(const int32& _CurrentTrackingWeaponKills) { CurrentWeaponKills = _CurrentTrackingWeaponKills; }
	FORCEINLINE void SetWeaponsKillsToUnlock(const int32& _WeaponsKillsToUnlock) { WeaponsKillsToUnlock = _WeaponsKillsToUnlock; }
	
private:
	EMMWeaponType CurrentTrackingWeapon;
	EMMWeaponType NextTrackingWeapon;
	FString CurrentTrackingWeaponName;
	FString NextTrackingWeaponName;
	TSoftObjectPtr<UTexture2D> Thumbnail;
	int32 CurrentWeaponKills;
	int32 WeaponsKillsToUnlock;
	
};

struct FMMWeaponNode
{
	FMMWeaponNode() : Value(nullptr), NextNode(nullptr) {}
	FMMWeaponNode(FMMWeaponsDataBase* _Value, const TSharedPtr<FMMWeaponNode>& _NextNode) : Value(_Value), NextNode(_NextNode) {}

	FMMWeaponsDataBase*  GetValue() const { return Value; }
	TSharedPtr<FMMWeaponNode> GetNextNode() const { return NextNode; }

	void SetValue(FMMWeaponsDataBase* _Value) { Value = _Value; }
	void SetNextNode(const TSharedPtr<FMMWeaponNode>& _NextNode) { NextNode = _NextNode; }

private:
	FMMWeaponsDataBase* Value;
	TSharedPtr<FMMWeaponNode> NextNode;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MONSTER_MAYHEM_API UMMAchievementsTrackerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMMAchievementsTrackerComponent();

private:
	void CreateWeaponSpawnList(const TWeakObjectPtr<UMMGameInstance>& _GameInstance);
	void TrackCurrentWeapon(const EMMWeaponType& WeaponType, const FMMWeaponTrackingInfo& TrackingInfo);
	bool SetWeaponTrackingInfo();

public:
	void Initialization(const TWeakObjectPtr<UMMGameInstance>& GameInstance, const TWeakObjectPtr<AMMPlayerController>& _PlayerControllerPtr,
													 const TWeakObjectPtr<AMMItemSpawner>& _ItemSpawnerPtr, const TWeakObjectPtr<UWMMAchievementBlock>& _AchievementBlockPtr);
private:
	TWeakObjectPtr<AMMItemSpawner> ItemSpawnerPtr;
	TWeakObjectPtr<UWMMAchievementBlock> AchievementBlockPtr;

	UPROPERTY()
	FMMTrackingWeaponInfo TrackingWeaponInfo;

	TSharedPtr<FMMWeaponNode> HeadNode;
	TWeakPtr<FMMWeaponNode> CurrentUnlockedNode;
};
