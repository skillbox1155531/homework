// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Structs/Character/MMCameraParams.h"
#include "MMCameraHandlerComponent.generated.h"

class AMMPlayerController;
class AMMCharacter;
class USpringArmComponent;

struct FInputActionValue;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MONSTER_MAYHEM_API UMMCameraHandlerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UMMCameraHandlerComponent();
	
private:
	void SetCameraPosition(const FVector& TargetPos, const float& DeltaSeconds);
	void CameraAimZoom(const bool& bIsAim, const FInputActionValue& InputActionValue);
	void CameraViewZoom(const bool& bIsAim, const FInputActionValue& InputActionValue);
	void CameraAimZoomIn(const float& DeltaSeconds);
	void CameraAimZoomOut(const float& DeltaSeconds);
	void CameraViewZoomIn();
	void CameraViewZoomOut();
	
	
protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY()
	AMMPlayerController* PlayerControllerPtr;
	UPROPERTY()
	USpringArmComponent* CameraBoomPtr;

	UPROPERTY(VisibleAnywhere, Category= "Parameters | Camera")
	FMMCameraParams CameraParams;

	float CurrentCameraHeight;
};
