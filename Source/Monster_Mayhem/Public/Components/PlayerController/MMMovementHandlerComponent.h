// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MMMovementHandlerComponent.generated.h"

class AMMPlayerController;
class AMMCharacter;
class USpringArmComponent;

struct FInputActionValue;
enum class EMMMovementState : uint8;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MONSTER_MAYHEM_API UMMMovementHandlerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMMMovementHandlerComponent();

private:
	void MoveCharacterForward(const FInputActionValue& InputActionValue);
	void MoveCharacterRight(const FInputActionValue& InputActionValue);
	void CharacterSprint(const FInputActionValue& InputActionValue);
	void CharacterAim(const FInputActionValue& InputActionValue);
	
protected:
	virtual void BeginPlay() override;
	
private:
	UPROPERTY()
	AMMPlayerController* PlayerControllerPtr;
	UPROPERTY()
	AMMCharacter* CharacterPtr;
};
