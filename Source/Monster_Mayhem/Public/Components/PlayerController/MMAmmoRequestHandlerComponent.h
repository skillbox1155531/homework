// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Structs/DataBase/MMAmmoDataBase.h"
#include "MMAmmoRequestHandlerComponent.generated.h"

class UMMGameInstance;
class AMMPlayerController;
class AMMCharacter;
class AMMItemSpawner;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MONSTER_MAYHEM_API UMMAmmoRequestHandlerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMMAmmoRequestHandlerComponent();

private:
	FMMAmmoDataBase* GetAmmoFromArr(const EMMWeaponSlotType& AmmoType);
	
public:
	void Initialization(const TWeakObjectPtr<UMMGameInstance>& GameInstance,
	                    const TWeakObjectPtr<AMMPlayerController>& PlayerController, const TWeakObjectPtr<AMMItemSpawner>& _ItemSpawnerPtr);
	bool TryToSpawnAmmo(const EMMWeaponSlotType& Slot, const float& Weight);
	bool SendAmmoToSpawner(const FMMAmmoDataBase* AmmoToSpawn);
	void CallSpawnAmmo(const EMMWeaponSlotType& Slot);
	
private:
	TWeakObjectPtr<AMMItemSpawner> ItemSpawnerPtr;
	TArray<FMMAmmoDataBase*> AmmoArr;
	TMultiMap<EMMWeaponSlotType, FMMAmmoDataBase*> AmmoDic;
	UPROPERTY(EditAnywhere, Category = Parameters)
	float SpecialAmmoDropWeight;
	UPROPERTY(EditAnywhere, Category = Parameters)
	float DefaultSpecialAmmoDropWeightMultiplier;
	UPROPERTY(EditAnywhere, Category = Parameters)
	float SpecialAmmoDropWeightMultiplier;
	UPROPERTY(EditAnywhere, Category = Parameters)
	float HeavyAmmoDropWeight;
	UPROPERTY(EditAnywhere, Category = Parameters)
	float DefaultHeavyAmmoDropWeightMultiplier;
	UPROPERTY(EditAnywhere, Category = Parameters)
	float HeavyAmmoDropWeightMultiplier;
};
