// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MMConsumableRequestHandlerComponent.generated.h"

class UMMGameInstance;
class AMMPlayerController;
class AMMItemSpawner;
struct FMMConsumablesDataBase;
enum class EMMConsumableType : uint8;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MONSTER_MAYHEM_API UMMConsumableRequestHandlerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMMConsumableRequestHandlerComponent();

private:
	FMMConsumablesDataBase* GetConsumableFromArr(const EMMConsumableType& ConsumableType);
	FMMConsumablesDataBase* RandomizeConsumable(const TArray<FMMConsumablesDataBase*>& Arr);
	bool TryToSpawnConsumable(const EMMConsumableType& ConsumableType, const float& Weight);
	bool SendConsumableToSpawner(const FMMConsumablesDataBase* Consumable);
	void CallSpawnConsumable(const EMMConsumableType& ConsumableType);
	
public:
	void Initialization(const TWeakObjectPtr<UMMGameInstance>& GameInstance, const TWeakObjectPtr<AMMPlayerController>& _PlayerControllerPtr,
											 const TWeakObjectPtr<AMMItemSpawner>& _ItemSpawnerPtr);

private:
	TWeakObjectPtr<AMMItemSpawner> ItemSpawnerPtr;
	TArray<FMMConsumablesDataBase*> ConsumablesArr;
	TMultiMap<EMMConsumableType, FMMConsumablesDataBase*> ConsumableDic;
	UPROPERTY(EditAnywhere, Category = Parameters)
	float HealthDropWeight;
	UPROPERTY(EditAnywhere, Category = Parameters)
	float DefaultHealthDropWeightMultiplier;
	UPROPERTY(EditAnywhere, Category = Parameters)
	float HealthDropWeightMultiplier;
	UPROPERTY(EditAnywhere, Category = Parameters)
	float ShieldDropWeight;
	UPROPERTY(EditAnywhere, Category = Parameters)
	float DefaultShieldDropWeightMultiplier;
	UPROPERTY(EditAnywhere, Category = Parameters)
	float ShieldDropWeightMultiplier;
};
