// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WMMBar.generated.h"

class UHorizontalBox;
class UWMMBarSegment;

UENUM(BlueprintType)
enum class EMMBarType : uint8
{
	BT_Health UMETA(DisplayName = "Health"),
	BT_Shield UMETA(DisplayName = "Shield"),
	BT_None UMETA(DisplayName = "None")
};

UCLASS()
class MONSTER_MAYHEM_API UWMMBar : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;
	
public:
	void Initialization(const float& MaxValue);
	void SetBarValue(const float& Amount);
	void IncreaseBarValue(float Amount);
	void DecreaseBarValue(float Amount);
private:
	UPROPERTY(EditDefaultsOnly)
	EMMBarType Type;
	UPROPERTY(VisibleAnywhere, meta=(BindWidget))
	UHorizontalBox* BarSegmentsHolder;
	UPROPERTY(EditDefaultsOnly)
	FLinearColor SegmentColor;
	UPROPERTY(EditDefaultsOnly)
	int8 SegmentsNumber;
	UPROPERTY(VisibleAnywhere)
	float SegmentValue;
	UPROPERTY(VisibleAnywhere, meta=(BindWidget))
	TArray<TWeakObjectPtr<UWMMBarSegment>> SegmentsArr;
	const FString HealthSegmentClassPath = TEXT("/Game/BPs/UI/HUD/BP_UW_BarSegment.BP_UW_BarSegment_C");
};
