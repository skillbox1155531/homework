// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WMMCurrentWeapon.generated.h"

class UImage;
class UTextBlock;

UCLASS()
class MONSTER_MAYHEM_API UWMMCurrentWeapon : public UUserWidget
{
	GENERATED_BODY()

private:
	void SetAmmoText(const int8& AmmoInClip, const int32& AmmoInReserve);
	void LoadTextureToSlot(TSoftObjectPtr<UTexture2D> Texture);
	void SetTextureToSlot(TSoftObjectPtr<UTexture2D> Texture);
	
public:
	void SetWeapon(TSoftObjectPtr<UTexture2D> Image, const int8& AmmoInClip, const int32& AmmoInReserve);
	void UpdateAmmoText(const int8& AmmoInClip, const int32& AmmoInReserve);
private:
	UPROPERTY(meta=(BindWidget))
	UImage* Thumbnail;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* AmmoTextBlock;
};
