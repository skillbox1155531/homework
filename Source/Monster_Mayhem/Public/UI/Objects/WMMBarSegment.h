// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WMMBarSegment.generated.h"

class UProgressBar;

UCLASS()
class MONSTER_MAYHEM_API UWMMBarSegment : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetColor(const FLinearColor& Color);
	void SetValue(const float& _Amount);
	void IncreaseValue(const float& _Amount);
	void DecreaseValue(const float& _Amount);
	float GetValue();
private:
	UPROPERTY(VisibleAnywhere, meta=(BindWidget))
	UProgressBar* Segment;
};
