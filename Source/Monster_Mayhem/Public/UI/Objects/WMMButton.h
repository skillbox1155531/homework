// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "Helper/DelegateRegistry.h"
#include "WMMButton.generated.h"

class UButton;
class UTextBlock;

UCLASS()
class MONSTER_MAYHEM_API UWMMButton : public UUserWidget
{
	GENERATED_BODY()

private:
	virtual void NativeConstruct() override;
	UFUNCTION()
	void OnButtonClicked();
	
public:
	FORCEINLINE void SetText(const FString& Text) { ButtonText -> SetText(FText::FromString(Text)); }
	
private:
	UPROPERTY(meta=(BindWidget))
	UButton* MainButton;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* ButtonText;

public:
	FOnButtonClicked OnButtonClickedDelegate;
};
