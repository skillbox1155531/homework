// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Core/MMGameMode.h"
#include "WMMAchievementBlock.generated.h"


class UImage;
class UTextBlock;
enum class EMMWeaponType : uint8;

UCLASS()
class MONSTER_MAYHEM_API UWMMAchievementBlock : public UUserWidget
{
	GENERATED_BODY()

private:
	void SetKillsToUnlockText(const int32& CurrentKills, const int32& KillsToUnlock);
	void SetWeaponToUnlockText(const FString& CurrentWeapon, const FString& NextWeapon);
	void LoadTextureToSlot(TSoftObjectPtr<UTexture2D> Texture);
	void SetTextureToSlot(TSoftObjectPtr<UTexture2D> Texture);
	FString GetDisplayNameFromEnum(EMMWeaponType Enum);
public:
	void SetBlock(const TSoftObjectPtr<UTexture2D>& Image, const FString& CurrentWeapon, const FString& NextWeapon, const int32&
	              CurrentKills, const int32& KillsToUnlock);
	void UpdateAmmoText(const int32& CurrentKills, const int32& KillsToUnlock);
private:
	UPROPERTY(meta=(BindWidget))
	UImage* Thumbnail;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* GoalDescriptionTextBlock;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* KillsToUnlockTextBlock;
};
