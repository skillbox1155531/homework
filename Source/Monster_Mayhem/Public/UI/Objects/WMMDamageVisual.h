// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WMMDamageVisual.generated.h"

class UTextBlock;
class UWidgetAnimation;

UCLASS()
class MONSTER_MAYHEM_API UWMMDamageVisual : public UUserWidget
{
	GENERATED_BODY()

private:
public:
	void EnableDamageAnim(const float& DamageNumber);
	UWidgetAnimation* GetWidgetAnimation() const { return WidgetAnimation; }
private:
	UPROPERTY(meta=(BindWidget))
	UTextBlock* DamageTextBlock;
	UPROPERTY(Transient, meta=(BindWidgetAnim))
	UWidgetAnimation* WidgetAnimation;
};
