// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WMMPickup.generated.h"

class UTextBlock;

UCLASS()
class MONSTER_MAYHEM_API UWMMPickup : public UUserWidget
{
	GENERATED_BODY()

private:
	void NativeConstruct() override;
	
public:
	void SetText(const FString& Text);
	void ShowText(const bool& bIsVisible);
	
private:
	UPROPERTY(meta=(BindWidget))
	UTextBlock* PickupText;
};
