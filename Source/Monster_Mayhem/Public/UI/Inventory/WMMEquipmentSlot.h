// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WMMEquipmentSlot.generated.h"

class UImage;
class UTexture2D;
class UMMItemBase;

enum class EMMItemType : uint8;
enum class EMMWeaponSlotType : uint8;
enum class EMMArmorType : uint8;

UCLASS()
class MONSTER_MAYHEM_API UWMMEquipmentSlot : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void Initialization(const bool& bIsVisible);
	void SetThumbnail(TSoftObjectPtr<UTexture2D> Texture);
	void LoadTextureToSlot(TSoftObjectPtr<UTexture2D> Texture);
	void SetTextureToSlot(TSoftObjectPtr<UTexture2D> Texture);
	void SetItem(TWeakObjectPtr<UMMItemBase> _Item);
	void SetSlotVisibility(const bool& bIsVisible);

	FORCEINLINE EMMItemType GetSlotType() const { return SlotType; }
	FORCEINLINE EMMWeaponSlotType GetWeaponSlotType() const { return WeaponType; }
	FORCEINLINE EMMArmorType GetArmorSlotType() const { return ArmorType; }
	FORCEINLINE TWeakObjectPtr<UMMItemBase>  GetItemInSlot() const { return Item; }
	
private:
	UPROPERTY(EditAnywhere)
	EMMItemType SlotType;
	UPROPERTY(EditAnywhere,  meta=(EditCondition = "SlotType == EMMItemType::IT_Weapon", EditConditionHides))
	EMMWeaponSlotType WeaponType;
	UPROPERTY(EditAnywhere,  meta=(EditCondition = "SlotType == EMMItemType::IT_Armor", EditConditionHides))
	EMMArmorType ArmorType;
	UPROPERTY(meta=(BindWidget))
	UImage* Thumbnail;
	TWeakObjectPtr<UMMItemBase> Item;
};

