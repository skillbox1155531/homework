// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WMMInventory.generated.h"

enum class EMMItemAction : uint8;
class UWMMEquipmentSlot;
class UWrapBox;
class UWMMInventorySlot;
class UMMItemBase;
class UMMInventoryComponent;

enum class EMMWeaponSlotType : uint8;
enum class EMMArmorType : uint8;
enum class EMMItemType : uint8;

UCLASS()
class MONSTER_MAYHEM_API UWMMInventory : public UUserWidget
{
	GENERATED_BODY()

private:
	virtual void NativeConstruct() override;
	FString GetDisplayNameFromEnum(EMMItemAction Enum);
public:
	void InitializeInventory(UMMInventoryComponent* InventoryComponent);
	void AddItemToSlot(const int8& Index, const TWeakObjectPtr<UMMItemBase>& Item);
	void RemoveItemFromSlot(const int8& Index);
	void UseItemInSlot(const TWeakObjectPtr<UMMItemBase>& Item);
	typedef TTuple<TWeakObjectPtr<UMMItemBase>, EMMWeaponSlotType> FWeapon;
	void EquipWeapon(const FWeapon& Item);
	typedef TTuple<UMMItemBase*, EMMArmorType> FArmor;
	void EquipArmor(FArmor Item);
	
private:
	UPROPERTY()
	UMMInventoryComponent* InventoryComponentPtr;
	UPROPERTY(meta=(BindWidget))
	UWrapBox* InventoryBox;
	UPROPERTY()
	TArray<UWMMInventorySlot*> InventorySlotArr;
	UPROPERTY()
	TMap<UWMMEquipmentSlot*, EMMItemType> EquipmentSlotDic;
	
	const FString InventorySlotClassPath = TEXT("/Script/UMGEditor.WidgetBlueprint'/Game/BPs/UI/Inventory/BP_UW_InteventorySlot.BP_UW_InteventorySlot_C'");
};
