// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WMMInventory.h"
#include "Blueprint/UserWidget.h"
#include "WMMInventorySlot.generated.h"

class UImage;
class UTextBlock;
class UWMMButton;
class UWMMInventory;
class UMMItemBase;

UCLASS()
class MONSTER_MAYHEM_API UWMMInventorySlot : public UUserWidget
{
	GENERATED_BODY()

private:
	virtual void NativeConstruct() override;
	void ExecuteSlotAction(UWMMButton* Button);
	void LoadTextureToSlot(TSoftObjectPtr<UTexture2D> Texture);
	void SetTextureToSlot(TSoftObjectPtr<UTexture2D> Texture);
	
public:
	void SetInventoryOwner(UWMMInventory* _Inventory);
	void SetThumbnail(TSoftObjectPtr<UTexture2D> Texture);
	void DeleteThumbnail();
	void SetButtonText(const FString& Text);
	void SetItem(TWeakObjectPtr<UMMItemBase> _Item);
	void SetSlotVisibility(const bool& bIsVisible);

	FORCEINLINE TWeakObjectPtr<UMMItemBase> GetItemInSlot() const { return Item; }
private:
	UPROPERTY()
	UWMMInventory* Inventory;
	UPROPERTY(meta=(BindWidget))
	UImage* Thumbnail;
	UPROPERTY(meta=(BindWidget))
	UWMMButton* ActionButton;
	TWeakObjectPtr<UMMItemBase> Item;
};
