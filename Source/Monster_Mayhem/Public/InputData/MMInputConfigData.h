// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputAction.h"
#include "InputMappingContext.h"
#include "Engine/DataAsset.h"
#include "MMInputConfigData.generated.h"

UCLASS()
class MONSTER_MAYHEM_API UMMInputConfigData : public UDataAsset
{
	GENERATED_BODY()

	UMMInputConfigData()
	{
		IMC_BaseContext = LoadObject<UInputMappingContext>(nullptr, *IMC_BaseContextPath);
		IA_MoveForward = LoadObject<UInputAction>(nullptr, *IA_MoveForwardPath);
		IA_MoveBackward = LoadObject<UInputAction>(nullptr, *IA_MoveBackwardPath);
		IA_MoveLeft = LoadObject<UInputAction>(nullptr, *IA_MoveLeftPath);
		IA_MoveRight = LoadObject<UInputAction>(nullptr, *IA_MoveRightPath);
		IA_Sprint = LoadObject<UInputAction>(nullptr, *IA_SprintPath);
		IA_SwitchWeapon = LoadObject<UInputAction>(nullptr, *IA_SwitchWeaponPath);
		IA_Fire = LoadObject<UInputAction>(nullptr, *IA_FirePath);
		IA_Reload = LoadObject<UInputAction>(nullptr, *IA_ReloadPath);
		IA_Aim = LoadObject<UInputAction>(nullptr, *IA_AimPath);
		IA_ZoomIn = LoadObject<UInputAction>(nullptr, *IA_ZoomInPath);
		IA_ZoomOut = LoadObject<UInputAction>(nullptr, *IA_ZoomOutPath);
		IA_Interact = LoadObject<UInputAction>(nullptr, *IA_InteractPath);

		ContextPriority = 0;
	}
	
public:
	UPROPERTY(VisibleDefaultsOnly, Category= "Context Settings")
	TSoftObjectPtr<UInputMappingContext> IMC_BaseContext;
	UPROPERTY(VisibleDefaultsOnly, Category= "Context Settings")
	int32 ContextPriority;
	UPROPERTY(VisibleDefaultsOnly, Category= "Input Actions")
	TSoftObjectPtr<UInputAction> IA_MoveForward;
	UPROPERTY(VisibleDefaultsOnly, Category= "Input Actions")
	TSoftObjectPtr<UInputAction> IA_MoveBackward;
	UPROPERTY(VisibleDefaultsOnly, Category= "Input Actions")
	TSoftObjectPtr<UInputAction> IA_MoveLeft;
	UPROPERTY(VisibleDefaultsOnly, Category= "Input Actions")
	TSoftObjectPtr<UInputAction> IA_MoveRight;
	UPROPERTY(VisibleDefaultsOnly, Category= "Input Actions")
	TSoftObjectPtr<UInputAction> IA_Sprint;
	UPROPERTY(VisibleDefaultsOnly, Category= "Input Actions")
	TSoftObjectPtr<UInputAction> IA_SwitchWeapon;
	UPROPERTY(VisibleDefaultsOnly, Category= "Input Actions")
	TSoftObjectPtr<UInputAction> IA_Fire;
	UPROPERTY(VisibleDefaultsOnly, Category= "Input Actions")
	TSoftObjectPtr<UInputAction> IA_Reload;
	UPROPERTY(VisibleDefaultsOnly, Category= "Input Actions")
	TSoftObjectPtr<UInputAction> IA_Aim;
	UPROPERTY(VisibleDefaultsOnly, Category= "Input Actions")
	TSoftObjectPtr<UInputAction> IA_ZoomIn;
	UPROPERTY(VisibleDefaultsOnly, Category= "Input Actions")
	TSoftObjectPtr<UInputAction> IA_ZoomOut;
	UPROPERTY(VisibleDefaultsOnly, Category= "Input Actions")
	TSoftObjectPtr<UInputAction> IA_Interact;

private:
	const FString IMC_BaseContextPath = TEXT("/Script/EnhancedInput.InputMappingContext'/Game/Input/Context/IMC_BaseContext.IMC_BaseContext'");
	const FString IA_MoveForwardPath = TEXT("/Script/EnhancedInput.InputAction'/Game/Input/Actions/IA_MoveForward.IA_MoveForward'");
	const FString IA_MoveBackwardPath = TEXT("/Script/EnhancedInput.InputAction'/Game/Input/Actions/IA_MoveBackward.IA_MoveBackward'");
	const FString IA_MoveLeftPath = TEXT("/Script/EnhancedInput.InputAction'/Game/Input/Actions/IA_MoveLeft.IA_MoveLeft'");
	const FString IA_MoveRightPath = TEXT("/Script/EnhancedInput.InputAction'/Game/Input/Actions/IA_MoveRight.IA_MoveRight'");
	const FString IA_SprintPath = TEXT("/Script/EnhancedInput.InputAction'/Game/Input/Actions/AI_Sprint.AI_Sprint'");
	const FString IA_SwitchWeaponPath = TEXT("/Script/EnhancedInput.InputAction'/Game/Input/Actions/IA_SwitchWeapon.IA_SwitchWeapon'");
	const FString IA_FirePath = TEXT("/Script/EnhancedInput.InputAction'/Game/Input/Actions/IA_Fire.IA_Fire'");
	const FString IA_ReloadPath = TEXT("/Script/EnhancedInput.InputAction'/Game/Input/Actions/IA_Reload.IA_Reload'");
	const FString IA_AimPath = TEXT("/Script/EnhancedInput.InputAction'/Game/Input/Actions/IA_Aim.IA_Aim'");
	const FString IA_ZoomInPath = TEXT("/Script/EnhancedInput.InputAction'/Game/Input/Actions/IA_ZoomIn.IA_ZoomIn'");
	const FString IA_ZoomOutPath = TEXT("/Script/EnhancedInput.InputAction'/Game/Input/Actions/IA_ZoomOut.IA_ZoomOut'");
	const FString IA_InteractPath = TEXT("/Script/EnhancedInput.InputAction'/Game/Input/Actions/AI_Interact.AI_Interact'");
};
