﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "Engine/DataTable.h"
#include "GameFramework/Character.h"
#include "Interfaces/IDamageable.h"
#include "MMEnemyBase.generated.h"

class UMMBaseHitHandlerComponent;
class AMMWeaponBase;
class AMMCharacter;
class UMMBaseHealthComponent;
class UMMCharacterAnimInstance;

enum class EMMMovementState : uint8;

UCLASS()
class MONSTER_MAYHEM_API AMMEnemyBase : public ACharacter, public IDamageable
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMMEnemyBase();

private:
	void InitWeapon();
	void TrackPlayer();
	void StartReloadCurrentWeapon(const FInputActionValue& InputActionValue);
	void PlayAnimation(TWeakObjectPtr<UAnimMontage> AnimMontage);
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	UFUNCTION()
	virtual void HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	FVector NormalImpulse, const FHitResult& Hit) override;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditDefaultsOnly)
	TArray<UAnimMontage*> Deaths;
	TWeakObjectPtr<UMMCharacterAnimInstance> CharacterAnimInstancePtr;
	TWeakObjectPtr<AMMCharacter> Character;
	UPROPERTY(EditAnywhere)
	UMMBaseHealthComponent* HealthComponent;
	UPROPERTY(VisibleAnywhere)
	UMMBaseHitHandlerComponent* HitHandlerComponent;
	UPROPERTY(EditAnywhere, Category = Weapon)
	FDataTableRowHandle WeaponID;
	UPROPERTY(EditAnywhere, Category = Weapon)
	TSoftObjectPtr<AMMWeaponBase> Weapon;
	const FName WeaponSocket = TEXT("hand_rSocket");
	EMMMovementState CurrentMovementState;
	UPROPERTY(VisibleAnywhere)
	bool bIsAlive;
	FTimerHandle DeathTimerHandle;
};
