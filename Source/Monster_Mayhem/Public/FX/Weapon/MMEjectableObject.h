// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Helper/DelegateRegistry.h"
#include "MMEjectableObject.generated.h"

class UStaticMeshComponent;

UCLASS()
class MONSTER_MAYHEM_API AMMEjectableObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMMEjectableObject();
	
private:
	void SetDeactivationTimer();
	void DeactivationTimer();

public:
	void Launch();
	void SetActive(const bool& IsActive);

	FORCEINLINE void SetLaunchDirection(const FVector& _LaunchDirection) { LaunchDirection = _LaunchDirection; }
private:
	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* MeshComponent;
	UPROPERTY(EditDefaultsOnly)
	float MinInitialVelocity;
	UPROPERTY(EditDefaultsOnly)
	float MaxInitialVelocity;
	UPROPERTY(EditDefaultsOnly)
	float LifeSpan;
	FVector LaunchDirection;

	FTimerHandle DeactivationTimerHandle;
	
public:
	FOnExecutePoolObjectAction OnDeactivateObject;
};
