// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/WidgetAnimationEvents.h"
#include "GameFramework/Actor.h"
#include "MMDamageVisual.generated.h"

class UWidgetComponent;
class UWMMDamageVisual;

UCLASS()
class MONSTER_MAYHEM_API AMMDamageVisual : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMMDamageVisual();

private:
	void InitWidget();
	UFUNCTION()
	void Deactivate();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:
	void ShowDamage(const float& DamageNumber);
	bool GetIsActive() const { return bIsActive; }
private:
	bool bIsActive;
	UPROPERTY(EditDefaultsOnly)
	UWidgetComponent* DamageWidgetComponent;
	UPROPERTY()
	TWeakObjectPtr<UWMMDamageVisual> DamageVisualWidget;
	FWidgetAnimationDynamicEvent OnAnimationEnded;
	const FString DamageVisualWidgetPath = TEXT("/Game/BPs/UI/Visuals/BP_UW_Damage.BP_UW_Damage_C");
};
