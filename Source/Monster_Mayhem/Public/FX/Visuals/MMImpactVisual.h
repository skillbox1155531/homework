// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MMImpactVisual.generated.h"

class UNiagaraSystem;
class UNiagaraComponent;

UCLASS()
class MONSTER_MAYHEM_API AMMImpactVisual : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMMImpactVisual();

private:
	UFUNCTION()
	void Deactivate(UNiagaraComponent* Impact);

public:
	void Initialization(UNiagaraSystem* Impact);
	void ShowImpact();
	FORCEINLINE bool GetIsActive() const { return bIsActive; }
	
private:
	bool bIsActive;
	UPROPERTY(EditDefaultsOnly)
	UNiagaraComponent* NiagaraComponent;
};
