// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "MMGameMode.generated.h"

class AMMItemSpawner;

UCLASS()
class MONSTER_MAYHEM_API AMMGameMode : public AGameMode
{
	GENERATED_BODY()

private:
	void CreateItemSpawner();
	
protected:
	virtual void BeginPlay() override;
public:
	TWeakObjectPtr<AMMItemSpawner> GetItemSpawner() const;
private:
	TWeakObjectPtr<AMMItemSpawner> ItemSpawner;
};
