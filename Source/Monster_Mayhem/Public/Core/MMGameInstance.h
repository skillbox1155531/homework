// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "MMGameInstance.generated.h"

class UNiagaraSystem;
enum class EMMObjectMaterialType : uint8;

UCLASS()
class MONSTER_MAYHEM_API UMMGameInstance : public UGameInstance
{
	GENERATED_BODY()

private:
	virtual void Init() override;

public:
	FORCEINLINE TSoftObjectPtr<UDataTable> GetWeaponsDataBase() const { return TSoftObjectPtr<UDataTable>(WeaponsDataBase); }
	FORCEINLINE TSoftObjectPtr<UDataTable> GetAmmoDataBase() const { return TSoftObjectPtr<UDataTable>(AmmoDataBase); }
	FORCEINLINE TSoftObjectPtr<UDataTable> GetConsumablesDataBase() const { return TSoftObjectPtr<UDataTable>(ConsumablesDataBase); }

	TSoftObjectPtr<UNiagaraSystem> GetImpact(const EMMObjectMaterialType& MaterialType) const;

private:
	UPROPERTY()
	UDataTable* WeaponsDataBase;
	const FString WeaponsDataBasePath = TEXT("/Game/DataTables/DT_DB_Weapons.DT_DB_Weapons");
	UPROPERTY()
	UDataTable* AmmoDataBase;
	const FString AmmoDataBasePath = TEXT("/Game/DataTables/DT_DB_Ammo.DT_DB_Ammo");
	UPROPERTY()
	UDataTable* ImpactsDataBase;
	const FString ImpactsDataBasePath = TEXT("/Game/DataTables/DT_DB_Impacts.DT_DB_Impacts");
	UPROPERTY()
	UDataTable* ConsumablesDataBase;
	const FString ConsumablesDataBasePath = TEXT("/Game/DataTables/DT_DB_Consumables.DT_DB_Consumables");
};