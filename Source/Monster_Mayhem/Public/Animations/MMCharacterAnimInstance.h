// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "Enums/MMMovementState.h"
#include "Structs/Weapons/MMWeaponInfo.h"
#include "MMCharacterAnimInstance.generated.h"

class UAnimMontage;

UCLASS()
class MONSTER_MAYHEM_API UMMCharacterAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

private:
public:
	void SetAnimParams(const bool& _bIsAlive, const FVector& Velocity, const FRotator& Rotation, const EMMMovementState& State, const EMMWeaponType& _CurrentWeaponType);
	void SetVelocityParam(const FVector& Velocity);
	void SetMovementDirParam(const FVector& Velocity, const FRotator& Rotation);
	void SetMovementStateParam(const EMMMovementState& State);
	void SetWeaponTypeParam(const EMMWeaponType& _CurrentWeaponType);
	void SetIsAliveParam(const bool& _bIsAlive);
	void PlayMontage(TWeakObjectPtr<UAnimMontage> AnimMontage);
	void StopMontage(const UAnimMontage* Montage);
	void StopAllMontages();
protected:
	UPROPERTY(EditDefaultsOnly, Category = Montages)
	UAnimMontage* HipFireMontage;
	UPROPERTY(EditDefaultsOnly, Category = Montages)
	UAnimMontage* AimFireMontage;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
	bool bIsAlive;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
	float MovementSpeed;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
	float MovementDirection;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
	EMMMovementState MovementState;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category= Weapon)
	EMMWeaponType CurrenWeaponType;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Animation)
	float BlendWeight;
};
