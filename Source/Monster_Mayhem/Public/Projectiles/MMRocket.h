// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectiles/MMProjectileBase.h"
#include "MMRocket.generated.h"

/**
 * 
 */
UCLASS()
class MONSTER_MAYHEM_API AMMRocket : public AMMProjectileBase
{
	GENERATED_BODY()

public:
	AMMRocket();
	
private:
	virtual void HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	void OnExplode();
	virtual void TrackProjectile() override;
	virtual void SetActive(const bool& bIsActive) override;
protected:
	virtual void BeginPlay() override;
	
public:
private:
	bool bIsHit;
	float DeactivationDelay;
	FTimerHandle DeactivationTimerHandle;
};
