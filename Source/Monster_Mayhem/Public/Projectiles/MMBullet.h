// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectiles/MMProjectileBase.h"
#include "MMBullet.generated.h"

/**
 * 
 */
UCLASS()
class MONSTER_MAYHEM_API AMMBullet : public AMMProjectileBase
{
	GENERATED_BODY()

private:
	virtual void HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	virtual void TrackProjectile() override;
	virtual void SetActive(const bool& bIsActive) override;
};
