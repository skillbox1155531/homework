// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NiagaraSystem.h"
#include "GameFramework/Actor.h"
#include "Interfaces/IProjectile.h"
#include "Structs/Weapons/MMWeaponInfo.h"
#include "Helper/DelegateRegistry.h"
#include "MMProjectileBase.generated.h"

class URadialFalloff;
class UFieldSystemComponent;
class USceneComponent;
class UCapsuleComponent;
class UStaticMeshComponent;
class UNiagaraComponent;
class UAudioComponent;

UCLASS()
class MONSTER_MAYHEM_API AMMProjectileBase : public AActor, public IProjectile
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMMProjectileBase();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void BeginDestroy() override;
	UFUNCTION()
	virtual void HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override ;
	
public:
	void Launch(const FRotator& Direction);
	virtual void TrackProjectile() override;
	virtual void SetActive(const bool& _bIsActive) override;
	
	//Getters
	FORCEINLINE UCapsuleComponent* GetCapsuleComponent() const { return CapsuleComponent; }
	FORCEINLINE UStaticMeshComponent* GetStaticMeshComponent() const { return StaticMesh; }
	FORCEINLINE FMMProjectileBaseParams& GetBaseParameters() { return BaseParameters; }

	//Setters
	FORCEINLINE void SetBaseParameters(const FMMProjectileBaseParams& _BaseParameters) { BaseParameters = _BaseParameters; }
	FORCEINLINE void SetEventInstigator(AController* Controller) { EventInstigator = Controller; }
	
protected:
	UPROPERTY(VisibleAnywhere)
	USceneComponent* Root;
	UPROPERTY(VisibleAnywhere)
	UCapsuleComponent* CapsuleComponent;
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* StaticMesh;
	UPROPERTY(EditDefaultsOnly, Category= FX)
	UNiagaraComponent* Trail;
	UPROPERTY(EditDefaultsOnly, Category= FX)
	UNiagaraComponent* Explosion;
	UPROPERTY(EditDefaultsOnly, Category= FX)
	UAudioComponent* AudioComponent;
	UPROPERTY()
	AController* EventInstigator;
	UPROPERTY(EditAnywhere, Category= Parameters)
	FMMProjectileBaseParams BaseParameters;
	FVector SpawnLocation;

	//Timers
	FTimerHandle ProjectileTrackerTimerHandle;

public:
	//Delegates
	FOnExecutePoolProjectileAction OnDeactivateProjectile;
};
