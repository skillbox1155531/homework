// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Environment/MMItemSpawnPoint.h"
#include "Items/MMItem.h"
#include "MMItemSpawner.generated.h"

struct FMMConsumablesDataBase;
class UMMItem;
class UDataTable;
class AMMItemSpawnPoint;

enum class EMMItemType : uint8;
struct FMMWeaponsDataBase;
struct FMMAmmoDataBase;
struct FMMTrackingWeaponInfo;

UCLASS()
class MONSTER_MAYHEM_API AMMItemSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	AMMItemSpawner();
	
private:
	TWeakObjectPtr<AMMItemSpawnPoint> GetSpawnPoint(const EMMSpawnPointType& Type);
	bool SpawnItem(const TWeakObjectPtr<AMMItemSpawnPoint>& SpawnPoint, const TWeakObjectPtr<UMMItem>& Item);

	template<typename T> 
	TWeakObjectPtr<UMMItem> CreateItem(EMMItemType ItemType, FMMItemInfo ItemInfo, T ItemParams)
	{
		const TWeakObjectPtr<UMMItem> Item = NewObject<UMMItem>();
		
		if (Item.IsValid())
		{
			Item.Get() -> Initialization(GenerateItemID(), ItemType, ItemInfo, ItemParams);
			
			return Item;
		}

		return TWeakObjectPtr<UMMItem>(nullptr);
	}
	
	int32 GenerateItemID();
	
public:
	void InitializeSpawnPoints();
	
	template<typename T>
	bool SpawnItemToPoint(const T* ItemToSpawn)
	{
		if (ItemToSpawn)
		{
			TWeakObjectPtr<AMMItemSpawnPoint> SpawnPoint;
			TWeakObjectPtr<UMMItem> Item;
			
			if constexpr (std::is_same_v<T*, FMMWeaponsDataBase*>)
			{
				SpawnPoint = GetSpawnPoint(EMMSpawnPointType::SPT_Weapon);
				Item = CreateItem(ItemToSpawn -> GetType(), ItemToSpawn -> GetItemInfo(), ItemToSpawn -> GetWeaponInfo());

				return SpawnItem(SpawnPoint, Item);
			}
			else if constexpr (std::is_same_v<T*, FMMAmmoDataBase*>)
			{
				SpawnPoint = GetSpawnPoint(EMMSpawnPointType::SPT_Ammo);
				Item = CreateItem(ItemToSpawn -> GetType(), ItemToSpawn -> GetItemInfo(), ItemToSpawn -> GetAmmoInfo());
				
				return SpawnItem(SpawnPoint, Item);
			}
			else if constexpr (std::is_same_v<T*, FMMConsumablesDataBase*>)
			{
				SpawnPoint = GetSpawnPoint(EMMSpawnPointType::SPT_Health);
				Item = CreateItem(ItemToSpawn -> GetType(), ItemToSpawn -> GetItemInfo(), ItemToSpawn -> GetConsumableInfo());

				return SpawnItem(SpawnPoint, Item);
			}
		}

		return false;
	}

	template<typename T>
	TWeakObjectPtr<UMMItem> SpawnItemToRequester(T* ItemToSpawn)
	{
		if constexpr (std::is_same_v<T*, FMMWeaponsDataBase*>)
		{
			return CreateItem(ItemToSpawn -> GetType(), ItemToSpawn -> GetItemInfo(), ItemToSpawn -> GetWeaponInfo());
		}
		else if constexpr (std::is_same_v<T*, FMMAmmoDataBase*>)
		{
			return CreateItem(ItemToSpawn -> GetType(), ItemToSpawn -> GetItemInfo(), ItemToSpawn -> GetAmmoInfo());
		}

		return 	TWeakObjectPtr<UMMItem>(nullptr);
	}
	
private:
	UPROPERTY(VisibleAnywhere)
	TArray<TWeakObjectPtr<AMMItemSpawnPoint>> SpawnPointsArr;
	int32 NextItemID;
};

