// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/AssetManager.h"
#include "Engine/StreamableManager.h"

class MONSTER_MAYHEM_API FMMAsyncLoader
{
public:
	FMMAsyncLoader();
	~FMMAsyncLoader();

	template<typename T>
	static void LoadAsset(TSoftObjectPtr<T> ObjectPtr, const FStreamableDelegate& Delegate)
	{
		const FSoftObjectPath& AssetRef = ObjectPtr.ToSoftObjectPath();
		UAssetManager::GetStreamableManager().RequestAsyncLoad(AssetRef, Delegate);
	}
	
	template<typename T>
	static void UnloadAsset(TSoftObjectPtr<T> ObjectPtr)
	{
		const FSoftObjectPath& AssetRef = ObjectPtr.ToSoftObjectPath();
		UAssetManager::GetStreamableManager().Unload(AssetRef);
	}
};


