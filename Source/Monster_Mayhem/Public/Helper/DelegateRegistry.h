﻿#pragma once

class AMMProjectileBase;
class AMMEjectableObject;
class UWMMButton;
class AMMItemSpawnPoint;

struct FInputActionValue;
struct FMMWeaponTrackingInfo;
enum class EMMMovementState : uint8;
enum class EMMWeaponType : uint8;
enum class EMMWeaponSlotType : uint8;
enum class EMMConsumableType: uint8;

//Character Actions
DECLARE_MULTICAST_DELEGATE_OneParam(FOnExecuteMovement, const FInputActionValue&);
DECLARE_DELEGATE_OneParam(FOnSetMovementState, const EMMMovementState&)
DECLARE_MULTICAST_DELEGATE_OneParam(FOnRequestConsumableSpawn, const EMMConsumableType&);
//Camera Actions
DECLARE_DELEGATE_TwoParams(FOnExecuteCameraZoom, const bool&, const FInputActionValue&);
DECLARE_DELEGATE_TwoParams(FOnExecuteCameraPosition, const FVector&, const float&);
//Weapon Actions
DECLARE_MULTICAST_DELEGATE_OneParam(FOnExecuteWeaponAction, const FInputActionValue&);
DECLARE_DELEGATE_TwoParams(FOnUpdateWeaponTrackingInfo, const EMMWeaponType&, const FMMWeaponTrackingInfo&);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnRequestWeaponAmmoSpawn, const EMMWeaponSlotType&);
//Pool Actions
DECLARE_MULTICAST_DELEGATE_OneParam(FOnExecutePoolProjectileAction, TWeakObjectPtr<AMMProjectileBase>);
DECLARE_MULTICAST_DELEGATE(FOnExecutePoolObjectAction);
//UI Action
DECLARE_MULTICAST_DELEGATE_OneParam(FOnButtonClicked, UWMMButton*);
//PickupAction
DECLARE_MULTICAST_DELEGATE_OneParam(FOnRespawnItem, AMMItemSpawnPoint*)