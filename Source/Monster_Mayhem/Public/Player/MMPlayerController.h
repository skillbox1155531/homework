// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Helper/DelegateRegistry.h"
#include "Structs/Character/MMCharacterStats.h"
#include "MMPlayerController.generated.h"

class UMMConsumableRequestHandlerComponent;
class AMMCharacter;
class UMMCharacterAnimInstance;
class UMMInputHandlerComponent;
class UMMMovementHandlerComponent;
class UMMCameraHandlerComponent;
class UMMAchievementsTrackerComponent;
class UMMAmmoRequestHandlerComponent;
class USpringArmComponent;

enum class EMMMovementState : uint8;

UCLASS()
class MONSTER_MAYHEM_API AMMPlayerController : public APlayerController
{
	GENERATED_BODY()

	AMMPlayerController();
	
private:
	void SetGameInputMode();
	void TrackCursorPosition(const float& DeltaSeconds);
	void TrackCursorDistance(const float& DeltaSeconds);
	void UpdateCursorDecalPosition();
	void TrackCharacterDirectionForSprint();
	void UpdateAnimInstanceParams();
	void SetIsAim(const FInputActionValue& InputActionValue);
	void TrackCharacterSprint();
	void TrackCharacterIdle();
	void RestoreCharacterStamina();
	void CallCameraAimZoom(const float& DeltaSeconds);
	void CallSetCameraPosition(const float& FromDistance, const float& ToDistance, const FVector& TargetPosition, const float& DeltaSeconds);
	
protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	
public:
	void ReduceCharacterStamina();

	FORCEINLINE TWeakObjectPtr<AMMCharacter> GetCharacterPtr() const { return CharacterPtr; }
	FORCEINLINE bool GetCanSprint() const { return bCanSprint; }
	FORCEINLINE FMMCharacterStatParams* GetCharacterStamina() { return CharacterStats.GetStamina(); }

	FORCEINLINE void SetIsSprinting(const bool& _bIsSprinting) { bIsSprinting = _bIsSprinting; }
	FORCEINLINE void SetIsAiming(const bool& _bIsAiming) { bIsAiming = _bIsAiming; }

private:
	UPROPERTY()
	TWeakObjectPtr<AMMCharacter> CharacterPtr;
	UPROPERTY()
	TWeakObjectPtr<UMMCharacterAnimInstance> CharacterAnimInstancePtr;
	UPROPERTY(VisibleAnywhere)
	UMMInputHandlerComponent* InputHandlerComponent;
	UPROPERTY(VisibleAnywhere)
	UMMMovementHandlerComponent* MovementHandlerComponent;
	UPROPERTY(VisibleAnywhere)
	UMMCameraHandlerComponent* CameraHandlerComponent;
	UPROPERTY(VisibleAnywhere)
	UMMAchievementsTrackerComponent* AchievementsTrackerComponent;
	UPROPERTY(VisibleAnywhere)
	UMMAmmoRequestHandlerComponent* AmmoRequestHandlerComponent;
	UPROPERTY(VisibleAnywhere)
	UMMConsumableRequestHandlerComponent* ConsumableRequestHandlerComponent;
	
	const FString InventoryUIClassPath = TEXT("/Game/BPs/UI/Inventory/BP_UW_Inventory.BP_UW_Inventory_C");
	const FString CurrentWeaponUIClassPath = TEXT("/Game/BPs/UI/HUD/BP_WP_HUD_CurrentWeapon.BP_WP_HUD_CurrentWeapon_C");
	const FString AchievementBlockClassPath = TEXT("/Script/UMGEditor.WidgetBlueprint'/Game/BPs/UI/Achivements/BP_W_WeaponProgress.BP_W_WeaponProgress_C'");
	const FString HealthBarUIClassPath = TEXT("/Game/BPs/UI/HUD/BP_UW_HUD_Health.BP_UW_HUD_Health_C");
	const FString ShieldBarUIClassPath = TEXT("/Game/BPs/UI/HUD/BP_UW_HUD_Shield.BP_UW_HUD_Shield_C");
	
	UPROPERTY(VisibleAnywhere, Category= "Status | Movement")
	bool bCanSprint;
	UPROPERTY(VisibleAnywhere, Category= "Status | Movement")
	bool bIsSprinting;
	UPROPERTY(VisibleAnywhere, Category= "Status | Movement")
	bool bIsAiming;
	UPROPERTY(VisibleAnywhere, Category= "Status | Movement")
	bool bIsAimZoomIn;

	UPROPERTY(EditAnywhere, Category= "Camera | Tracking Settings")
	float DistanceThresholdMultiplayer;
	UPROPERTY(EditAnywhere, Category= "Camera | Tracking Settings")
	float CameraDirectionMultiplayer;
	UPROPERTY(EditAnywhere, Category= "Camera | Tracking Settings")
	float CameraDistanceTolerance;
	UPROPERTY(EditAnywhere, Category= "Camera | Tracking Settings")
	float CameraZeroDistance;
	
	UPROPERTY(EditAnywhere, Category = "Character | Stats")
	FMMCharacterStats CharacterStats;
	FTimerHandle StaminaRestoreTimerHandle;
	float StaminaReduceValue;
	float StaminaRestoreValue;
	float StaminaRestoreRate;
	float StaminaRestoreDelay;
	UPROPERTY(EditAnywhere, Category= "Weapon | Ammo")
	float SecondaryAmmoThreshold;
	UPROPERTY(EditAnywhere, Category= "Weapon | Ammo")
	float HeavyAmmoThreshold;

public:
	FOnExecuteMovement OnMoveCharacterForward;
	FOnExecuteMovement OnMoveCharacterRight;
	FOnExecuteMovement OnCharacterSprint;
	FOnExecuteMovement OnCharacterAim;
	FOnExecuteMovement OnCharacterInteract;
	FOnSetMovementState OnSetMovementState;
	FOnExecuteCameraZoom OnCameraViewZoom;
	FOnExecuteCameraZoom OnCameraAimZoom;
	FOnExecuteCameraPosition OnCameraSetPosition;
	FOnExecuteWeaponAction OnExecuteSwitchWeapon;
	FOnExecuteWeaponAction OnExecuteWeaponFire;
	FOnExecuteWeaponAction OnExecuteWeaponReload;
};
