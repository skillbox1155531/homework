// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Structs/Weapons/MMWeaponInfo.h"
#include "GameFramework/Character.h"
#include "Interfaces/IDamageable.h"
#include "Items/MMItem.h"
#include "Structs/Character/MMCharacterMovementParams.h"
#include "Weapons/MMWeaponBase.h"
#include "MMCharacter.generated.h"

class UMMBaseHitHandlerComponent;
class UMMWeaponsHandlerComponent;
class UWMMCurrentWeapon;
class UWMMBar;
class UMMCharacterHealthComponent;
class IInteractable;
class UWMMInventory;
class UMMInventoryComponent;
class UBoxComponent;
class AMMPickupActor;
class USphereComponent;
class AMMPlayerController;
class UMMCharacterAnimInstance;
class AMMWeaponBase;
class USpringArmComponent;
class UCameraComponent;
class UCapsuleComponent;
class UCharacterMovementComponent;
class UDataTable;

struct FInputActionValue;

enum class EMMMovementState : uint8;

UCLASS()
class MONSTER_MAYHEM_API AMMCharacter : public ACharacter, public IDamageable
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMMCharacter();

private:
	void ExecuteWeaponSwitch(const FInputActionValue& InputActionValue);
	void CallExecuteFireOnCurrentWeapon(const FInputActionValue& InputActionValue);
	void CallExecuteReloadOnCurrentWeapon(const FInputActionValue& InputActionValue);
	void ExecuteInteract(const FInputActionValue& InputActionValue);
	void SetMovementState(const EMMMovementState& MovementState);

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
	UFUNCTION()
	void HandleStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void HandleEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UFUNCTION()
	void HandleStartInteract(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void HandleEndInteract(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UFUNCTION()
	virtual void HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			FVector NormalImpulse, const FHitResult& Hit) override;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	void Initialization(const TWeakObjectPtr<AMMPlayerController>& _PlayerController, const TWeakObjectPtr<UWMMInventory>& _InventoryUIPtr,
		const TWeakObjectPtr<UWMMCurrentWeapon>& _CurrentWeaponUIPtr, const TWeakObjectPtr<UWMMBar>& _HealthBarUIPtr, const TWeakObjectPtr<UWMMBar>& _ShieldBarUIPtr);
	void ExecuteSetWeapon(const TWeakObjectPtr<UMMItemBase> Item, const FMMWeaponInfo& _WeaponInfo, const int8& Index);
	void ExecuteSetCurrentWeaponUI(UTexture2D* Image, const int8& AmmoInClip, const int32& AmmoInReserve);
	void UpdateWeaponInfoInInventory(const int32& _Id, const FMMWeaponBaseParams& BaseParams, const FMMWeaponTrackingInfo& TrackingInfo) const;
	EMMWeaponType GetCurrentWeaponType() const;
	
	//Getters
	FORCEINLINE USpringArmComponent* GetSpringArm() const { return CameraBoom; }
	FORCEINLINE UCameraComponent* GetCamera() const { return CameraComponent; }
	FORCEINLINE UDecalComponent* GetMouseCursorDecal() const { return DecalComponent; }
	FORCEINLINE UMMCharacterAnimInstance* GetCharacterAnimInstance() const { return CharacterAnimInstancePtr; }
	FORCEINLINE EMMMovementState GetCurrentMovementState() const { return CurrentMovementState; }
	FORCEINLINE TWeakObjectPtr<USkeletalMeshComponent> GetShellMesh() const { return TWeakObjectPtr<USkeletalMeshComponent>(ShellMeshComponent); }
	TWeakObjectPtr<AMMWeaponBase> GetSecondaryWeapon() const;
	TWeakObjectPtr<AMMWeaponBase> GetHeavyWeapon() const;
	//Setters
	FORCEINLINE void SetCurrentMovementState(const EMMMovementState& _CurrentMovementState) { SetMovementState(_CurrentMovementState); }
	

private:
	UPROPERTY()
	TWeakObjectPtr<AMMPlayerController> PlayerControllerPtr;
	UPROPERTY()
	UMMCharacterAnimInstance* CharacterAnimInstancePtr;
	UPROPERTY(EditAnywhere)
	USkeletalMeshComponent* ShellMeshComponent;
	UPROPERTY(VisibleAnywhere)
	USpringArmComponent* CameraBoom;
	UPROPERTY(VisibleAnywhere)
	UCameraComponent* CameraComponent;
	UPROPERTY(VisibleAnywhere)
	UDecalComponent* DecalComponent;
	UPROPERTY(VisibleAnywhere)
	USphereComponent* SphereComponent;
	UPROPERTY(VisibleAnywhere)
	UBoxComponent* BoxComponent;
	UPROPERTY(EditAnywhere)
	UMMCharacterHealthComponent* HealthComponent;
	UPROPERTY(EditAnywhere)
	UMMInventoryComponent* InventoryComponent;
	UPROPERTY(EditAnywhere)
	UMMWeaponsHandlerComponent* WeaponsHandlerComponent;
	UPROPERTY(VisibleAnywhere)
	UMMBaseHitHandlerComponent* HitHandlerComponent;
	UPROPERTY(VisibleAnywhere)
	UMMBaseHitHandlerComponent* ShieldHitHandlerComponent;
	
	UPROPERTY(EditDefaultsOnly, Category= "Character | Movement")
	FMMCharacterMovementParams MovementParams;
	UPROPERTY(VisibleAnywhere, Category= "Character | Movement")
	EMMMovementState CurrentMovementState;

	UPROPERTY(VisibleAnywhere)
	TMap<AActor*, bool> PickupsInRange;
	IInteractable* CurrentPickup;

	UPROPERTY(EditAnywhere, Category= "Character | Parameters | Weapon")
	FDataTableRowHandle DefaultWeaponID;
	const FName WeaponSocket = TEXT("hand_rSocket");

public:
	FOnUpdateWeaponTrackingInfo OnUpdateWeaponTrackingInfo;
	FOnRequestWeaponAmmoSpawn OnRequestWeaponAmmoSpawn;
	FOnRequestConsumableSpawn OnRequestConsumableSpawn;
};