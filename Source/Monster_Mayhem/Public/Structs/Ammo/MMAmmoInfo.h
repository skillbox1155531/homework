﻿#pragma once

#include "CoreMinimal.h"
#include "Structs/Weapons/MMWeaponInfo.h"
#include "MMAmmoInfo.generated.h"

USTRUCT()
struct FMMAmmoBaseParams
{
	GENERATED_BODY();

public:
	FMMAmmoBaseParams() : Type(EMMWeaponSlotType::WST_None) {}
	
	FORCEINLINE EMMWeaponSlotType GetAmmoType() const { return Type; }

private:
	UPROPERTY(EditAnywhere, Category= Parameters)
	EMMWeaponSlotType Type;
};

USTRUCT()
struct FMMAmmoInfo
{
	GENERATED_BODY();

public:
	FMMAmmoInfo() : BaseParameters(FMMAmmoBaseParams()) {}
	
	//Getters
	FORCEINLINE FMMAmmoBaseParams GetBaseParameters() const { return BaseParameters; }
private:
	UPROPERTY(EditAnywhere, Category= Info)
	FMMAmmoBaseParams BaseParameters;
};