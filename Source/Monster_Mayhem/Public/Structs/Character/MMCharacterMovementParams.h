﻿#pragma once

#include "MMCharacterMovementParams.generated.h"

USTRUCT()
struct FMMCharacterMovementParams
{
	GENERATED_BODY()

	FMMCharacterMovementParams() : WalkSpeed(400.f), SprintSpeed(800.f), AimSpeed(250.f) {}
	
public:
	FORCEINLINE float GetWalkSpeed() const { return WalkSpeed; }
	FORCEINLINE float GetSprintSpeed() const { return SprintSpeed; }
	FORCEINLINE float GetAimSpeed() const { return AimSpeed; }
	
private:
	UPROPERTY(EditDefaultsOnly, Category = Parameters)
	float WalkSpeed;
	UPROPERTY(EditDefaultsOnly, Category = Parameters)
	float SprintSpeed;
	UPROPERTY(EditDefaultsOnly, Category = Parameters)
	float AimSpeed;
};
