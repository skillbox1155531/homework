﻿#pragma once

#include "MMCharacterStatParams.h"
#include "MMCharacterStats.generated.h"

USTRUCT()
struct FMMCharacterStats
{
	GENERATED_BODY()

	FMMCharacterStats() : Health(FMMCharacterStatParams()), Stamina(FMMCharacterStatParams()) {}
	
public:
	FORCEINLINE FMMCharacterStatParams* GetHealth() { return &Health; }
	FORCEINLINE FMMCharacterStatParams* GetStamina() { return &Stamina; }
	
private:
	UPROPERTY(EditAnywhere, Category = Stat)
	FMMCharacterStatParams Health;
	UPROPERTY(EditAnywhere, Category = Stat)
	FMMCharacterStatParams Stamina;
};