﻿#pragma once

#include "MMCameraParams.generated.h"

USTRUCT()
struct FMMCameraParams
{
	GENERATED_BODY()

	FMMCameraParams() : MaxCameraHeight(1500.f), MinCameraHeight(1000.f), AimCameraHeight(750.f), CameraAimTolerance(10.f),
	CameraViewZoomStep(10.f), CameraViewInterpSpeed(55.f), CameraAimInterpSpeed(10.f), CameraPosInterpSpeed(500.f) {}
	
public:
	FORCEINLINE float GetMaxCameraHeight() const { return MaxCameraHeight; }
	FORCEINLINE float GetMinCameraHeight() const { return MinCameraHeight; }
	FORCEINLINE float GetAimCameraHeight() const { return AimCameraHeight; }
	FORCEINLINE float GetCameraAimTolerance() const { return CameraAimTolerance; }
	FORCEINLINE float GetCameraZoomStep() const { return CameraViewZoomStep; }
	FORCEINLINE float GetCameraViewInterpSpeed() const { return CameraViewInterpSpeed; }
	FORCEINLINE float GetCameraAimInterpSpeed() const { return CameraAimInterpSpeed; }
	FORCEINLINE float GetCameraPosInterpSpeed() const { return CameraPosInterpSpeed; }
	
private:
	UPROPERTY(EditDefaultsOnly, Category = Camera)
    float MaxCameraHeight;
    UPROPERTY(EditDefaultsOnly, Category = Camera)
    float MinCameraHeight;
    UPROPERTY(EditDefaultsOnly, Category = Camera)
    float AimCameraHeight;
	UPROPERTY(EditDefaultsOnly, Category = Camera)
	float CameraAimTolerance;
	UPROPERTY(EditDefaultsOnly, Category = Camera)
	float CameraViewZoomStep;
    UPROPERTY(EditDefaultsOnly, Category = Camera)
    float CameraViewInterpSpeed;
	UPROPERTY(EditDefaultsOnly, Category = Camera)
	float CameraAimInterpSpeed;
	UPROPERTY(EditDefaultsOnly, Category = Camera)
	float CameraPosInterpSpeed;
};