﻿#pragma once

#include "Enums/MMCharacterStatType.h"
#include "MMCharacterStatParams.generated.h"

enum class EMMCharacterStatType : uint8;

USTRUCT()
struct FMMCharacterStatParams
{
	GENERATED_BODY()

	FMMCharacterStatParams() : StatType(EMMCharacterStatType::CST_None), CurrentValue(0.f), MinValue(0.f), MaxValue(100.f), DefaultValue(0.f) {}
	
public:
	FORCEINLINE EMMCharacterStatType GetStatType() const { return StatType; }
	FORCEINLINE float GetCurrentValue() const { return CurrentValue; }
	FORCEINLINE float GetMinValue() const { return MinValue; }
	FORCEINLINE float GetMaxValue() const { return MaxValue; }
	FORCEINLINE float GetDefaultValue() const { return DefaultValue; }

	FORCEINLINE void SetCurrentValue(const float& _CurrentValue) { CurrentValue = _CurrentValue; }
	FORCEINLINE void SetMaxValue(const float& _MaxValue) { MaxValue = _MaxValue; }
	
private:
	UPROPERTY(EditAnywhere, Category = Parametrs)
	EMMCharacterStatType StatType;
	UPROPERTY(EditAnywhere, Category = Parametrs)
	float CurrentValue;
	UPROPERTY(EditAnywhere, Category = Parametrs)
	float MinValue;
	UPROPERTY(EditAnywhere, Category = Parametrs)
	float MaxValue;
	UPROPERTY(EditAnywhere, Category = Parametrs)
	float DefaultValue;
};