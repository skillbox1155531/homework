﻿#pragma once

#include "CoreMinimal.h"
#include "Enums/MMObjectMaterialType.h"
#include "MMImpactInfo.generated.h"

class UNiagaraSystem;

USTRUCT()
struct FMMImpactInfo
{
	GENERATED_BODY();

public:
	FMMImpactInfo() : Type(EMMObjectMaterialType::OMT_None), ImpactFX(nullptr)  {}
	
	//Getters
	FORCEINLINE EMMObjectMaterialType GetType() const { return Type; }
	FORCEINLINE TSoftObjectPtr<UNiagaraSystem> GetImpactFX() const { return ImpactFX; }
	
private:
	UPROPERTY(EditAnywhere, Category= Info)
	EMMObjectMaterialType Type;
	UPROPERTY(EditAnywhere, Category= Info)
	TSoftObjectPtr<UNiagaraSystem> ImpactFX;
};