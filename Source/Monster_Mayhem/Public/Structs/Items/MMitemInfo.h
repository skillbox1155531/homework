﻿#pragma once

#include "CoreMinimal.h"
#include "MMItemInfo.generated.h"

UENUM(BlueprintType)
enum class EMMItemAction : uint8
{
	IA_Equip UMETA(DisplayName = "Equip"),
	IA_Consume UMETA(DisplayName = "Consume"),
	IA_None UMETA(DisplayName = "None")
};

USTRUCT()
struct FMMItemInfo
{
	GENERATED_BODY();

public:
	FMMItemInfo() : Name(TEXT("")), Description(TEXT("")), StaticMesh(nullptr), Action(EMMItemAction::IA_None), InventoryThumbnail(nullptr), HUDThumbnail(nullptr),
	bIsStackable(false), Quantity(1) {}

	//Getters
	FORCEINLINE FString GetName() const { return Name; }
	FORCEINLINE FString GetDescription() const { return Description; }
	FORCEINLINE TSoftObjectPtr<UStaticMesh> GetStaticMesh() const { return StaticMesh; }
	FORCEINLINE EMMItemAction GetItemAction() const { return Action; }
	FORCEINLINE TSoftObjectPtr<UTexture2D> GetInventoryThumbnail() const { return InventoryThumbnail; }
	FORCEINLINE TSoftObjectPtr<UTexture2D> GetHUDThumbnail() const { return HUDThumbnail; }
	FORCEINLINE bool GetIsStackable() const { return bIsStackable; }
	FORCEINLINE int8 GetQuantity() const { return Quantity; }
	
private:
	UPROPERTY(EditAnywhere, Category= "General Info")
	FString Name;
	UPROPERTY(EditAnywhere, Category= "General Info", meta=(MultiLine = true))
	FString Description;
	UPROPERTY(EditAnywhere, Category= "Meshes")
	TSoftObjectPtr<UStaticMesh> StaticMesh;
	UPROPERTY(EditAnywhere, Category= "Inventory Info")
	EMMItemAction Action;
	UPROPERTY(EditAnywhere, Category= "Inventory Info")
	TSoftObjectPtr<UTexture2D> InventoryThumbnail;
	UPROPERTY(EditAnywhere, Category= "Inventory Info")
	TSoftObjectPtr<UTexture2D> HUDThumbnail;
	UPROPERTY(EditAnywhere, Category= "Inventory Info")
	bool bIsStackable;
	UPROPERTY(EditAnywhere, Category= "Inventory Info", meta=(EditCondition = "bIsStackable", EditConditionHides))
	int8 Quantity;
};