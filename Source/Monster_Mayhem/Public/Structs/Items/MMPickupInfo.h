﻿#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Enums/MMItemType.h"
#include "MMPickupInfo.generated.h"

// UENUM(BlueprintType)
// enum class EMMItemAction : uint8
// {
// 	IA_Equip UMETA(DisplayName = "Equip"),
// 	IA_Consume UMETA(DisplayName = "Consume"),
// 	IA_None UMETA(DisplayName = "None")
// };

USTRUCT()
struct FMMPickupBaseParams
{
	GENERATED_BODY();
	
//
// public:
// 	FMMPickupBaseParams() : Type(EMMItemType::IT_None), Mesh(nullptr), Action(EMMItemAction::IA_None), Thumbnail(nullptr),
// 	bIsStackable(false), Quantity(1) {}
//
// 	//Getters
// 	FORCEINLINE EMMItemType GetItemType() const { return Type; }
// 	FORCEINLINE UStaticMesh* GetMesh() const { return Mesh.LoadSynchronous(); }
// 	FORCEINLINE FDataTableRowHandle GetItemID() const { return ItemID; }
// 	FORCEINLINE EMMItemAction GetItemAction() const { return Action; }
// 	FORCEINLINE UTexture2D* GetItemThumbnail() const { return Thumbnail.LoadSynchronous(); }
// 	FORCEINLINE bool GetIsStackable() const { return bIsStackable; }
// 	FORCEINLINE int8 GetQuantity() const { return Quantity; }
// 	FORCEINLINE FString GetName() const { return Name; }
// 	FORCEINLINE FString GetDescription() const { return Description; }
// 	
// private:
// 	UPROPERTY(EditAnywhere, Category= Parameters)
// 	EMMItemType Type;
// 	UPROPERTY(EditAnywhere, Category= Parameters)
// 	TSoftObjectPtr<UStaticMesh> Mesh;
// 	UPROPERTY(EditAnywhere, Category= Parameters)
// 	FDataTableRowHandle ItemID;
// 	UPROPERTY(EditAnywhere, Category= Parameters)
// 	EMMItemAction Action;
// 	UPROPERTY(EditAnywhere, Category= Parameters)
// 	TSoftObjectPtr<UTexture2D> Thumbnail;
// 	UPROPERTY(EditAnywhere, Category= Parameters)
// 	bool bIsStackable;
// 	UPROPERTY(EditAnywhere, Category= Parameters, meta=(EditCondition = "bIsStackable", EditConditionHides))
// 	int8 Quantity;
// 	UPROPERTY(EditAnywhere, Category= Parameters)
// 	FString Name;
// 	UPROPERTY(EditAnywhere, Category= Parameters, meta=(MultiLine = true))
// 	FString Description;
// };
//
// USTRUCT()
// struct FMMPickupInfo
// {
// 	GENERATED_BODY();
//
// public:
// 	FMMPickupInfo() : BaseParameters(FMMPickupBaseParams()) {}
//
// 	//Getters
// 	FORCEINLINE FMMPickupBaseParams GetBaseParameters() const { return BaseParameters; }
// private:
// 	UPROPERTY(EditAnywhere, Category= Info)
// 	FMMPickupBaseParams BaseParameters;
};