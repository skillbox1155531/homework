﻿#pragma once

#include "CoreMinimal.h"
#include "MMConsumableInfo.generated.h"

UENUM(BlueprintType)
enum class EMMConsumableType : uint8
{
	CT_Health UMETA(DisplayName = "Health"),
	CT_Shield UMETA(DisplayName = "Shield"),
	CT_None UMETA(DisplayName = "None")
};

USTRUCT()
struct FMMConsumableBaseParams
{
	GENERATED_BODY();

public:
	FMMConsumableBaseParams() : Type(EMMConsumableType::CT_None), Amount(0) {};

	//Getters
	FORCEINLINE EMMConsumableType GetType() const { return Type; }
	FORCEINLINE int8 GetAmount() const { return Amount; }

private:
	UPROPERTY(EditAnywhere, Category= Parameters)
	EMMConsumableType Type;
	UPROPERTY(EditAnywhere, Category= Parameters)
	int8 Amount;
};

USTRUCT()
struct FMMConsumableInfo
{
	GENERATED_BODY();

public:
	FMMConsumableInfo() : BaseParameters(FMMConsumableBaseParams()) {}
	
	//Getters
	FORCEINLINE FMMConsumableBaseParams GetBaseParameters() const { return BaseParameters; }
private:
	UPROPERTY(EditAnywhere, Category= Info)
	FMMConsumableBaseParams BaseParameters;
};