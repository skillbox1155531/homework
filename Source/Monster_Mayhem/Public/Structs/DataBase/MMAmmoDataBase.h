﻿#pragma once

#include "CoreMinimal.h"
#include "Ammo/MMAmmoInfo.h"
#include "Engine/DataTable.h"
#include "Enums/MMItemType.h"
#include "Items/MMitemInfo.h"
#include "Weapons/MMWeaponInfo.h"
#include "MMAmmoDataBase.generated.h"

USTRUCT()
struct FMMAmmoDataBase : public FTableRowBase
{
	GENERATED_BODY()

public:
	FMMAmmoDataBase() : Type(EMMItemType::IT_Ammo), ItemInfo(FMMItemInfo()), AmmoInfo(FMMAmmoInfo()) {}

	FORCEINLINE EMMItemType GetType() const { return Type; }
	FORCEINLINE FMMItemInfo GetItemInfo() const { return ItemInfo; }
	FORCEINLINE FMMAmmoInfo GetAmmoInfo() const { return AmmoInfo; }
	
private:
	UPROPERTY(EditDefaultsOnly, Category= Info)
	EMMItemType Type;
	UPROPERTY(EditAnywhere, Category= Info)
	FMMItemInfo ItemInfo;
	UPROPERTY(EditAnywhere, Category= Info)
	FMMAmmoInfo AmmoInfo;
};