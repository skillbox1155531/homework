﻿#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Enums/MMItemType.h"
#include "Structs/Armor/MMArmorInfo.h"
#include "MMArmorDataBase.generated.h"

USTRUCT()
struct FMMArmorDataBase : public FTableRowBase
{
	GENERATED_BODY()

public:
	FMMArmorDataBase() : Type(EMMItemType::IT_Armor), ArmorInfo(FMMArmorInfo()) {}

	FORCEINLINE EMMItemType GetType() const { return Type; }
	FORCEINLINE FMMArmorInfo GetArmorInfo() const { return ArmorInfo; }
	
private:
	UPROPERTY(EditDefaultsOnly, Category= Info)
	EMMItemType Type;
	UPROPERTY(EditDefaultsOnly, Category= Info)
	FMMArmorInfo ArmorInfo;
};