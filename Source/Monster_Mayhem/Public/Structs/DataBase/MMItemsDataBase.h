#pragma once

#include "CoreMinimal.h"
#include "Armor/MMArmorInfo.h"
#include "Consumables/MMConsumableInfo.h"
#include "Items/MMPickupInfo.h"
#include "Weapons/MMWeaponInfo.h"
#include "Engine/DataTable.h"
#include "Enums/MMItemType.h"
#include "MMItemsDataBase.generated.h"

USTRUCT()
struct FMMItemsDataBase : public FTableRowBase
{
	GENERATED_BODY()

public:
	FMMItemsDataBase() : ItemType(EMMItemType::IT_None) {}

	FORCEINLINE EMMItemType GetItemType() const { return ItemType; }
	FORCEINLINE FMMWeaponInfo GetWeaponInfo() const { return WeaponInfo; }
	FORCEINLINE FMMArmorInfo GetArmorInfo() const { return ArmorInfo; }
	FORCEINLINE FMMConsumableInfo GetConsumableInfo() const { return ConsumableInfo; }
	//FORCEINLINE FMMPickupInfo GetPickupInfo() const { return PickupInfo; }
	
private:
	UPROPERTY(EditDefaultsOnly, Category= Info)
	EMMItemType ItemType;
	UPROPERTY(EditDefaultsOnly, Category= Info, meta=(EditCondition = "ItemType == EMMItemType::IT_Weapon", EditConditionHides))
	FMMWeaponInfo WeaponInfo;
	UPROPERTY(EditDefaultsOnly, Category= Info, meta=(EditCondition = "ItemType == EMMItemType::IT_Armor", EditConditionHides))
	FMMArmorInfo ArmorInfo;
	UPROPERTY(EditDefaultsOnly, Category= Info, meta=(EditCondition = "ItemType == EMMItemType::IT_Consumable", EditConditionHides))
	FMMConsumableInfo ConsumableInfo;
	// UPROPERTY(EditDefaultsOnly, Category= Info, meta=(EditCondition = "ItemType == EMMItemType::IT_Pickup", EditConditionHides))
	// FMMPickupInfo PickupInfo;
};