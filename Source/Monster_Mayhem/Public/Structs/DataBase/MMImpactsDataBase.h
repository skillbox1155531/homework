﻿#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Impacts/MMImpactInfo.h"
#include "MMImpactsDataBase.generated.h"

USTRUCT()
struct FMMImpactsDataBase : public FTableRowBase
{
	GENERATED_BODY()

public:
	FMMImpactsDataBase() : ImpactInfo(FMMImpactInfo()) {}

	FORCEINLINE FMMImpactInfo GetImpactInfo() const { return ImpactInfo; }
private:
	UPROPERTY(EditDefaultsOnly, Category= Info)
	FMMImpactInfo ImpactInfo;
};