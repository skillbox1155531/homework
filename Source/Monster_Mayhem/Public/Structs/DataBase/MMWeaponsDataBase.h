﻿#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Enums/MMItemType.h"
#include "Structs/Weapons/MMWeaponInfo.h"
#include "MMWeaponsDataBase.generated.h"

USTRUCT()
struct FMMWeaponsDataBase : public FTableRowBase
{
	GENERATED_BODY()

public:
	FMMWeaponsDataBase() : Type(EMMItemType::IT_Weapon), ItemInfo(FMMItemInfo()), WeaponInfo(FMMWeaponInfo()) {}
	
	FORCEINLINE EMMItemType GetType() const { return Type; }
	FORCEINLINE FMMItemInfo GetItemInfo() const { return ItemInfo; }
	FORCEINLINE FMMWeaponInfo GetWeaponInfo() const { return WeaponInfo; }
	
private:
	UPROPERTY(EditAnywhere, Category = Info)
	EMMItemType Type;
	UPROPERTY(EditAnywhere, Category= Info)
	FMMItemInfo ItemInfo;
	UPROPERTY(EditDefaultsOnly, Category= Info)
	FMMWeaponInfo WeaponInfo;
};