﻿#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Enums/MMItemType.h"
#include "Items/MMitemInfo.h"
#include "Structs/Consumables/MMConsumableInfo.h"
#include "MMConsumablesDataBase.generated.h"

USTRUCT()
struct FMMConsumablesDataBase : public FTableRowBase
{
	GENERATED_BODY()

public:
	FMMConsumablesDataBase() : Type(EMMItemType::IT_Consumable), ItemInfo(FMMItemInfo()), ConsumableInfo(FMMConsumableInfo()) {}

	FORCEINLINE EMMItemType GetType() const { return Type; }
	FORCEINLINE FMMItemInfo GetItemInfo() const { return ItemInfo; }
	FORCEINLINE FMMConsumableInfo GetConsumableInfo() const { return ConsumableInfo; }
	
private:
	UPROPERTY(EditDefaultsOnly, Category= Info)
	EMMItemType Type;
	UPROPERTY(EditAnywhere, Category= Info)
	FMMItemInfo ItemInfo;
	UPROPERTY(EditDefaultsOnly, Category= Info)
	FMMConsumableInfo ConsumableInfo;
};