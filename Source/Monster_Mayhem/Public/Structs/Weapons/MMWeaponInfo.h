﻿#pragma once

#include "CoreMinimal.h"
#include "Sound/SoundCue.h"
#include "NiagaraSystem.h"
#include "Items/MMitemInfo.h"
#include "MMWeaponInfo.generated.h"

class AMMEjectableObject;
class AMMWeaponBase;
class AMMProjectileBase;

UENUM(BlueprintType)
enum class EMMWeaponType : uint8
{
	WT_AutoRifle UMETA(DisplayName = "Auto Rifle"),
	WT_Pistol UMETA(DisplayName = "Pistol"),
	WT_RocketLauncher UMETA(DisplayName = "Rocket Launcher"),
	WT_Shotgun UMETA(DisplayName = "Shotgun"),
	WT_SniperRifle UMETA(DisplayName = "Sniper Rifle"),
	WT_None UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum class EMMWeaponFireType : uint8
{
	WFT_AutoFire UMETA(DisplayName = "Auto Fire"),
	WFT_SingleFire UMETA(DisplayName = "Single Fire"),
	WFT_SpreadFire UMETA(DisplayName = "Spread Fire"),
	WFT_None UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum class EMMWeaponSlotType : uint8
{
	WST_Default UMETA(DisplayName = "Default"),
	WST_Secondary UMETA(DisplayName = "Secondary"),
	WST_Heavy UMETA(DisplayName = "Heavy"),
	WST_None UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum class EMMProjectileType : uint8
{
	PT_Bullet UMETA(DisplayName = "Bullet"),
	PT_Grenade UMETA(DisplayName = "Grenade"),
	PT_Rocket UMETA(DisplayName = "Rocket"),
	WT_None UMETA(DisplayName = "None")
};

USTRUCT()
struct FMMWeaponComponentsParams
{
	GENERATED_BODY()
	
	FMMWeaponComponentsParams() : SkeletalMesh(nullptr), FireSpotTransform(FTransform::Identity), ClipEjectionTransform(FTransform::Identity), ShellEjectionSpotTransform(FTransform::Identity), MuzzleFlashTransform(FTransform::Identity) {}

	FORCEINLINE TSoftObjectPtr<USkeletalMesh> GetSkeletalMesh() const { return SkeletalMesh; }
	FORCEINLINE FTransform GetFireSpotTransform () const { return FireSpotTransform; }
	FORCEINLINE FTransform GetClipEjectionTransform () const { return ClipEjectionTransform; }
	FORCEINLINE FTransform GetShellEjectionSpotTransform () const { return ShellEjectionSpotTransform; }
	FORCEINLINE FTransform GetMuzzleFlashTransform () const { return MuzzleFlashTransform; }

private:
	UPROPERTY(EditAnywhere, Category= Info)
	TSoftObjectPtr<USkeletalMesh> SkeletalMesh;
	UPROPERTY(EditAnywhere, Category= Info)
	FTransform FireSpotTransform;
	UPROPERTY(EditAnywhere, Category= Info)
	FTransform ClipEjectionTransform;
	UPROPERTY(EditAnywhere, Category= Info)
	FTransform ShellEjectionSpotTransform;
	UPROPERTY(EditAnywhere, Category= Info)
	FTransform MuzzleFlashTransform;
};

USTRUCT()
struct FMMWeaponBaseParams
{
	GENERATED_BODY()

	FMMWeaponBaseParams() : Type(EMMWeaponType::WT_None), FireType(EMMWeaponFireType::WFT_None), MinDamage(0.f), MaxDamage(0.f), DamageFallOffAmount(0.f), RateOfFire(0.f), MaxRange(0.f), DamageFallOffRange(0.f), DamageInnerRadius(0.f), DamageOuterRadius(0.f), RoundsPerShot(0),
	RoundsInClip(0), MaxRoundsInClip(0), RoundsInReserve(0), MaxRoundsInReserve(0) {}

	//Getters
	FORCEINLINE EMMWeaponType GetWeaponType() const { return Type; }
	FORCEINLINE EMMWeaponFireType GetWeaponFireType() const { return FireType; }
	FORCEINLINE float GetMinDamage() const { return MinDamage; }
	FORCEINLINE float GetMaxDamage() const { return MaxDamage; }
	FORCEINLINE float GetDamageFallOffAmount() const { return DamageFallOffAmount; }
	FORCEINLINE float GetRateOfFire() const { return RateOfFire; }
	FORCEINLINE float GetMaxRange() const { return MaxRange; }
	FORCEINLINE float GetDamageInnerRadius() const { return DamageInnerRadius; }
	FORCEINLINE float GetDamageOuterRadius() const { return DamageOuterRadius; }
	FORCEINLINE float GetDamageFallOffRange() const { return DamageFallOffRange; }
	FORCEINLINE int8 GetRoundsPerShot() const { return RoundsPerShot; }
	FORCEINLINE int8 GetRoundsInClip() const { return RoundsInClip; }
	FORCEINLINE int8 GetMaxRoundsInClip() const { return MaxRoundsInClip; }
	FORCEINLINE int32 GetRoundsInReserve() const { return  RoundsInReserve; }
	FORCEINLINE int32 GetMaxRoundsInReserve() const { return  MaxRoundsInReserve; }
	//Setters
	FORCEINLINE void SetRoundsInClip(const int8& Amount) { RoundsInClip = Amount; }
	FORCEINLINE void SetRoundsInReserve(const int32& Amount) { RoundsInReserve = Amount; }
	
private:
	UPROPERTY(EditAnywhere, Category= Info)
	EMMWeaponType Type;
	UPROPERTY(EditAnywhere, Category= Info)
	EMMWeaponFireType FireType;
	UPROPERTY(EditAnywhere, Category= "Parameters | General")
	float MinDamage;
	UPROPERTY(EditAnywhere, Category= "Parameters | General")
	float MaxDamage;
	UPROPERTY(EditAnywhere, Category= "Parameters | General")
	float DamageFallOffAmount;
	UPROPERTY(EditAnywhere, Category= "Parameters | General")
	float RateOfFire;
	UPROPERTY(EditAnywhere, Category= "Parameters | General")
	float MaxRange;
	UPROPERTY(EditAnywhere, Category= "Parameters | General")
	float DamageFallOffRange;
	UPROPERTY(EditAnywhere, meta=(EditCondition = "Type == EMMWeaponType::WT_RocketLauncher", EditConditionHides), Category="Parameters | General")
	float DamageInnerRadius;
	UPROPERTY(EditAnywhere, meta=(EditCondition = "Type == EMMWeaponType::WT_RocketLauncher", EditConditionHides), Category="Parameters | General")
	float DamageOuterRadius;
	UPROPERTY(EditAnywhere, Category= "Parameters | General")
	int8 RoundsPerShot;
	UPROPERTY(EditAnywhere, Category= "Parameters | General")
	int8 RoundsInClip;
	UPROPERTY(EditAnywhere, Category= "Parameters | General")
	int8 MaxRoundsInClip;
	UPROPERTY(EditAnywhere, Category= "Parameters | General")
	int32 RoundsInReserve;
	UPROPERTY(EditAnywhere, Category= "Parameters | General")
	int32 MaxRoundsInReserve;
};

USTRUCT()
struct FMMWeaponDispersionParams
{
	GENERATED_BODY()
	
	FMMWeaponDispersionParams () : DispersionMin(0.f), DispersionMax(0.f), DispersionReduction(0.f), Recoil(0.f) {}

	FORCEINLINE float GetDispersionMin () const { return DispersionMin; }
	FORCEINLINE float GetDispersionMax () const { return DispersionMax; }
	FORCEINLINE float GetDispersionReduction () const { return DispersionReduction; }
	FORCEINLINE float GetRecoil () const { return Recoil; }

private:
	UPROPERTY(EditAnywhere, Category= Parameters)
	float DispersionMin;
	UPROPERTY(EditAnywhere, Category= Parameters)
	float DispersionMax;
	UPROPERTY(EditAnywhere, Category= Parameters)
	float DispersionReduction;
	UPROPERTY(EditAnywhere, Category= Parameters)
	float Recoil;
};

USTRUCT()
struct FMMWeaponDispersionData
{
	GENERATED_BODY()
	
	FMMWeaponDispersionData() : IdleDispersionParams(FMMWeaponDispersionParams()), AimDispersionParams(FMMWeaponDispersionParams()), WalkDispersionParams(FMMWeaponDispersionParams()) {}

	FORCEINLINE FMMWeaponDispersionParams GetIdleDispersionParams () const { return IdleDispersionParams; }
	FORCEINLINE FMMWeaponDispersionParams GetAimDispersionParams () const { return AimDispersionParams; }
	FORCEINLINE FMMWeaponDispersionParams GetWalkDispersionParams () const { return WalkDispersionParams; }

private:
	UPROPERTY(EditAnywhere, Category= Parameters)
	FMMWeaponDispersionParams IdleDispersionParams;
	UPROPERTY(EditAnywhere, Category= Parameters)
	FMMWeaponDispersionParams AimDispersionParams;
	UPROPERTY(EditAnywhere, Category= Parameters)
	FMMWeaponDispersionParams WalkDispersionParams;
};

USTRUCT()
struct FMMWeaponAnimations
{
	GENERATED_BODY()
	
	FMMWeaponAnimations () : BaseFire(nullptr), AimFire(nullptr), Reload(nullptr) {}

	FORCEINLINE TSoftObjectPtr<UAnimMontage> GetBaseFire () const { return BaseFire; }
	FORCEINLINE TSoftObjectPtr<UAnimMontage> GetAimFire () const { return AimFire; }
	FORCEINLINE TSoftObjectPtr<UAnimMontage> GetReload () const { return Reload; }

private:
	UPROPERTY(EditAnywhere, Category= Animations)
	TSoftObjectPtr<UAnimMontage> BaseFire;
	UPROPERTY(EditAnywhere, Category= Animations)
	TSoftObjectPtr<UAnimMontage> AimFire;
	UPROPERTY(EditAnywhere, Category= Animations)
	TSoftObjectPtr<UAnimMontage> Reload;
};

USTRUCT()
struct FMMWeaponFX
{
	GENERATED_BODY()
	
	FMMWeaponFX () : MuzzleFlash(nullptr), FireSound(nullptr), ClipClass(nullptr), ShellClass(nullptr) {}

	FORCEINLINE TSoftObjectPtr<UNiagaraSystem> GetMuzzleFlash () const { return MuzzleFlash; }
	FORCEINLINE TSoftObjectPtr<USoundCue> GetFireSound () const { return FireSound; }
	FORCEINLINE TSubclassOf<AMMEjectableObject> GetClipClass () const { return ClipClass; }
	FORCEINLINE TSubclassOf<AMMEjectableObject> GetShellClass () const { return ShellClass; }

private:
	UPROPERTY(EditAnywhere, Category= FX)
	TSoftObjectPtr<UNiagaraSystem> MuzzleFlash;
	UPROPERTY(EditAnywhere, Category= FX)
	TSoftObjectPtr<USoundCue> FireSound;
	UPROPERTY(EditAnywhere, Category= FX)
	TSubclassOf<AMMEjectableObject> ClipClass;
	UPROPERTY(EditAnywhere, Category= FX)
	TSubclassOf<AMMEjectableObject> ShellClass;
};

USTRUCT()
struct FMMProjectileBaseParams
{
	GENERATED_BODY()

	FMMProjectileBaseParams() : Type(EMMProjectileType::WT_None), InitialVelocity(0.f), CurrentDamage(0.f), MinDamage(0.f), MaxDamage(0.f), MaxRange(0.f), DamageFallOffRange(0.f), DamageFallOffAmount(0.f), DamageInnerRadius(0.f), DamageOuterRadius(0.f) {}
	
	//Getters
	FORCEINLINE EMMProjectileType GetProjectileType() const { return Type; }
	FORCEINLINE float GetInitialVelocity() const { return InitialVelocity; }
	FORCEINLINE float GetCurrentDamage() const { return CurrentDamage; }
	FORCEINLINE float GetMinDamage() const { return MinDamage; }
	FORCEINLINE float GetMaxDamage() const { return MaxDamage; }
	FORCEINLINE float GetMaxRange() const { return MaxRange; }
	FORCEINLINE float GetDamageFallOffRange() const { return DamageFallOffRange; }
	FORCEINLINE float GetDamageFallOffAmount() const { return DamageFallOffAmount; }
	FORCEINLINE float GetDamageInnerRadius() const { return DamageInnerRadius; }
	FORCEINLINE float GetDamageOuterRadius() const { return DamageOuterRadius; }
	
	//Setters
	FORCEINLINE void InitializeProjectileParams(const FMMWeaponBaseParams& _BaseParams)
	{
		CurrentDamage = _BaseParams.GetMaxDamage();
		MinDamage = _BaseParams.GetMinDamage();
		MaxDamage = _BaseParams.GetMaxDamage();
		MaxRange = _BaseParams.GetMaxRange();
		DamageFallOffRange = _BaseParams.GetDamageFallOffRange();
		DamageFallOffAmount = _BaseParams.GetDamageFallOffAmount();
		DamageInnerRadius = _BaseParams.GetDamageInnerRadius();
		DamageOuterRadius = _BaseParams.GetDamageOuterRadius();
	}

	FORCEINLINE void SetCurrentDamage(const float& _CurrentDamage) { CurrentDamage = _CurrentDamage; }
private:
	UPROPERTY(EditAnywhere, Category="Parameters | General")
	EMMProjectileType Type;
	UPROPERTY(EditAnywhere, Category="Parameters | General")
	float InitialVelocity;
	UPROPERTY(VisibleAnywhere)
	float CurrentDamage;
	UPROPERTY(VisibleAnywhere)
	float MinDamage;
	UPROPERTY()
	float MaxDamage;
	UPROPERTY()
	float MaxRange;
	UPROPERTY()
	float DamageFallOffRange;
	UPROPERTY()
	float DamageFallOffAmount;
	UPROPERTY()
	float DamageInnerRadius;
	UPROPERTY()
	float DamageOuterRadius;
};

USTRUCT()
struct FMMWeaponTrackingInfo
{
	GENERATED_BODY()
	FMMWeaponTrackingInfo() :
	NextWeaponToUnlock(EMMWeaponType::WT_None), CurrentKills(0), KillsToUnlockNextWeapon(0) {}

	FORCEINLINE EMMWeaponType GetNextWeaponToUnlock() const { return NextWeaponToUnlock; }
	FORCEINLINE int32 GetCurrentKills() const { return CurrentKills; }
	FORCEINLINE int32 GetKillsToUnlockNextWeapon() const { return KillsToUnlockNextWeapon; }

	FORCEINLINE void SetCurrentKills(const int32& _CurrentKills) { CurrentKills = _CurrentKills; }
private:
	UPROPERTY(EditDefaultsOnly, Category= Info)
	EMMWeaponType NextWeaponToUnlock;
	UPROPERTY(VisibleAnywhere, Category= Info)
	int32 CurrentKills;
	UPROPERTY(EditDefaultsOnly, Category= Info)
	int32 KillsToUnlockNextWeapon;
};

USTRUCT()
struct FMMWeaponInfo
{
	GENERATED_BODY()

public:
	FMMWeaponInfo() : bIsUnlocked(false), SlotType(EMMWeaponSlotType::WST_None), ProjectileBaseClass(nullptr) {}
	
	FORCEINLINE EMMWeaponSlotType GetSlotType() const { return SlotType; }
	FORCEINLINE TSubclassOf<AMMProjectileBase> GetProjectileBaseClass() const { return ProjectileBaseClass; }
	FORCEINLINE FMMWeaponComponentsParams GetComponentsParams() const { return WeaponComponentsParams; }
	FORCEINLINE FMMWeaponBaseParams GetBaseParameters() const { return WeaponBaseParameters; }
	FORCEINLINE FMMWeaponDispersionData GetWeaponDispersionData() const { return WeaponDispersionData; }
	FORCEINLINE FMMWeaponTrackingInfo GetWeaponTrackingInfo() const { return WeaponTrackingInfo; }
	FORCEINLINE FMMWeaponAnimations GetWeaponAnimations() const { return WeaponAnimations; }
	FORCEINLINE FMMWeaponFX GetWeaponFX() const { return WeaponFX; }
	FORCEINLINE FMMProjectileBaseParams GetProjectileBaseParameters() const { return ProjectileBaseParameters; }

	FORCEINLINE void SetBaseParameters(const FMMWeaponBaseParams& _WeaponBaseParameters) { WeaponBaseParameters = _WeaponBaseParameters; }
	FORCEINLINE void SetTrackingInfo(const FMMWeaponTrackingInfo& _WeaponTrackingInfo) { WeaponTrackingInfo = _WeaponTrackingInfo; }
private:
	UPROPERTY(EditAnywhere, Category= WeaponInfo)
	bool bIsUnlocked;
	UPROPERTY(EditAnywhere, Category= WeaponInfo)
	EMMWeaponSlotType SlotType;
	UPROPERTY(EditAnywhere, Category= WeaponInfo)
	FMMWeaponComponentsParams WeaponComponentsParams;
	UPROPERTY(EditAnywhere, Category= WeaponInfo)
	FMMWeaponBaseParams WeaponBaseParameters;
	UPROPERTY(EditAnywhere, Category= WeaponInfo)
	FMMWeaponDispersionData WeaponDispersionData;
	UPROPERTY(EditAnywhere, Category= WeaponInfo)
	FMMWeaponTrackingInfo WeaponTrackingInfo;
	UPROPERTY(EditAnywhere, Category= WeaponInfo)
	FMMWeaponAnimations WeaponAnimations;
	UPROPERTY(EditAnywhere, Category= WeaponInfo)
	FMMWeaponFX WeaponFX;
	UPROPERTY(EditAnywhere, Category= ProjectileInfo)
	TSubclassOf<AMMProjectileBase> ProjectileBaseClass;
	UPROPERTY(EditAnywhere, Category= ProjectileInfo)
	FMMProjectileBaseParams ProjectileBaseParameters;
	
};