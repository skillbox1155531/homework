﻿#pragma once

#include "CoreMinimal.h"
#include "MMArmorInfo.generated.h"

UENUM(BlueprintType)
enum class EMMArmorType : uint8
{
	AT_Head UMETA(DisplayName = "Head"),
	AT_Arms UMETA(DisplayName = "Arms"),
	AT_Body UMETA(DisplayName = "Body"),
	AT_Legs UMETA(DisplayName = "Legs"),
	AT_None UMETA(DisplayName = "None")
};

USTRUCT()
struct FMMArmorBaseParams
{
	GENERATED_BODY();

public:
	FMMArmorBaseParams() : Type(EMMArmorType::AT_None), AmmoAmount(0), MinAmount(0), MaxAmount(0) {};

	//Getters
	FORCEINLINE EMMArmorType GetArmorType() const { return Type; }
	FORCEINLINE int8 GetAmmoAmount() const { return AmmoAmount; }
	FORCEINLINE int8 GetMinAmount() const { return MinAmount; }
	FORCEINLINE int8 GetMaxAmount() const { return MaxAmount; }

	//Setters
	FORCEINLINE void SetCurrentAmount(int8 Amount) { AmmoAmount = Amount; }
private:
	UPROPERTY(EditAnywhere, Category= Parameters)
	EMMArmorType Type;
	UPROPERTY(EditAnywhere, Category= Parameters)
	int8 AmmoAmount;
	UPROPERTY(EditAnywhere, Category= Parameters)
	int8 MinAmount;
	UPROPERTY(EditAnywhere, Category= Parameters)
	int8 MaxAmount;
};

USTRUCT()
struct FMMArmorInfo
{
	GENERATED_BODY();

public:
	FMMArmorInfo() : BaseParameters(FMMArmorBaseParams()) {}
	
	//Getters
	FORCEINLINE FMMArmorBaseParams GetBaseParameters() const { return BaseParameters; }
private:
	UPROPERTY(EditAnywhere, Category= Info)
	FMMArmorBaseParams BaseParameters;
};