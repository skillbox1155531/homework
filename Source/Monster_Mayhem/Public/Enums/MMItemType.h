﻿#pragma once

UENUM(BlueprintType)
enum class EMMItemType : uint8
{
	IT_Weapon UMETA(DisplayName = "Weapon"),
	IT_Armor UMETA(DisplayName = "Armor"),
	IT_Consumable UMETA(DisplayName = "Consumable"),
	IT_Ammo UMETA(DisplayName = "Ammo"),
	IT_None UMETA(DisplayName = "None")
};