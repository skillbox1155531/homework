﻿#pragma once

UENUM(BlueprintType)
enum class EMMCharacterStatType : uint8
{
	CST_Health UMETA(DisplayName = "Health"),
	CST_Stamina UMETA(DisplayName = "Stamina"),
	CST_None UMETA(DisplayName = "None")
};