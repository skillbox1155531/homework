﻿#pragma once

UENUM(BlueprintType)
enum class EMMMovementState : uint8
{
	MS_Idle UMETA(DisplayName = "Idle"),
	MS_Aim UMETA(DisplayName = "Aim"),
	MS_Walk UMETA(DisplayName = "Walk"),
	MS_Sprint UMETA(DisplayeName = "Sprint"),
	MS_None UMETA(DisplayName = "None")
};