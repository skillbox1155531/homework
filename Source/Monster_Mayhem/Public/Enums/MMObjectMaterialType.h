﻿#pragma once

UENUM(BlueprintType)
enum class EMMObjectMaterialType : uint8
{
	OMT_Acid UMETA(DisplayName = "Acid"),
	OMT_Blood UMETA(DisplayName = "Blood"),
	OMT_Default UMETA(DisplayName = "Default"),
	OMT_Electric UMETA(DisplayName = "Electric"),
	OMT_Glass UMETA(DisplayName = "Glass"),
	OMT_Metal UMETA(DisplayName = "Metal"),
	OMT_None UMETA(DisplayName = "None")
};