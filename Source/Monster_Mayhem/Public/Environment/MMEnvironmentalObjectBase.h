// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/IDamageable.h"
#include "MMEnvironmentalObjectBase.generated.h"

class UMMBaseHitHandlerComponent;
class UBoxComponent;
enum class EMMObjectMaterialType : uint8;
class UNiagaraSystem;
class AMMDamageVisual;
class AMMImpactVisual;

UCLASS()
class MONSTER_MAYHEM_API AMMEnvironmentalObjectBase : public AActor, public IDamageable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMMEnvironmentalObjectBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UFUNCTION()
	virtual void HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	
protected:
	UPROPERTY()
	USceneComponent* Root;
	UPROPERTY(EditDefaultsOnly)
	UBoxComponent* Collision;
	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* Mesh;
	UPROPERTY(VisibleAnywhere)
	UMMBaseHitHandlerComponent* HitHandlerComponent;
};
