// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Items/MMItem.h"
#include "Items/MMPickupActor.h"
#include "MMItemSpawnPoint.generated.h"

class AMMPickupActor;

UENUM(BlueprintType)
enum class EMMSpawnPointType : uint8
{
	SPT_Weapon UMETA(DisplayName = "Weapon"),
	SPT_Ammo UMETA(DisplayName = "Ammo"),
	SPT_Health UMETA(DisplayName = "Health"),
	SPT_None UMETA(DisplayName = "None")
};

UCLASS()
class MONSTER_MAYHEM_API AMMItemSpawnPoint : public AActor
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
public:	
	// Sets default values for this actor's properties
	AMMItemSpawnPoint() : Type(EMMSpawnPointType::SPT_None), bIsActive(false), PickupActorPtr(nullptr), RotationUpdateRate(0.05f), RotationAngle(50.f), RotationInterpSpeed(150.f) {}

	void Initialization();
	bool SpawnItem(TWeakObjectPtr<UMMItem> _Item);
	void DeactivateItem();
	void SetItemRotationTimer();
	void ItemRotation();
	
	FORCEINLINE TWeakObjectPtr<UMMItem> GetItemToSpawn() const
	{
		if (PickupActorPtr)
		{
			return  PickupActorPtr -> GetPickupItem();
		}
		return TWeakObjectPtr<UMMItem>();
	}
	
	FORCEINLINE EMMSpawnPointType GetType() const { return Type; }
	FORCEINLINE bool GetIsActive() const { return bIsActive; }
	FORCEINLINE void SetIsActive(const bool& _bIsActive) { bIsActive = _bIsActive; }
private:
	UPROPERTY(EditAnywhere, Category= Parameters)
	EMMSpawnPointType Type;
	UPROPERTY(VisibleAnywhere)
	bool bIsActive;
	UPROPERTY()
	AMMPickupActor* PickupActorPtr;
	FTimerHandle ItemRotationTimerHandle;
	UPROPERTY(EditAnywhere, Category= Parameters)
	float RotationUpdateRate;
	UPROPERTY(EditAnywhere, Category= Parameters)
	float RotationAngle;
	UPROPERTY(EditAnywhere, Category= Parameters)
	float RotationInterpSpeed;
};
