// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MMEnvironmentalObjectBase.h"
#include "GameFramework/Actor.h"
#include "GeometryCollection/GeometryCollectionComponent.h"
#include "MMDestructibleObject.generated.h"

class UObjectLibrary;
class UBoxComponent;

UENUM(BlueprintType)
enum class EMMDestructionDirection : uint8
{
	DD_UP UMETA(DisplayName = "Up"),
	DD_FORWARD UMETA(DisplayName = "Forward")
};

UCLASS()
class MONSTER_MAYHEM_API AMMDestructibleObject : public AMMEnvironmentalObjectBase
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMMDestructibleObject();

private:
	void SpawnDamageVisualsPool();
	void SpawnDamageVisual();
	bool ShowDamageVisual(const float& DamageAmount);
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

	void EnableDestructibleMesh();
	void Destruct();
	bool SpawnMasterField();
	
private:
	UPROPERTY(EditAnywhere)
	UGeometryCollectionComponent* DamagedMesh;
	
	UPROPERTY(EditAnywhere, Category= General)
	bool bIsMultiShot;
	UPROPERTY(VisibleAnywhere, Category= General)
	bool bIsDamaged;
	UPROPERTY(VisibleAnywhere, Category= General)
	int8 CurrentShots;
	UPROPERTY(EditAnywhere, Category= General)
	int8 ShotsToChangeMesh;
	UPROPERTY(EditAnywhere, Category= General)
	int8 ShotsToDestruct;
	UPROPERTY(EditAnywhere, Category= General)
	float DestroyDelay;

	UPROPERTY(EditAnywhere, Category= Destruction)
	EMMDestructionDirection ForceDirection;
	UPROPERTY(EditAnywhere, Category= Destruction);
	TSubclassOf<AFieldSystemActor> MasterFieldClass;
	UPROPERTY()
	AFieldSystemActor* MasterFieldPtr;
	UPROPERTY()
	FRotator Direction;

	UPROPERTY(EditDefaultsOnly)
	int8 DamageVisualsSpawnAmount;
	UPROPERTY(EditAnywhere)
	float DamageVisualOffset;
	UPROPERTY(VisibleAnywhere)
	TArray<AMMDamageVisual*> DamageVisualsArr;
	
	FTimerHandle DestroyTimerHandle;
};
