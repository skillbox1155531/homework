// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Enums/MMMovementState.h"
#include "GameFramework/Actor.h"
#include "Interfaces/IWeapon.h"
#include "Projectiles/MMProjectileBase.h"
#include "Structs/Weapons/MMWeaponInfo.h"
#include "Helper/DelegateRegistry.h"
#include "MMWeaponBase.generated.h"

class UDecalComponent;
class USceneComponent;
class USkeletalMeshComponent;
class UNiagaraComponent;
class UArrowComponent;
class UAudioComponent;
class AMMProjectileBase;

enum class EMMMovementState : uint8;

UCLASS()
class MONSTER_MAYHEM_API AMMWeaponBase : public AActor, public IWeapon
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMMWeaponBase();

private:
	
	void AutoFire(const FOnUpdateWeaponTrackingInfo& Delegate);
	void SingleFire();
	void FireLogic(const FOnUpdateWeaponTrackingInfo& Delegate);
	bool LaunchProjectile();
	void LaunchClip();
	void LaunchShell();
	void Reload();
	FVector ApplyDispersion(const FVector& FireDirection);
	void ReloadTimer();
	void SetDispersionReductionTimer();
	void DispersionReductionTimer();
	void AddDispersion();
	FRotator CalcProjectileRotation(const FVector& DecalLocation);
	FVector GetFireEndLocation(const FVector& DecalLocation);
	TWeakObjectPtr<AMMProjectileBase> GetProjectileFromInactivePool();
	void AddProjectileToInactivePool(TWeakObjectPtr<AMMProjectileBase> Projectile);
	TWeakObjectPtr<AMMEjectableObject> GetClipFromInactivePool();
	void AddClipToInactivePool();
	TWeakObjectPtr<AMMEjectableObject> GetShellFromInactivePool();
	void AddShellToInactivePool();
	void LoadWeaponMesh(const TSoftObjectPtr<USkeletalMesh>& Mesh);
	void SetWeaponMesh(TSoftObjectPtr<USkeletalMesh> Mesh);
	void LoadBaseFireAnim(TSoftObjectPtr<UAnimMontage> Anim);
	void SetBaseFireAnim(TSoftObjectPtr<UAnimMontage> Anim);
	void LoadAimFireAnim(TSoftObjectPtr<UAnimMontage> Anim);
	void SetAimFireAnim(TSoftObjectPtr<UAnimMontage> Anim);
	void LoadReloadAnim(TSoftObjectPtr<UAnimMontage> Anim);
	void SetReloadAnim(TSoftObjectPtr<UAnimMontage> Anim);
	void LoadMuzzleFlash(TSoftObjectPtr<UNiagaraSystem> Particle);
	void SetMuzzleFlash(TSoftObjectPtr<UNiagaraSystem> Particle);
	void LoadFireSound(TSoftObjectPtr<USoundCue> Sound);
	void SetFireSound(TSoftObjectPtr<USoundCue> Sound);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	void InitializeComponents(const FMMWeaponComponentsParams& ComponentsParams);
	void InitializeParams(USceneComponent* Component, const int32& _Id, const TSoftObjectPtr<UTexture2D>& _HUDThumbnail, const FMMWeaponInfo& _WeaponInfo);
	void InitializeParams(USceneComponent* Component, const FMMWeaponInfo& _WeaponInfo);
	void SetCurrentDispersionParams(const EMMMovementState& MovementState);
	virtual void ExecuteFire(const FOnUpdateWeaponTrackingInfo& Delegate) override;
	virtual void ExecuteReload() override;
	void ResetFire();
	void ResetWeaponStates();
	virtual void Tick(float DeltaTime) override;
	void SpawnProjectilePool(const TSubclassOf<AMMProjectileBase>& ProjectileBaseClass, const FMMProjectileBaseParams& ProjectileBaseParameters);
	void SpawnClipPool(const TSubclassOf<AMMEjectableObject>& ClipBaseClass);
	void SpawnShellPool(const TSubclassOf<AMMEjectableObject>& ShellBaseClass);
	void Deinitialize();
	void SetActive(const bool& bIsActive);

	FORCEINLINE int32 GetWeaponId() const { return Id; }
	FORCEINLINE FMMWeaponBaseParams& GetWeaponBaseParameters() { return BaseParameters; }
	FORCEINLINE FMMWeaponTrackingInfo  GetWeaponTrackingInfo() const { return WeaponTrackingInfo; }
	FORCEINLINE EMMWeaponType GetWeaponType() const { return BaseParameters.GetWeaponType(); }
	FORCEINLINE EMMWeaponFireType GetWeaponFireType() const { return BaseParameters.GetWeaponFireType(); }
	FORCEINLINE int8 GetCurrentRoundsInClip() const { return BaseParameters.GetRoundsInClip(); }
	FORCEINLINE int32 GetCurrentRoundsInReserve() const { return BaseParameters.GetRoundsInReserve(); }
	FORCEINLINE int32 GetMaxRoundsInReserve() const { return BaseParameters.GetMaxRoundsInReserve(); }
	FORCEINLINE bool GetIsEquipped() const { return bIsEquipped; }
	FORCEINLINE bool GetCanFire() const { return bCanFire; }
	FORCEINLINE bool GetCanReload() const { return BaseParameters.GetRoundsInClip() < BaseParameters.GetMaxRoundsInClip() && BaseParameters.GetRoundsInReserve() > 0; }
	FORCEINLINE bool GetIsReload() const { return bIsReload; }
	FORCEINLINE TWeakObjectPtr<UAnimMontage> GetBaseFireAnimation() const { return BaseFireAnim; }
	FORCEINLINE TWeakObjectPtr<UAnimMontage> GetAimFireAnimation() const { return AimFireAnim; }
	FORCEINLINE TWeakObjectPtr<UAnimMontage> GetReloadAnimation() const { return ReloadAnim; }
	FORCEINLINE TSoftObjectPtr<UTexture2D> GetHUDThumbnail() const { return HUDThumbnail; }

	FORCEINLINE void SetRoundsInClip(const int8& Amount) { BaseParameters.SetRoundsInClip(Amount); }
	FORCEINLINE void SetRoundsInReserve(const int32& Amount) { BaseParameters.SetRoundsInReserve(Amount); }
	
private:
	UPROPERTY()
	USceneComponent* CursorDecal;
	UPROPERTY(EditDefaultsOnly)
	USceneComponent* Root;
	UPROPERTY(EditDefaultsOnly)
	USkeletalMeshComponent* WeaponMesh;
	UPROPERTY(EditDefaultsOnly)
	UArrowComponent* FireSpot;
	UPROPERTY(EditDefaultsOnly)
	UArrowComponent* ShellEjectionSpot;
	UPROPERTY(EditDefaultsOnly)
	UArrowComponent* ClipEjectionSpot;
	UPROPERTY(EditDefaultsOnly)
	UNiagaraComponent* MuzzleFlash;
	UPROPERTY(EditDefaultsOnly)
	UAudioComponent* AudioComponent;

	UPROPERTY(VisibleAnywhere, Category= "Parameters | Status")
	bool bIsEquipped;
	UPROPERTY(VisibleAnywhere, Category= "Parameters | Fire")
	bool bCanFire;
	UPROPERTY(VisibleAnywhere, Category= "Parameters | Reload")
	bool bIsReload;
	UPROPERTY(VisibleAnywhere, Category= "Parameters | Reload")
	float ReloadTime;

	UPROPERTY(VisibleAnywhere, Category= "Parameters | General")
	int32 Id;
	UPROPERTY(VisibleAnywhere, Category= "Parameters | General")
	EMMWeaponSlotType SlotType;
	UPROPERTY(VisibleAnywhere, Category= "Parameters | General")
	FMMWeaponBaseParams BaseParameters;
	UPROPERTY(VisibleAnywhere, Category= "Parameters | General")
	float FireDirectionThreshold;
	
	UPROPERTY(VisibleAnywhere, Category= "Parameters | Dispersion")
	float CurrentDispersion;
	UPROPERTY(VisibleAnywhere, Category= "Parameters | Dispersion")
	FMMWeaponDispersionData DispersionData;
	UPROPERTY(VisibleAnywhere, Category= "Parameters | Dispersion")
	FMMWeaponDispersionParams CurrentDispersionParams;

	UPROPERTY(VisibleAnywhere, Category= "Parameters | Tracking")
	FMMWeaponTrackingInfo WeaponTrackingInfo;
	
	UPROPERTY(VisibleAnywhere, Category= "Animations")
	TWeakObjectPtr<UAnimMontage> BaseFireAnim;
	UPROPERTY(VisibleAnywhere, Category= "Animations")
	TWeakObjectPtr<UAnimMontage> AimFireAnim;
	UPROPERTY(VisibleAnywhere, Category= "Animations")
	TWeakObjectPtr<UAnimMontage> ReloadAnim;
	UPROPERTY(VisibleAnywhere, Category= "Display")
	TSoftObjectPtr<UTexture2D> HUDThumbnail;
	
	FTimerHandle FireTimerHandle;
	FTimerHandle DispersionReductionTimerHandle;
	FTimerHandle ReloadTimerHandle;

	UPROPERTY(VisibleAnywhere)
	TMap<TWeakObjectPtr<AMMProjectileBase>, bool> ActiveProjectileQueue;
	UPROPERTY(VisibleAnywhere)
	TArray<TWeakObjectPtr<AMMProjectileBase>> InactiveProjectileQueue;
	
	TQueue<TWeakObjectPtr<AMMEjectableObject>> ActiveClipQueue;
	TQueue<TWeakObjectPtr<AMMEjectableObject>> InactiveClipQueue;
	
	TQueue<TWeakObjectPtr<AMMEjectableObject>> ActiveShellQueue;
	TQueue<TWeakObjectPtr<AMMEjectableObject>> InactiveShellQueue;

public:
	FOnExecuteWeaponAction OnReloadWeapon;
	FOnExecuteWeaponAction OnUpdateWeaponUI;
	FOnRequestWeaponAmmoSpawn OnRequestWeaponAmmo;
};
