// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "IInteractable.generated.h"

class AMMPickupActor;
class UMMItem;
 enum class EMMItemType : uint8;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInteractable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MONSTER_MAYHEM_API IInteractable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual void StartInteract() = 0;
	virtual void EndInteract() = 0;
	virtual EMMItemType GetItemType() = 0;
	virtual TTuple<TWeakObjectPtr<AMMPickupActor>, TWeakObjectPtr<UMMItem>> Interact() = 0;
	virtual void StartFocus() = 0;
	virtual void EndFocus() = 0;
};
