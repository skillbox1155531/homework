// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "IProjectile.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UProjectile : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MONSTER_MAYHEM_API IProjectile
{
	GENERATED_BODY()

public:
	virtual void TrackProjectile() = 0;
	virtual void HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	FVector NormalImpulse, const FHitResult& Hit) = 0;
	virtual void SetActive(const bool& bIsActive) = 0;
};
