// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Monster_Mayhem : ModuleRules
{
	public Monster_Mayhem(ReadOnlyTargetRules Target) : base(Target)
	{
		PrivateDependencyModuleNames.AddRange(new string[] { "AnimGraphRuntime" });
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "Niagara", "EnhancedInput", "GeometryCollectionEngine", "FieldSystemEngine" });
       
        PublicIncludePaths.AddRange(new string[]
        {
	        "Monster_Mayhem",
	        "Monster_Mayhem/Private",
	        "Monster_Mayhem/Private/FX",
	        "Monster_Mayhem/Private/Player",
	        "Monster_Mayhem/Private/Enemy",
	        "Monster_Mayhem/Private/Components",
	        "Monster_Mayhem/Public",
	        "Monster_Mayhem/Public/FX",
	        "Monster_Mayhem/Public/Player",
	        "Monster_Mayhem/Public/Enemy",
	        "Monster_Mayhem/Public/Components",
	        "Monster_Mayhem/Public/Structs",
	        "Monster_Mayhem/Public/UI"
        });
	}
}
